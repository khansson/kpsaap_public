#ifndef _OPTICS_
#define _OPTICS_



#include "controller.h"
#include <cmath>
#include <vector>
#include <complex>




struct Particle_Ideal
{
	std::vector<double> Parms;
	double Qa;
	double Qe;
	double Qs;
	std::vector<double> PhaseFunction;
	Particle_Ideal(int i) {Parms.resize(i);};
	
	Particle_Ideal(){;};
	double GetAngle();
};

class multi_vector
{
	private:
	std::vector<Particle_Ideal> vec;
	std::vector<int> dims;
	
	public:
	void Generate(std::vector<int> bins, std::vector<double> mins, std::vector<double> maxes);

	Particle_Ideal * at(std::vector<int> ind);
	Particle_Ideal  * at(int i) {return &vec.at(i);};
	Particle_Ideal * search(std::vector<double> parm);
	
	int dim() { return dims.size();};
	int size() { return vec.size();};
	
	
};

typedef std::complex<double> dcomp;

struct dcomp_counted
{
	dcomp_counted(){n = 0; complex = 0;};
	dcomp_counted(dcomp d, int nn){ complex = d; n = nn;};
	dcomp complex;
	int n;
	dcomp_counted& operator+(const dcomp_counted& rhs){ 
            complex += rhs.complex;
            n += rhs.n;
            return *this;
    }
    dcomp_counted& operator+=(const dcomp_counted& rhs){ 
            complex += rhs.complex;
            n += rhs.n;
            return *this;
    }
    double normm(){ if (n!= 0){return n *norm(complex/(double)n);} else{return 0;}};
    double abss(){ if (n!= 0){return n *abs(complex/(double)n);} else{return 0;}};
};





double Borosilicate_Absorbtion( double lamda);

double Borosilicate_Reflectance( double lamda);


struct OpticData
{


	//on emission
	double RayPower;
	void emission_spectrum(double * props);
	
	double partn,partk;
	
	std::vector<double> ParticleDefaultVal;
	std::vector<int> ParticleBins;
	std::vector<int> ParticleIndecies;
	
	
	//on scattering
	

	
	
	
	int (OpticData::*ScatInitFunc)();
	
	# define X(q) \
	int Initialize_##q (); \
	
	SCATTER_TXT
	#   undef X

	
	std::vector<dcomp> IndexParticle;
	dcomp IndexAir;
		
	double EmissionLambda;	
	
	double MinLambda() { return EmissionLambda;};
	double MaxLambda() { return EmissionLambda;};
		
	int lambdasamples;
	
	
	int particle_dimensions;
	
	multi_vector Ideal_Array;
	
	
	
	
};








#endif
