

#include <stdio.h>


#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "controller.h"
#include <stdlib.h>
#include <math.h> 
#include "optics.h"
#include "ranf.h"

//These functions take ghosts from sumulation.c and using stuff from optics, create new rays representing scatters


int controller::Scatter_One_Ray_Mie(int i, std::deque<ghost> *ghost_vec,std::vector<ray> *ray_vec, int trial)
{
	
	if (ghost_vec->size()>0)
	{	
		double chi;
		std::vector<double> parms;
		
		
		
		//homogenization
		bool homo = false;
		
		if(ghost_vec->front().prt() == -4)
		{
			homo = true;
			
			ghost_vec->front().prt(mesh_exact.at(ghost_vec->front().EX()).at(ghost_vec->front().EY()).at(ghost_vec->front().EZ()).at(floor(mesh_exact.at(ghost_vec->front().EX()).at(ghost_vec->front().EY()).at(ghost_vec->front().EZ()).size() * ranf(trial))));
			
		}
		
		//real
		
		
		parms = ParmVec(ghost_vec->front().prt(),ghost_vec->front().Lambda());
		
		Particle_Ideal * Ideal;
		
		Ideal = Optics.Ideal_Array.search(parms); 
		
		double particle_transmittance;
		
		if (homo)
		{
			particle_transmittance = 1;
		}
		else
		{
			 particle_transmittance = 1-(fabs(Ideal->Qa/Ideal->Qe));
			ParticleQ.at(ghost_vec->front().prt()).at(trial) += ghost_vec->front().P()*(1-particle_transmittance);
		}
		
		
		
		double ParticleRadius = ParticleParm(ghost_vec->front().prt(),0)/2.0;
		
		double randx;
		double randy;
		double randz;
		double dot, mag, angle;
		double finalx, finaly, finalz;
		double costheta, sintheta;
		double zenith, azmuthial;
		//printf("i be here");
		
		if(ghost_vec->front().P()*particle_transmittance > MinPowerThreshold)
		{
			//printf("Ray scattered \n");
			
			do
			{
				zenith =  M_PI*ranf(trial);
			}while (zenith==0.0);
			
			azmuthial = 2* 3.14159265 *ranf(trial);
			
			randx = sin(azmuthial)*sin(zenith);
			randy = cos(azmuthial)*sin(zenith);
			randz = cos(zenith);
			
			dot = randx * ghost_vec->front().DX() + randy * ghost_vec->front().DY()  +randz * ghost_vec->front().DZ();
			
			randx -= ghost_vec->front().DX() * dot;
			randy -= ghost_vec->front().DY() * dot;
			randz -= ghost_vec->front().DZ() * dot;
			
			mag = sqrt(randx * randx + randy * randy + randz * randz);
			
			randx /= mag;
			randy /= mag;
			randz /= mag;
			
			angle = Ideal->GetAngle();
			costheta = cos(angle);
			sintheta = sin(angle);
			
			finalx = costheta * ghost_vec->front().DX() + sintheta * randx;
			finaly = costheta * ghost_vec->front().DY() + sintheta * randy;
			finalz = costheta * ghost_vec->front().DZ() + sintheta * randz;
			
			if(std::isnan(finaly))
			{
				printf("well that explains it\n");
				
			}
			
			if(homo)
			{
				ray_vec->push_back(ray(ghost_vec->front().xyz.at(0),ghost_vec->front().xyz.at(1),ghost_vec->front().xyz.at(2),finalx,finaly,finalz,ghost_vec->front().P()*particle_transmittance,ghost_vec->front().Lambda(),ghost_vec->front().S()) );
				ray_vec->back().T(ghost_vec->front().T());
				ray_vec->back().oT(ghost_vec->front().T());
				
			}
			else
			{			
				ray_vec->push_back(ray(ParticleX.at(ghost_vec->front().prt())+finalx*ParticleRadius*1.01,ParticleY.at(ghost_vec->front().prt())+finaly*ParticleRadius*1.01,ParticleZ.at(ghost_vec->front().prt())+finalz*ParticleRadius*1.01,finalx,finaly,finalz,ghost_vec->front().P()*particle_transmittance,ghost_vec->front().Lambda(),ghost_vec->front().S()));
				ray_vec->back().T(ghost_vec->front().T() + ParticleRadius*1.01);
				ray_vec->back().oT(ghost_vec->front().T());
				
			}
			
			//ray_vec->back().T(ghost_vec->front().T());
			/*ray_vec->back().E(1,ghost_vec->front().EX());
			ray_vec->back().E(2,ghost_vec->front().EY());
			ray_vec->back().E(3,ghost_vec->front().EZ());
			
			
			
			if (ray_vec->back().TestElem(Xelem_size,Yelem_size,Zelem_size) == true)
			{
				ray_vec->back().E(1,floor(ray_vec->back().X()/Xelem_size));
				ray_vec->back().E(2,floor(ray_vec->back().Y()/Yelem_size));
				ray_vec->back().E(3,floor(ray_vec->back().Z()/Zelem_size));
				//printf("We jumped an element\n");
			}*/	
				
			ray_vec->back().E(1,floor(ray_vec->back().X()/Xelem_size));
			ray_vec->back().E(2,floor(ray_vec->back().Y()/Yelem_size));
			ray_vec->back().E(3,floor(ray_vec->back().Z()/Zelem_size));	
			ray_vec->back().just_scatted(1);
			if(LogRays) {ghost_vec->front().GiveHistory(&(ray_vec->back())); ray_vec->back().SaveHistory(2);}
			if(ray_vec->back().X() > domainX || ray_vec->back().X() < 0  || ray_vec->back().Y() > domainY || ray_vec->back().Y() < 0 || ray_vec->back().Z() > domainZ || ray_vec->back().Z() < 0)
			{
				printf("shit scatt\n");
				ray_vec->back().obliterate();
			}
			
			
			
			
			ghost_vec->pop_front();
		}
		else
		{
			printf("Ray entirely absorbed \n");
			
			int S = ghost_vec->front().S();
			if (S+1 > Scatt_vec.at(trial).size())
			{
				Scatt_vec.at(trial).resize(S +1);
				 
			}
			Scatt_vec.at(trial).at(S)+=1;
			
			ghost_vec->pop_front();
			
			return -3;
		}
		
		
	}
	else
	{
		return 1;
	}
	
	//printf("GREAT SUCESS \n");
	return 0;
	
}


int controller::Scatter_One_Ray_Mie_Phase(int i, std::deque<ghost> *ghost_vec,std::vector<ray> *ray_vec, int trial)
{
	
	if (ghost_vec->size()>0)
	{	
		double chi;
		std::vector<double> parms;
		
		//homogenization
		bool homo = false;
		
		if(ghost_vec->front().prt() == -4)
		{
			homo = true;
			
			ghost_vec->front().prt(mesh_exact.at(ghost_vec->front().EX()).at(ghost_vec->front().EY()).at(ghost_vec->front().EZ()).at(floor(mesh_exact.at(ghost_vec->front().EX()).at(ghost_vec->front().EY()).at(ghost_vec->front().EZ()).size() * ranf(trial))));
			
		}
		
		
		parms = ParmVec(ghost_vec->front().prt(),ghost_vec->front().Lambda());
		
		Particle_Ideal * Ideal;
		
		Ideal = Optics.Ideal_Array.search(parms);
		
		double particle_transmittance;
		
		if(homo)
		{
			particle_transmittance = 1;
		}
		else
		{
			particle_transmittance = 1-(fabs(Ideal->Qa/Ideal->Qe));
			ParticleQ.at(ghost_vec->front().prt()).at(trial) += ghost_vec->front().P()*(1-particle_transmittance);
		}
		double ParticleRadius = ParticleParm(ghost_vec->front().prt(),0)/2.0;
		
		double randx;
		double randy;
		double randz;
		double dot, mag, angle;
		double finalx, finaly, finalz;
		double costheta, sintheta;
		double zenith, azmuthial;
		//printf("i be here");
		
		if(ghost_vec->front().P()*particle_transmittance > MinPowerThreshold)
		{
			//printf("Ray scattered \n");
			
			do
			{
				zenith =  M_PI*ranf(trial);
			}while (zenith==0.0);
			
			azmuthial = 2* 3.14159265 *ranf(trial);
			
			randx = sin(azmuthial)*sin(zenith);
			randy = cos(azmuthial)*sin(zenith);
			randz = cos(zenith);
			
			dot = randx * ghost_vec->front().DX() + randy * ghost_vec->front().DY()  +randz * ghost_vec->front().DZ();
			
			randx -= ghost_vec->front().DX() * dot;
			randy -= ghost_vec->front().DY() * dot;
			randz -= ghost_vec->front().DZ() * dot;
			
			mag = sqrt(randx * randx + randy * randy + randz * randz);
			
			randx /= mag;
			randy /= mag;
			randz /= mag;
			
			angle = Ideal->GetAngle();
			costheta = cos(angle);
			sintheta = sin(angle);
			
			finalx = costheta * ghost_vec->front().DX() + sintheta * randx;
			finaly = costheta * ghost_vec->front().DY() + sintheta * randy;
			finalz = costheta * ghost_vec->front().DZ() + sintheta * randz;
			
			if(!homo)
			{			
				ray_vec->push_back(ray(ParticleX.at(ghost_vec->front().prt())+finalx*ParticleRadius*1.01,ParticleY.at(ghost_vec->front().prt())+finaly*ParticleRadius*1.01,ParticleZ.at(ghost_vec->front().prt())+finalz*ParticleRadius*1.01,finalx,finaly,finalz,ghost_vec->front().P()*sqrt(particle_transmittance),ghost_vec->front().Lambda(), ghost_vec->front().T() + ParticleRadius*1.01 ,ghost_vec->front().S()));
				if(LogRays) {ghost_vec->front().GiveHistory(&(ray_vec->back())); ray_vec->back().SaveHistory(2);}
				
			}
			else
			{			
				ray_vec->push_back(ray(ParticleX.at(ghost_vec->front().prt())+finalx*ParticleRadius*1.01,ParticleY.at(ghost_vec->front().prt())+finaly*ParticleRadius*1.01,ParticleZ.at(ghost_vec->front().prt())+finalz*ParticleRadius*1.01,finalx,finaly,finalz,ghost_vec->front().P()*particle_transmittance,ghost_vec->front().Lambda(), ghost_vec->front().T(),ghost_vec->front().S()));
				if(LogRays) {ghost_vec->front().GiveHistory(&(ray_vec->back())); ray_vec->back().SaveHistory(2);}
			}
			
			
			
			ray_vec->back().E(1,ghost_vec->front().EX());
			ray_vec->back().E(2,ghost_vec->front().EY());
			ray_vec->back().E(3,ghost_vec->front().EZ());
			if (ray_vec->back().TestElem(Xelem_size,Yelem_size,Zelem_size) == true)
			{
				ray_vec->back().E(1,floor(ray_vec->back().X()/Xelem_size));
				ray_vec->back().E(2,floor(ray_vec->back().Y()/Yelem_size));
				ray_vec->back().E(3,floor(ray_vec->back().Z()/Zelem_size));
				printf("We jumped an element\n");
				
								
			}
			ghost_vec->pop_front();
		}
		else
		{
			//printf("Ray entirely absorbed \n");
			
			int S = ghost_vec->front().S();
			if (S+1 > Scatt_vec.at(trial).size())
			{
				Scatt_vec.at(trial).resize(S +1);
				 
			}
			Scatt_vec.at(trial).at(S)+=1;
			
			ghost_vec->pop_front();
			
			
			
			return -3;
		}
		
		
		
		
	}
	else
	{
		return 1;
	}
	
	//printf("GREAT SUCESS \n");
	return 0;
	
}

int controller::Scatter_One_Ray_Mie_Shell(int i, std::deque<ghost> *ghost_vec,std::vector<ray> *ray_vec, int trial)
{
	
	if (ghost_vec->size()>0)
	{	
		double chi;
		std::vector<double> parms;
		
		
		
		//homogenization
		bool homo = false;
		
		if(ghost_vec->front().prt() == -4)
		{
			homo = true;
			
			ghost_vec->front().prt(mesh_exact.at(ghost_vec->front().EX()).at(ghost_vec->front().EY()).at(ghost_vec->front().EZ()).at(floor(mesh_exact.at(ghost_vec->front().EX()).at(ghost_vec->front().EY()).at(ghost_vec->front().EZ()).size() * ranf(trial))));
			
		}
		
		//real
		
		
		parms = ParmVec(ghost_vec->front().prt(),ghost_vec->front().Lambda());
		
		Particle_Ideal * Ideal;
		
		Ideal = Optics.Ideal_Array.search(parms);
		
		double particle_transmittance;
		
		if(homo)
		{
			particle_transmittance = 1;
		}
		else
		{
			particle_transmittance = 1-(fabs(Ideal->Qa/Ideal->Qe));
			ParticleQ.at(ghost_vec->front().prt()).at(trial) += ghost_vec->front().P()*(1-particle_transmittance);
		}
		
		
		
		double ParticleRadius = ParticleParm(ghost_vec->front().prt(),0)/2.0;
		
		double randx;
		double randy;
		double randz;
		double dot, mag, angle;
		double finalx, finaly, finalz;
		double costheta, sintheta;
		double zenith, azmuthial;
		//printf("i be here");
		
		if(ghost_vec->front().P()*particle_transmittance > MinPowerThreshold)
		{
			//printf("Ray scattered \n");
			
			do
			{
				zenith =  M_PI*ranf(trial);
			}while (zenith==0.0);
			
			azmuthial = 2* 3.14159265 *ranf(trial);
			
			randx = sin(azmuthial)*sin(zenith);
			randy = cos(azmuthial)*sin(zenith);
			randz = cos(zenith);
			
			dot = randx * ghost_vec->front().DX() + randy * ghost_vec->front().DY()  +randz * ghost_vec->front().DZ();
			
			randx -= ghost_vec->front().DX() * dot;
			randy -= ghost_vec->front().DY() * dot;
			randz -= ghost_vec->front().DZ() * dot;
			
			mag = sqrt(randx * randx + randy * randy + randz * randz);
			
			randx /= mag;
			randy /= mag;
			randz /= mag;
			
			angle = Ideal->GetAngle();
			costheta = cos(angle);
			sintheta = sin(angle);
			
			finalx = costheta * ghost_vec->front().DX() + sintheta * randx;
			finaly = costheta * ghost_vec->front().DY() + sintheta * randy;
			finalz = costheta * ghost_vec->front().DZ() + sintheta * randz;
			
			
			
			if(homo)
			{
				ray_vec->push_back(ray(ghost_vec->front().xyz.at(0),ghost_vec->front().xyz.at(1),ghost_vec->front().xyz.at(2),finalx,finaly,finalz,ghost_vec->front().P()*particle_transmittance,ghost_vec->front().Lambda(),ghost_vec->front().S()));
				
			}
			else
			{			
				ray_vec->push_back(ray(ParticleX.at(ghost_vec->front().prt())+finalx*ParticleRadius*1.01,ParticleY.at(ghost_vec->front().prt())+finaly*ParticleRadius*1.01,ParticleZ.at(ghost_vec->front().prt())+finalz*ParticleRadius*1.01,finalx,finaly,finalz,ghost_vec->front().P()*particle_transmittance,ghost_vec->front().Lambda(),ghost_vec->front().S()));
			}
			ray_vec->back().E(1,ghost_vec->front().EX());
			ray_vec->back().E(2,ghost_vec->front().EY());
			ray_vec->back().E(3,ghost_vec->front().EZ());
			if (ray_vec->back().TestElem(Xelem_size,Yelem_size,Zelem_size) == false)
			{
				ray_vec->back().E(1,floor(ray_vec->back().X()/Xelem_size));
				ray_vec->back().E(2,floor(ray_vec->back().Y()/Yelem_size));
				ray_vec->back().E(3,floor(ray_vec->back().Z()/Zelem_size));
				printf("We jumped an element\n");
				
				if(ray_vec->back().EX() > domain_elemX || ray_vec->back().EX() < 0 || ray_vec->back().EY() > domain_elemY || ray_vec->back().EY() < 0 || ray_vec->back().EZ() > domain_elemZ || ray_vec->back().EZ() < 0)
				{
					printf("WE JUMPED TOO FAR AND narrowly avoied a seg fault\n");
					ray_vec->back().obliterate();
					Lost_in_transmission +=1;
				}
				
			}
			
			ghost_vec->pop_front();
		}
		else
		{
			//printf("Ray entirely absorbed \n");
			
			int S = ghost_vec->front().S();
			if (S+1 > Scatt_vec.at(trial).size())
			{
				Scatt_vec.at(trial).resize(S +1);
				 
			}
			Scatt_vec.at(trial).at(S)+=1;

			ghost_vec->pop_front();
			
			return -3;
		}
		
		
	}
	else
	{
		return 1;
	}
	
	//printf("GREAT SUCESS \n");
	return 0;
	
}



int controller::Scatter_One_Ray_Isotropic(int i, std::deque<ghost> *ghost_vec,std::vector<ray> *ray_vec, int trial)
{
	
	if (ghost_vec->size()>0)
	{	
		double chi;
		std::vector<double> parms;
		
		
		
		//homogenization
		bool homo = false;
		
		if(ghost_vec->front().prt() == -4)
		{
			homo = true;
			
			ghost_vec->front().prt(mesh_exact.at(ghost_vec->front().EX()).at(ghost_vec->front().EY()).at(ghost_vec->front().EZ()).at(floor(mesh_exact.at(ghost_vec->front().EX()).at(ghost_vec->front().EY()).at(ghost_vec->front().EZ()).size() * ranf(trial))));
			
		}
		
		//real
		
		
		parms = ParmVec(ghost_vec->front().prt(),ghost_vec->front().Lambda());
		
		Particle_Ideal * Ideal;
		
		Ideal = Optics.Ideal_Array.search(parms); 
		
		double particle_transmittance;
		
		if (homo)
		{
			particle_transmittance = 1;
		}
		else
		{
			 particle_transmittance = (fabs(Ideal->Qs/Ideal->Qe));
			ParticleQ.at(ghost_vec->front().prt()).at(trial) += ghost_vec->front().P()*(1-particle_transmittance);
		}
		
		
		
		double ParticleRadius = ParticleParm(ghost_vec->front().prt(),0)/2.0;
		
		double randx;
		double randy;
		double randz;
		double dot, mag, angle;
		double finalx, finaly, finalz;
		double costheta, sintheta;
		double zenith, azmuthial;
		//printf("i be here");
		
		if(ghost_vec->front().P()*particle_transmittance > MinPowerThreshold)
		{
			//printf("Ray scattered \n");
			
			do
			{
				zenith =  M_PI*ranf(trial);
			}while (zenith==0.0);
			
			azmuthial = 2* 3.14159265 *ranf(trial);
			
			randx = sin(azmuthial)*sin(zenith);
			randy = cos(azmuthial)*sin(zenith);
			randz = cos(zenith);
			
			dot = randx * ghost_vec->front().DX() + randy * ghost_vec->front().DY()  +randz * ghost_vec->front().DZ();
			
			randx -= ghost_vec->front().DX() * dot;
			randy -= ghost_vec->front().DY() * dot;
			randz -= ghost_vec->front().DZ() * dot;
			
			mag = sqrt(randx * randx + randy * randy + randz * randz);
			
			randx /= mag;
			randy /= mag;
			randz /= mag;
			
			angle = Ideal->GetAngle();
			costheta = cos(angle);
			sintheta = sin(angle);
			
			finalx = costheta * ghost_vec->front().DX() + sintheta * randx;
			finaly = costheta * ghost_vec->front().DY() + sintheta * randy;
			finalz = costheta * ghost_vec->front().DZ() + sintheta * randz;
			
				
			
			
			if(homo)
			{
				ray_vec->push_back(ray(ghost_vec->front().xyz.at(0),ghost_vec->front().xyz.at(1),ghost_vec->front().xyz.at(2),finalx,finaly,finalz,ghost_vec->front().P()*particle_transmittance,ghost_vec->front().Lambda(),ghost_vec->front().S()) );
				ray_vec->back().T(ghost_vec->front().T());
				ray_vec->back().oT(ghost_vec->front().T());
			}
			else
			{			
				ray_vec->push_back(ray(ParticleX.at(ghost_vec->front().prt())+finalx*ParticleRadius*1.01,ParticleY.at(ghost_vec->front().prt())+finaly*ParticleRadius*1.01,ParticleZ.at(ghost_vec->front().prt())+finalz*ParticleRadius*1.01,finalx,finaly,finalz,ghost_vec->front().P()*particle_transmittance,ghost_vec->front().Lambda(),ghost_vec->front().S()));
				ray_vec->back().T(ghost_vec->front().T() + ParticleRadius*1.01);
				ray_vec->back().oT(ghost_vec->front().T());
			}
			
			
			ray_vec->back().E(1,floor(ray_vec->back().X()/Xelem_size));
			ray_vec->back().E(2,floor(ray_vec->back().Y()/Yelem_size));
			ray_vec->back().E(3,floor(ray_vec->back().Z()/Zelem_size));	
			ray_vec->back().just_scatted(1);
			if(LogRays) {ghost_vec->front().GiveHistory(&(ray_vec->back())); ray_vec->back().SaveHistory(2);}
			if(ray_vec->back().X() > domainX || ray_vec->back().X() < 0  || ray_vec->back().Y() > domainY || ray_vec->back().Y() < 0 || ray_vec->back().Z() > domainZ || ray_vec->back().Z() < 0)
			{
				printf("shit scatt\n");
				ray_vec->back().obliterate();
			}
			
			ghost_vec->pop_front();
		}
		else
		{
			//printf("Ray entirely absorbed \n");
			
			ParticleQ.at(ghost_vec->front().prt()).at(trial) += ghost_vec->front().P()*(particle_transmittance); //put the rest of the powe in the particle
			
			int S = ghost_vec->front().S();  //deal with scattering counts
			if (S+1 > Scatt_vec.at(trial).size())
			{
				Scatt_vec.at(trial).resize(S +1);
				 
			}
			Scatt_vec.at(trial).at(S)+=1;
			
			ghost_vec->pop_front();
			
			return -3;
		}
		
		
	}
	else
	{
		return 1;
	}
	
	//printf("GREAT SUCESS \n");
	return 0;
	
}



int controller::Scatter_One_Ray_Mie_NoTP(int i, std::deque<ghost> *ghost_vec,std::vector<ray> *ray_vec, int trial)
{
	
	if (ghost_vec->size()>0)
	{	
		double chi;
		std::vector<double> parms;
		
		
		
		//homogenization
		bool homo = false;
		
		if(ghost_vec->front().prt() == -4)
		{
			homo = true;
			
			ghost_vec->front().prt(mesh_exact.at(ghost_vec->front().EX()).at(ghost_vec->front().EY()).at(ghost_vec->front().EZ()).at(floor(mesh_exact.at(ghost_vec->front().EX()).at(ghost_vec->front().EY()).at(ghost_vec->front().EZ()).size() * ranf(trial))));
			
		}
		
		//real
		
		
		parms = ParmVec(ghost_vec->front().prt(),ghost_vec->front().Lambda());
		
		Particle_Ideal * Ideal;
		
		Ideal = Optics.Ideal_Array.search(parms); 
		
		double particle_transmittance;
		
		if (homo)
		{
			particle_transmittance = 1;
		}
		else
		{
			 particle_transmittance = 1-(fabs(Ideal->Qa/Ideal->Qe));
			ParticleQ.at(ghost_vec->front().prt()).at(trial) += ghost_vec->front().P()*(1-particle_transmittance);
		}
		
		
		
		double ParticleRadius = ParticleParm(ghost_vec->front().prt(),0)/2.0;
		
		double randx;
		double randy;
		double randz;
		double dot, mag, angle;
		double finalx, finaly, finalz;
		double costheta, sintheta;
		double zenith, azmuthial;
		//printf("i be here");
		
		if(ghost_vec->front().P()*particle_transmittance > MinPowerThreshold)
		{
			//printf("Ray scattered \n");
			
			do
			{
				zenith =  M_PI*ranf(trial);
			}while (zenith==0.0);
			
			azmuthial = 2* 3.14159265 *ranf(trial);
			
			randx = sin(azmuthial)*sin(zenith);
			randy = cos(azmuthial)*sin(zenith);
			randz = cos(zenith);
			
			dot = randx * ghost_vec->front().DX() + randy * ghost_vec->front().DY()  +randz * ghost_vec->front().DZ();
			
			randx -= ghost_vec->front().DX() * dot;
			randy -= ghost_vec->front().DY() * dot;
			randz -= ghost_vec->front().DZ() * dot;
			
			mag = sqrt(randx * randx + randy * randy + randz * randz);
			
			randx /= mag;
			randy /= mag;
			randz /= mag;
			
			angle = Ideal->GetAngle();
			costheta = cos(angle);
			sintheta = sin(angle);
			
			finalx = costheta * ghost_vec->front().DX() + sintheta * randx;
			finaly = costheta * ghost_vec->front().DY() + sintheta * randy;
			finalz = costheta * ghost_vec->front().DZ() + sintheta * randz;
			
			if(std::isnan(finaly))
			{
				printf("well that explains it\n");
				
			}
			
			
			ray_vec->push_back(ray(ghost_vec->front().xyz.at(0),ghost_vec->front().xyz.at(1),ghost_vec->front().xyz.at(2),finalx,finaly,finalz,ghost_vec->front().P()*particle_transmittance,ghost_vec->front().Lambda(),ghost_vec->front().S()) );
			ray_vec->back().T(ghost_vec->front().T());
			ray_vec->back().oT(ghost_vec->front().T());
				
			
			
			//ray_vec->back().T(ghost_vec->front().T());
			/*ray_vec->back().E(1,ghost_vec->front().EX());
			ray_vec->back().E(2,ghost_vec->front().EY());
			ray_vec->back().E(3,ghost_vec->front().EZ());
			
			
			
			if (ray_vec->back().TestElem(Xelem_size,Yelem_size,Zelem_size) == true)
			{
				ray_vec->back().E(1,floor(ray_vec->back().X()/Xelem_size));
				ray_vec->back().E(2,floor(ray_vec->back().Y()/Yelem_size));
				ray_vec->back().E(3,floor(ray_vec->back().Z()/Zelem_size));
				//printf("We jumped an element\n");
			}*/	
				
			ray_vec->back().E(1,floor(ray_vec->back().X()/Xelem_size));
			ray_vec->back().E(2,floor(ray_vec->back().Y()/Yelem_size));
			ray_vec->back().E(3,floor(ray_vec->back().Z()/Zelem_size));	
			ray_vec->back().just_scatted(1);
			if(LogRays) {ghost_vec->front().GiveHistory(&(ray_vec->back())); ray_vec->back().SaveHistory(2);}
			if(ray_vec->back().X() > domainX || ray_vec->back().X() < 0  || ray_vec->back().Y() > domainY || ray_vec->back().Y() < 0 || ray_vec->back().Z() > domainZ || ray_vec->back().Z() < 0)
			{
				printf("shit scatt\n");
				ray_vec->back().obliterate();
			}
			
			
			
			
			ghost_vec->pop_front();
		}
		else
		{
			printf("Ray entirely absorbed \n");
			
			int S = ghost_vec->front().S();
			if (S+1 > Scatt_vec.at(trial).size())
			{
				Scatt_vec.at(trial).resize(S +1);
				 
			}
			Scatt_vec.at(trial).at(S)+=1;
			
			ghost_vec->pop_front();
			
			return -3;
		}
		
		
	}
	else
	{
		return 1;
	}
	
	//printf("GREAT SUCESS \n");
	return 0;
	
}
