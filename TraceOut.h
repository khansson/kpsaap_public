



#ifndef _TRACEOUT_
#define _TRACEOUT_


#include <stdio.h>


#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <deque>
#include "controller.h"
#include <stdlib.h>
#include <math.h> 
#include "ray.h"
#include "ranf.h"
#include "optics.h"
#include <algorithm>
#include <chrono>


struct tally
{
	tally() {sum = 0; sum_sqr = 0; count = 0;};
	double sum;
	double sum_sqr;
	int count;
	double mean() {return sum / count;};
	double std_dev() {return sqrt(count / (double)(count -1))*sqrt(fabs(sum * sum - sum_sqr))/sqrt((double)count);};
	tally& operator+=(const double& rhs){ 
            sum += rhs;
           sum_sqr += rhs * rhs;
           count++;
            return *this;
    };
    tally& operator+(const tally& rhs){ 
            sum += rhs.sum;
            sum_sqr += rhs.sum_sqr;
            count += rhs.count;
            return *this;
    };
};




class controller;

class TraceOut_Base
{
	public:
	
	virtual ~TraceOut_Base(){};
		
	controller *master;
	
	virtual double  TraceRayOut(ray *Ray, int trial) ;
	
	//ApplyBoundarys(ray *Ray)
	
	std::vector<std::vector<std::vector<std::vector<tally>>>> SensorMap; // trial, x, y, scatt 
 	std::vector<std::vector<double>> Lengths;
	
	void Initialize(controller * Master);
	
	int mapxsamples, mapysamples, mapscattsamples;
	double CollectionRadius, WindowSize, WindowOffset;
	
	
	virtual std::string name() {return "base";};
	
	virtual CollectionTypes type() {return NUM_COLLECT;};\
	
	double PowerSeen();
	double PowerSeen(int i);
	
	bool initialized;
	
	int WriteMap_tec();
	int WriteLegnthsHistogram();
	int WriteLegnthsRaw();
	virtual int WriteMap_vtk();
	int WriteMap_csv();
	int WriteTransmissionStats();
};


# define X(q) \
	class TraceOut_##q : public TraceOut_Base\
	{\
	double TraceRayOut(ray *Ray, int trial);\
	std::string name() {return #q;};\
	CollectionTypes type() {return q;};\
	};\
	
	
	COLLECT_TXT
# undef X
	













#endif





