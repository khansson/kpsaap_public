# Integrated Makefile for the MCRT software system.
# Uses file "Makefile.arch" for system specific declarations
#
# Last modified Sept. 11, 2014

SHELL = /bin/sh

# Necessary directories

#BASEDIR=/home/khansson/Reseach/KPSAAP
BASEDIR=$(PWD)/../..

BINDIR = $(BASEDIR)/bin
SRCDIR = $(BASEDIR)/src/kpsaap


ARCH       = LINUX
COMM       = SERIAL
#CC         = icc -fast
CC         = g++ -std=gnu++11 -Xpreprocessor -fopenmp -lomp
#-fopenmp
#CFLAGS     = -DDIM_3D -Wall -g -fno-strict-aliasing -U_FORTIFY_SOURCE 
#CFLAG Notes: Additional modeling flags:ifndef
#               -DDIM_3D -DUNSTEADY
#Use the following to debug with the gnu debugger
#CFLAGS     = -Wall -g



PROGNAME = KPSAAP

PROG    = $(PROGNAME)

# Targets...
# Targets...

default : KPSAAP

TARGETS = kpsaap.c controller.c simulation.c ray.c ranf.c optics.c scattering.c Bessel.c TraceOut.c tracing.c TraceIn.c WriteOut.c pugixml.cpp
OBJS      = $(TARGETS:.c=.o)
# Make the monaco executable and all utilities
#all :  monaco oxford util

#-lboost;\

kpsaap : $(OBJS)
	$(CC) -c -g $(TARGETS) -lm;  
	$(CC) -o $(PROG) $(OBJS) -lm -L/usr/local/lib/;\
        rm $(SRCDIR)/*.o
	mkdir -p $(BINDIR)/Test/;\
	/bin/cp -f $(PROG) $(BINDIR)/Test/;
	/bin/mv -f $(PROG) $(BINDIR);


kpsaapLR : $(OBJS)
	$(CC)  -DLOGRAYS -c  $(TARGETS) -lm;\
	$(CC)  -DLOGRAYS -o  $(PROG) $(OBJS) -lm;\
	/bin/mv -f $(PROG) $(BINDIR);
	
kpsaapT : $(OBJS)
	$(CC)  -DTIMER -c $(TARGETS) -lm;\
	$(CC)  -DTIMER -o $(PROG) $(OBJS) -lm;\
	/bin/mv -f $(PROG) $(BINDIR);
	
kpsaapLRtest : $(OBJS)
	$(CC) -E -DLOGRAYS  $(TARGETS) -lm
	


# Cleanup options

# Remove object files from directories related to mcrt executable
clean :



# Remove all object files, libraries and executables from entire directory tree
# Basically resets to the original mcrt distribution state
distclean : clean
	cd $(BINDIR); /bin/rm -f *

.c.o :
	$(CC) $(CFLAGS) -c $<

# DO NOT DELETE
