

#include <stdio.h>


#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <deque>
#include "controller.h"
#include <stdlib.h>
#include <math.h> 
#include "ray.h"
#include "ranf.h"
#include "optics.h"
#include <algorithm>
#include <chrono>



int controller::BoundaryOut(ray *Ray, int trial)
{
	int dir;
	int direction = 6;
	int i,j;
	
	for (dir = 1;dir<=3;dir++)
	{
			
		if (Ray->E(dir) < 0)
		{
			direction = dir*2 - 1;
			
		}
			
		if (Ray->E(dir) >= domain_elem(dir))
		{
			direction = dir*2-2;
				
		}
	}

	if (direction == 6)
	{
		printf("freak the hell out\n");
	}
	
	
	
	dir = direction/2+1;
	int sign = -1*direction%2;
	
	if (bounds[direction] == Periodic)
	{
		
		//printf("test\n");
		
		if (sign == -1)
		{
			Ray->R(dir,domain(dir)) ;
			Ray->E(dir, domain_elem(dir)-1) ;
			Ray->resurrecte();
		}
		else
		{
			Ray->R(dir,0) ;
			Ray->E(dir, 0) ;
			Ray->resurrecte();
		}
		
		return 1;
	}
	
	//printf("ping, %d, %d\n",bounds[direction], direction);
	
	if (bounds[direction] == Reflect_Full)
	{
		
		Ray->D(dir,Ray->D(dir)*-1) ;
		Ray->resurrecte();
		
		return 1;
		
		
	}
	
	ray *RayII;
	
	if (bounds[direction] == Partial_Reflect)
	{
		RayII = new ray();
		
		*RayII = *Ray;
		
		Ray->attenuate(Borosilicate_Reflectance( Ray->Lambda()));
		RayII->attenuate(1-Borosilicate_Reflectance( Ray->Lambda()));
		
		Ray->D(dir,Ray->D(dir)*-1) ;
		Ray->resurrecte();
		
		Ray = RayII;
		
	}
	
	
	
	
	
		std::vector<double> times(6); // txup,txdown,tyup,tydown,tzup,tzdown;
		std::vector<double>::iterator result;
		int index=6;
		double keepp;
		
		
		
		times.at(0) = (Ray->DX()>0?fabs(domain_ext[0] - Ray->X())/(Ray->DX()):-1);
		times.at(1) = (Ray->DX()<0?-fabs(domain_ext[1] - Ray->X())/(Ray->DX()):-1);
		times.at(2) = (Ray->DY()>0?fabs(domain_ext[2] - Ray->Y())/(Ray->DY()):-1);
		times.at(3) = (Ray->DY()<0?-fabs(domain_ext[3] - Ray->Y())/(Ray->DY()):-1);
		times.at(4) = (Ray->DZ()>0?fabs(domain_ext[4] - Ray->Z())/(Ray->DZ()):-1);
		times.at(5) = (Ray->DZ()<0?-fabs(domain_ext[5] - Ray->Z())/(Ray->DZ()):-1);
		keepp = domain_ext[0] + domain_ext[2] + domain_ext[4];
		
		
		
		
		for(i = 0; i<times.size() ; i++)
		{
			
			if (times.at(i) >= 0 && times.at(i) < keepp)
			{
				index = i;
				keepp = times.at(i);
			}
			
		}
		
		//printf("index: %d\n",index);
		
		if(index == 6)
		{
			printf("our worst fears \n");
			for(i = 0;i< 6;i++)
			{
				printf("%f,",times.at(i));
			}
			printf("\n");
			if(LogRays) {Ray->SaveHistory(3);}
			Ray->obliterate();
			return 0;
			
		}
		
		
		
		
		double x = Ray->X() + times.at(index) * (Ray->DX());
		double y = Ray->Y() + times.at(index) * (Ray->DY());
		double z = Ray->Z() + times.at(index) * (Ray->DZ());
		
		double dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
		
		
		
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
	
		Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
		
		for(j = 0;j<TraceOuts.size();j++)
		{
			TraceOuts.at(j)->TraceRayOut(Ray,trial);
		}
	
	return 1;
}


void TraceOut_Base::Initialize(controller *Master)
{
	
	master = Master;
	if (!initialized)
	{
		mapxsamples = master->mapxsamples;
		mapysamples = master->mapysamples;
		mapscattsamples = master->mapscattsamples;
		CollectionRadius = master->CollectionRadius;
		WindowOffset= master->WindowOffset;
		WindowSize= master->WindowSize;
	}
	
	int i,j,k;
	
	
	
	if(master->PathLengths)
	{
		Lengths.resize(master->NUM_TRIALS);
		for(i = 0;i< master->NUM_TRIALS;i++)
		{
			Lengths.at(i).reserve(master->NUM_RAYS);
		}
	}
	
	SensorMap.resize(master->NUM_TRIALS);
	
	for(i = 0; i<SensorMap.size(); i++)
	{
		SensorMap.at(i).resize(mapysamples);
		for(j = 0; j<SensorMap.at(i).size(); j++)
		{
			SensorMap.at(i).at(j).resize(mapxsamples);
			for(k = 0; k<SensorMap.at(i).at(j).size(); k++)
			{
				SensorMap.at(i).at(j).at(k).resize(mapscattsamples);
			}
		}
	}
	
}


double TraceOut_Base::PowerSeen()
{
	double sum=0;
	int i,j,k,l;
	for(i = 0; i<SensorMap.size(); i++)
	{
		
		for(j = 0; j<SensorMap.at(i).size(); j++)
		{
			
			for(k = 0; k<SensorMap.at(i).at(j).size(); k++)
			{
				for(l = 0; l<SensorMap.at(i).at(j).at(k).size(); l++)
				{
					sum +=SensorMap.at(i).at(j).at(k).at(l).sum;
				}
				
			}
		}
	}
	
	return sum;

}






double TraceOut_Base::PowerSeen(int i)
{
	double sum=0;
	int j,k,l;
	
		
		for(j = 0; j<SensorMap.at(i).size(); j++)
		{
			
			for(k = 0; k<SensorMap.at(i).at(j).size(); k++)
			{
				for(l = 0; l<SensorMap.at(i).at(j).at(k).size(); l++)
				{
					sum +=SensorMap.at(i).at(j).at(k).at(l).sum;
				}
				
			}
		}
	
	
	return sum;

}


double TraceOut_Base::TraceRayOut(ray *Ray, int trial)
{
	
	
	if(Ray->Y() < master->domainY && Ray->Y()  > 0 && Ray->X() < master->domainX && Ray->X() > 0)
	{
	
		SensorMap.at(trial).at(floor(Ray->Y()/master->domainY*mapysamples)).at(floor(Ray->X()/master->domainX*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		
		return Ray->Power();
		
	}
	return 0;
}




double TraceOut_Panel_C::TraceRayOut(ray *Ray, int trial)
{
	bool flag = true;
	int dir;
	double dis;
	/*
	for (dir = 1;dir<=3;dir++)
	{
		if (Ray->E(dir) < 0)
		{
			Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()), DISTANCE(master->glass_thickness,master->glass_thickness/Ray->D(dir)*Ray->D((dir+1)%3),master->glass_thickness/Ray->D(dir)*Ray->D((dir+2)%3),0.0,0.0,0.0));
			flag = false;
			break;
			
		}
		
		if (Ray->E(dir) >= master->domain_elem(dir))
		{
			Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()), DISTANCE(master->glass_thickness,master->glass_thickness/Ray->D(dir)*Ray->D((dir+1)%3),master->glass_thickness/Ray->D(dir)*Ray->D((dir+2)%3),0.0,0.0,0.0));
			flag = false;
			break;
			
		}
	}
	
	
	if (flag)
	{
		printf("Something is rather screwed up");		
	}
	*/
	
	
	double y = Ray->DY() * (master->domain_ext[4]-Ray->Z())/Ray->DZ() + Ray->Y();
	double x = Ray->DX() * (master->domain_ext[4]-Ray->Z())/Ray->DZ() + Ray->X();
	double z = master->domain_ext[4];
	//printf("impact?");
	if(y < master->domainY && y > 0 && x < master->domainX && x > 0)
	{
		//printf("impact!");
		dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
		Ray->aT(dis);		
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		
		SensorMap.at(trial).at(floor(y/master->domainY*mapysamples)).at(floor(x/master->domainX*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		if(master->PathLengths)
		{
			Lengths.at(trial).push_back(Ray->T());
			
			if(Lengths.size() == Lengths.capacity())
			{
				Lengths.reserve(Lengths.size() + 1000);
			}
			
			
			
		}
		return Ray->Power();
			
	}	
	
	return 0;

}

/*

double TraceOut_Panel_Reflection_C::TraceRayOut(ray *Ray, int trial)
{
	bool flag = true;
	int dir;
	double r = ranf(trial);
	
	if (Borosilicate_Reflectance( Ray->Lambda()) < r)
	{
		for (dir = 1;dir<=3;dir++)
		{
			if (Ray->E(dir) < 0)
			{
				Ray->D(dir,-1 * Ray->D(dir)) ;
				Ray->E(dir, Ray->E(dir)+1) ;
				flag = false;
				return 0;
				
			}
			
			if (Ray->E(dir) >= master->domain_elem(dir))
			{
				Ray->D(dir,-1 * Ray->D(dir)) ;
				Ray->E(dir, Ray->E(dir)-1) ;
				flag = false;
				return 0; 
				
			}
		}
	}
	
	
	double dis;
	double y = Ray->DY() * (master->glass_thickness+master->domainZ-Ray->Z())/Ray->DZ() + Ray->Y();
	double x = Ray->DX() * (master->glass_thickness+master->domainZ-Ray->Z())/Ray->DZ() + Ray->X();
	double z = master->glass_thickness+master->domainZ;
	if(y < master->domainY && y > 0 && x < master->domainX && x > 0)
	{
		//printf("impact!");
		dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
		Ray->aT(dis);		
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		
		SensorMap.at(trial).at(floor(y/master->domainY*mapysamples)).at(floor(x/master->domainX*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		return Ray->Power();
	}	
	
	return 0;

}
*/


double TraceOut_Sphere_C::TraceRayOut(ray *Ray, int trial)
{
	bool flag = true;
	int dir;
	
	/*
	for (dir = 1;dir<=3;dir++)
	{
		if (Ray->E(dir) < 0)
		{
			Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()), DISTANCE(master->glass_thickness,master->glass_thickness/Ray->D(dir)*Ray->D((dir+1)%3),master->glass_thickness/Ray->D(dir)*Ray->D((dir+2)%3),0.0,0.0,0.0));
			flag = false;
			break;
			
		}
		
		if (Ray->E(dir) >= master->domain_elem(dir))
		{
			Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()), DISTANCE(master->glass_thickness,master->glass_thickness/Ray->D(dir)*Ray->D((dir+1)%3),master->glass_thickness/Ray->D(dir)*Ray->D((dir+2)%3),0.0,0.0,0.0));
			flag = false;
			break;
			
		}
	}
	
	
	if (flag)
	{
		printf("Something is rather screwed up");		
	}
	
	*/
	
	double zenith;
	double lat = sqrt((Ray->X() - master->domainX/2.0)*(Ray->X() - master->domainX/2.0) + (Ray->Y() - master->domainY/2.0)*(Ray->Y() - master->domainY/2.0));
	double dist = sqrt(lat*lat + (Ray->Z() - master->domainZ/2.0)*(Ray->Z() - master->domainZ/2.0));
	double phi = acos(Ray->DZ());
	double psi = atan((Ray->Z() - master->domainZ/2.0)/lat);
	
	zenith = asin(dist*sin(psi+phi+M_PI/2)/master->CollectionRadius)+phi;
	/*
	if(Ray->DZ<0)
	{
		zenith = M_PI - zenith;
	}*/
	
	if(zenith<0)
	{
		zenith += 2*M_PI;
	}
	
	
	if(zenith>=M_PI)
	{
		zenith -= 2*M_PI;
	}
	
		lat = Ray->X() - master->domainX/2.0;
	dist = sqrt(lat*lat + (Ray->Y() - master->domainY/2.0)*(Ray->Y() - master->domainY/2.0));
	phi = atan2(Ray->DY(),Ray->DX());
	psi = atan2((Ray->Y() - master->domainY/2.0),lat);
	
	double azmuthial = asin(dist*sin(psi-phi+M_PI/2)/master->CollectionRadius)+phi-psi;
	if(azmuthial<0)
	{
		azmuthial += 2*M_PI;
	}
	if(azmuthial>=2*M_PI)
	{
		azmuthial -= 2*M_PI;
	}
	if(zenith>1&&zenith<2)
	{
		lat = 2;
	}
	
	double x,y,z,dis;
	
	if(true)
	{
		//printf("impact!");
		x = sin(azmuthial)*sin(zenith)*master->CollectionRadius;
		y = cos(azmuthial)*sin(zenith)*master->CollectionRadius;
		z = cos(zenith)*master->CollectionRadius;
		dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
		Ray->aT(dis);	
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		SensorMap.at(trial).at(floor(zenith/M_PI*mapysamples)).at(floor(azmuthial/2/M_PI*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		SensorMap.at(trial).at(floor(y/master->domainY*mapysamples)).at(floor(x/master->domainX*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		if(master->PathLengths)
		{
			Lengths.at(trial).push_back(Ray->T());
			
			if(Lengths.size() == Lengths.capacity())
			{
				Lengths.reserve(Lengths.size() + 1000);
			}
			
			
			
		}
		return Ray->Power();
	}	
	
	return 0;

}


double TraceOut_Sphere_Inf_C::TraceRayOut(ray *Ray, int trial)
{
	bool flag = true;
	int dir;
	/*
	for (dir = 1;dir<=3;dir++)
	{
		if (Ray->E(dir) < 0)
		{
			Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()), DISTANCE(master->glass_thickness,master->glass_thickness/Ray->D(dir)*Ray->D((dir+1)%3),master->glass_thickness/Ray->D(dir)*Ray->D((dir+2)%3),0.0,0.0,0.0));
			flag = false;
			break;
			
		}
		
		if (Ray->E(dir) >= master->domain_elem(dir))
		{
			Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()), DISTANCE(master->glass_thickness,master->glass_thickness/Ray->D(dir)*Ray->D((dir+1)%3),master->glass_thickness/Ray->D(dir)*Ray->D((dir+2)%3),0.0,0.0,0.0));
			flag = false;
			break;
			
		}
	}
	
	
	if (flag)
	{
		printf("Something is rather screwed up");		
	}
	*/
	
	
	double zenith;
	double lat = sqrt((Ray->X() - master->domainX/2.0)*(Ray->X() - master->domainX/2.0) + (Ray->Y() - master->domainY/2.0)*(Ray->Y() - master->domainY/2.0));
	double dist = sqrt(lat*lat + (Ray->Z() - master->domainZ/2.0)*(Ray->Z() - master->domainZ/2.0));
	double phi = acos(Ray->DZ());
	double psi = atan((Ray->Z() - master->domainZ/2.0)/lat);
	
	zenith = phi;
	/*
	if(Ray->DZ<0)
	{
		zenith = M_PI - zenith;
	}*/
	
	if(zenith<0)
	{
		zenith += 2*M_PI;
	}
	
	
	if(zenith>=M_PI)
	{
		zenith -= 2*M_PI;
	}
	
	lat = Ray->X() - master->domainX/2.0;
	dist = sqrt(lat*lat + (Ray->Y() - master->domainY/2.0)*(Ray->Y() - master->domainY/2.0));
	phi = atan2(Ray->DY(),Ray->DX());
	psi = atan2((Ray->Y() - master->domainY/2.0),lat);
	
	double azmuthial = phi;
	if(azmuthial<0)
	{
		azmuthial += 2*M_PI;
	}
	if(azmuthial>=2*M_PI)
	{
		azmuthial -= 2*M_PI;
	}
	if(zenith>1&&zenith<2)
	{
		lat = 2;
	}
	
	double x,y,z,dis;
	
	if(true)
	{
		//printf("impact!");
		x = sin(azmuthial)*sin(zenith)*master->CollectionRadius;
		y = cos(azmuthial)*sin(zenith)*master->CollectionRadius;
		z = cos(zenith)*master->CollectionRadius;
		dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
		Ray->aT(dis);	
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		
		
		SensorMap.at(trial).at(floor(zenith/M_PI*mapysamples)).at(floor(azmuthial/2/M_PI*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		SensorMap.at(trial).at(floor(y/master->domainY*mapysamples)).at(floor(x/master->domainX*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		if(master->PathLengths)
		{
			Lengths.at(trial).push_back(Ray->T());
			
			if(Lengths.size() == Lengths.capacity())
			{
				Lengths.reserve(Lengths.size() + 1000);
			}
			
			
			
		}
		return Ray->Power();
		
	}	
	
	return 0;

}
/*

double TraceOut_Offset_Panel_Reflection_C::TraceRayOut(ray *Ray, int trial)
{
	
	
	
	bool flag = true;
	int dir;
	double r = ranf(trial);
	
	if (Borosilicate_Reflectance( Ray->Lambda()) < r)
	{
		for (dir = 1;dir<=3;dir++)
		{
			if (Ray->E(dir) < 0)
			{
				Ray->D(dir,-1 * Ray->D(dir)) ;
				Ray->E(dir, Ray->E(dir)+1) ;
				flag = false;
				return 0;
				
			}
			
			if (Ray->E(dir) >= master->domain_elem(dir))
			{
				Ray->D(dir,-1 * Ray->D(dir)) ;
				Ray->E(dir, Ray->E(dir)-1) ;
				flag = false;
				return 0; 
				
			}
		}
	}
	
	

	
	double offset = master->WindowOffset;
	double Window = master->WindowSize;
	double dis;
	double y = Ray->DY() * (offset+master->glass_thickness+master->domainZ-Ray->Z())/Ray->DZ() + Ray->Y();
	double x = Ray->DX() * (offset+master->glass_thickness+master->domainZ-Ray->Z())/Ray->DZ() + Ray->X();
	double z = offset +master->glass_thickness+master->domainZ;
	if(y < (master->domainY+Window)/2 && y > (master->domainY-Window)/2 && x < (master->domainX+Window)/2  && x > (master->domainX-Window)/2)
	{
		//printf("impact!");
		dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
		Ray->aT(dis);		
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		SensorMap.at(trial).at(floor((y-(master->domainY-Window)/2)/Window*mapysamples)).at(floor((x-(master->domainX-Window)/2)/Window*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		return Ray->Power();
	}	
	
	return 0;
	
	
	
	
}*/

double TraceOut_Offset_Panel_C::TraceRayOut(ray *Ray, int trial)
{
	bool flag = true;
	int dir;
	
	/*
	for (dir = 1;dir<=3;dir++)
	{
		if (Ray->E(dir) < 0)
		{
			Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()), DISTANCE(master->glass_thickness,master->glass_thickness/Ray->D(dir)*Ray->D((dir+1)%3),master->glass_thickness/Ray->D(dir)*Ray->D((dir+2)%3),0.0,0.0,0.0));
			flag = false;
			break;
			
		}
		
		if (Ray->E(dir) >= master->domain_elem(dir))
		{
			Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()), DISTANCE(master->glass_thickness,master->glass_thickness/Ray->D(dir)*Ray->D((dir+1)%3),master->glass_thickness/Ray->D(dir)*Ray->D((dir+2)%3),0.0,0.0,0.0));
			flag = false;
			break;
			
		}
	}
	
	
	if (flag)
	{
		printf("Something is rather screwed up");		
	}
	*/
	double offset = master->WindowOffset;
	double Window = master->WindowSize;
	double dis;
	double y = Ray->DY() * (offset+master->glass_thickness+master->domainZ-Ray->Z())/Ray->DZ() + Ray->Y();
	double x = Ray->DX() * (offset+master->glass_thickness+master->domainZ-Ray->Z())/Ray->DZ() + Ray->X();
	double z = offset +master->glass_thickness+master->domainZ;
	if(y < (master->domainY+Window)/2 && y > (master->domainY-Window)/2 && x < (master->domainX+Window)/2  && x > (master->domainX-Window)/2)
	{
		//printf("impact!");
		dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
		Ray->aT(dis);		
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		
		SensorMap.at(trial).at(floor((y-(master->domainY-Window)/2)/Window*mapysamples)).at(floor((x-(master->domainX-Window)/2)/Window*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		SensorMap.at(trial).at(floor(y/master->domainY*mapysamples)).at(floor(x/master->domainX*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		if(master->PathLengths)
		{
			Lengths.at(trial).push_back(Ray->T());
			
			if(Lengths.size() == Lengths.capacity())
			{
				Lengths.reserve(Lengths.size() + 1000);
			}
			
			
			
		}
		return Ray->Power();
		
	}	
	
	return 0;

}




/*

double TraceOut_Panel_Periodic_XY_C::TraceRayOut(ray *Ray, int trial)
{
	
	
	
	bool flag = true;
	int dir;
	double r = ranf(trial);
	
	
	for (dir = 1;dir<=3;dir++)
		{
			printf("%d,",Ray->E(dir));
		}
	printf("\n");
	
	if (true)
	{
		for (dir = 1;dir<=2;dir++)
		{
			//printf("%d,",Ray->E(dir));
			if (Ray->E(dir) < 0)
			{
				Ray->R(dir,master->domain(dir)) ;
				Ray->E(dir, master->domain_elem(dir)-1) ;
				flag = false;
				//printf("down\n");
				Ray->resurrecte();
				return 0;
				
			}
			
			if (Ray->E(dir) >= master->domain_elem(dir))
			{
				Ray->R(dir,0) ;
				Ray->E(dir, 0) ;
				flag = false;
				//printf("up\n");
				Ray->resurrecte();
				return 0; 
				
			}
		}
	}
	
	

	double dis;
	double y = Ray->DY() * (master->glass_thickness+master->domainZ-Ray->Z())/Ray->DZ() + Ray->Y();
	double x = Ray->DX() * (master->glass_thickness+master->domainZ-Ray->Z())/Ray->DZ() + Ray->X();
	double z = master->glass_thickness+master->domainZ;
	if(y < master->domainY && y > 0 && x < master->domainX && x > 0)
	{
		//printf("impact!");
		dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
		Ray->aT(dis);		
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		SensorMap.at(trial).at(floor(y/master->domainY*mapysamples)).at(floor(x/master->domainX*mapxsamples)).at(Ray->S()<mapscattsamples? Ray->S() : mapscattsamples - 1) += Ray->Power();
		
		return Ray->Power();
	}	
	
	
	
	return 0;
	
	
	
	
}*/
