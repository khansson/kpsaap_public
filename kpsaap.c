/*
 * kpsaap.c
 * 
 * Copyright 2017 Kaelan Hansson <khansson@HP-Z820-Workstation>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */



#include "controller.h"
#include <stdio.h>
#include "ranf.h"
#include <time.h>
#include <string>
#include "pugixml.hpp"
#include <chrono>


//FLOW IN X, RAYS IN Z

int main(int argc, char **argv)
{
	controller cntrl;
	
	int j;
	
	std::string arg, partfile;
	int thing = 0;
	bool datfile = false;
	if(argc >1)
	{
		for(j = 1; j<argc;j ++)
		{
			arg = argv[j];
			if (arg == "--export" || arg == "-e")
			{
				cntrl.ExportConfigData();
				return 9000;
			}
			
			if (arg == "--xml" || arg == "-x")
			{
				
				thing = 2;
			}
			
			if (arg == "--review" || arg == "-r")
			{
				
				thing = 3;
			}
			
			if (arg == "--old" || arg == "-o")
			{
				
				datfile = true;
			}
			
			
			
			if (arg == "--adjust" || arg == "-a")
			{
				if (j!=1)
				{
					
					printf(" -a option can only be used as first option\n");
					continue;
				}
				cntrl.ChangeXMLConfig(argc, argv);
				return 600;
			}
			
			if (arg == "--decryptconfig" || arg == "-y")
			{
				if (j!=1)
				{
					
					printf(" -y option can only be used as first option\n");
					continue;
				}
				cntrl.UpdateXMLDecrypt(argc, argv);
				return 600;
			}
			
			
			
			if (arg == "-d" && argc > j + 1)
			{
				partfile = argv[j+1];
				printf("%s\n",argv[j+1]);
				thing = 1;
				j++;
			}
			
			if ((arg == "--seed" || arg == "-s") && argc > j + 1)
			{
				ranset(atof(argv[j+1] ) * 0x2C4856AD);
				printf("%s\n",argv[j+1]);
				j++;
			}
			
			if ((arg == "--random" || arg == "-d") && argc > j )
			{
				std::chrono::high_resolution_clock::time_point t2;
				std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1-t2);
				ranset(time_span.count());
				printf("%d\n",time_span.count());
				j++;
			}
			
			if (arg == "-t" && argc > j + 1)
			{				
				cntrl.TimeStamp = atof(argv[j+1]);
				printf("%s\n",argv[j+1]);
				j++;
			}
			else
			{
				cntrl.TimeStamp = 0.0;
			}
		}
	}
	
	
	
	
	
	printf("---  ---  ---  Welcome to the KPSAAP Jungle  ---  ---  --- \n");
	
	printf(__DATE__);
	printf("\n");
	
	
	cntrl. start = time(0);
	
	if (thing ==2)
	{
		cntrl.ReadXMLConfigFile("KPSAAP_CONFIG.xml");
		cntrl.WriteXMLConfig("outfile.xml");
		return 900000;

		
	}
	
	
	if(datfile)
	{
		if(cntrl.ReadConfigFile("KPSAAP_CONFIG.dat"))
		{
			return 0;
		}
	}
	else
	{
		
		if(cntrl.ReadXMLConfigFile("KPSAAP_CONFIG.xml"))
		{
			return 0;
		}
	}
		
	
	#if LOGRAYS
	printf("My God it's full of stars\n");
	cntrl.RAYLOG = fopen ("RayLog.csv" , "w");
	#endif
	
	#if TIMER
	cntrl.TIMERFILE = fopen ("Times.csv" , "w");s
	#endif
	
	printf("Reading Data Files \n");
	
	
	
	
	cntrl.ReadInitalTemperatures = false;
	if (thing != 1)
	{
		if(cntrl.ReadParticleData("../parts.csv"))
		{
			printf("All is lost, ending \n ");
			return 0;
		}
	}
	else
	{
		if(cntrl.ReadParticleData(&partfile[0]))
		{
			printf("All is lost, ending \n ");
			return 0;
		}
	}
	
	printf("Read Complete \n");
	
	
	
	//Things that will need to go into the config file
	//cntrl.InitiateDomain(.16,320,.04,80,.04,80,.006);
	//cntrl.InitiateDomain(.16,2000,.04,500,.04,35,.002);
	//                    X   nX  Y  nY  Z,  nZ  G
	
	
	//for Gaussian Beam
	//cntrl.Offset_Particles(-.04,-.02,0.0);
	//cntrl.InitiateDomain(.005,60,.005,60,.04,35,.002);
	//cntrl.InitiateDomain(.005,5,.005,5,.04,10,.002);
	
	//cntrl.InitiateDomain(.001,1,.001,1,.001,1,0.0);
	
	//
	/*
	cntrl.EmissionType = 0; // 0 = panel 1 = beam
	cntrl.CollectionType = 0; // 1 = shpere 0 = panel
	cntrl.ScatFunc = One_Ray_Mie;
	cntrl.iteration_limit = 50;
	cntrl.initial_temp = 300; //K
	cntrl.prt_cp = 50;// J/K
	cntrl.MonitorPhase = false;
	*/
	
	
	cntrl.InitiateDomain();
	cntrl.Sort_Particles();
	
			
	cntrl.BeginSimulation(cntrl.NUM_TRIALS,cntrl.NUM_RAYS);
	
	printf("Writing particle heats \n");
	
	//cntrl.WriteHeatsII();
	//384cntrl.WriteHeatsTEC_ASCII();
	
	
	if (thing ==3)
	{
		
		cntrl.WriteXMLConfig("KPSAAP_CONFIG.xml");
		return 900000;

		
	}
	
	cntrl.WriteData();
	
	
	//printf("\n\n ----  %f  seconds have elapsed ----\n", TimeElapsed);
	#if LOGRAYS	
	fclose(cntrl.RAYLOG);
	#endif
	
	#if TIMER	
	fclose(cntrl.TIMERFILE);
	#endif
	
	cntrl.end = time(0);
	cntrl.TimeElapsed = difftime(cntrl.end,cntrl.start);
	printf("\n\n ----  %i minutes and %i seconds have elapsed ----\n\n\n\ns", (int) floor(cntrl.TimeElapsed/60), (int) round(cntrl.TimeElapsed - 60*floor(cntrl.TimeElapsed/60)));
	//cntrl.print();
	
	cntrl.clean();
	
	return 0;
}

