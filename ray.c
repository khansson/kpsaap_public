


#include <stdio.h>


#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "controller.h"
#include <stdlib.h>
#include <math.h> 
#include "ray.h"



bool ray::TestDomain(double xx,double yy, double zz) 
{
	if (x<0.0||x > xx|| y < 0.0 || y >yy ||z < 0.0 || z >zz) 
	{
		printf("tracein failure?\n"); 
		return false;
	} 
	else
	{
		return true;
	}
}

void ray::SaveHistory( int i)
{
	std::vector<double> line;
	
	line.push_back(x);
	line.push_back(y);
	line.push_back(z);
	
	line.push_back(elemX);
	line.push_back(elemY);
	line.push_back(elemZ);
	line.push_back(travel);
	line.push_back(i);
	
	history.push_back(line);
	
	if (history.capacity()- history.size() < 1)
	{
		history.reserve(history.size() + 1000);
	}
	
}

void ray::SaveHistory( int i, int j)
{
	std::vector<double> line;
	
	line.push_back(x);
	line.push_back(y);
	line.push_back(z);
	
	line.push_back(elemX);
	line.push_back(elemY);
	line.push_back(elemZ);
	line.push_back(travel);
	line.push_back(scatt);
	line.push_back(i);
	line.push_back(j);
	
	history.push_back(line);
	
	if (history.capacity()- history.size() < 1)
	{
		history.reserve(history.size() + 1000);
	}
	
}

void ray::PrintData()
{
	printf("Ray Data:");
	
	printf("Position:: %f,%f,%f",x,y,z);
	printf("Direction:: %f,%f,%f",dx,dy,dz);
	printf("Element:: %d,%d,%d",elemX,elemY,elemZ);
	
	
}

void ray::WriteHistory(int count, int trial)
{
	
	char buffer[50];
	int i, j;
	FILE * FPT;
	sprintf(buffer,"Ray_%d_%d.csv",count,trial);
    FPT = fopen(buffer,"w");
	
	
	
	for (i = 0 ;i<history.size();i++)
	{
		for(j = 0; j < history.at(i).size(); j++)
		{
			fprintf(FPT,"%f,",history.at(i).at(j));
		}
		
		fprintf(FPT,"\n");
	}
	
	fclose(FPT);
}

ray::ray(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L)
{
	x = X;
	y= Y;
	z = Z;
	dx = Dx;
	dy = Dy;
	dz = Dz;
	power = P;
	lambda = L;
	travel = 0.0;
	alive = true;
	scatt = 0;
	
}

ray::ray(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L, double T)
{
	x = X;
	y= Y;
	z = Z;
	dx = Dx;
	dy = Dy;
	dz = Dz;
	power = P;
	lambda = L;
	travel  = T;
	alive = true;
	scatt = 0;
	
}

ray::ray(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L, int S)
{
	x = X;
	y= Y;
	z = Z;
	dx = Dx;
	dy = Dy;
	dz = Dz;
	power = P;
	lambda = L;
	travel = 0.0;
	alive = true;
	scatt = S;
	
}

ray::ray(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L, double T, int S)
{
	x = X;
	y= Y;
	z = Z;
	dx = Dx;
	dy = Dy;
	dz = Dz;
	power = P;
	lambda = L;
	travel  = T;
	alive = true;
	scatt = S;
	
}


void ray::fill(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L)
{
	x = X;
	y= Y;
	z = Z;
	dx = Dx;
	dy = Dy;
	dz = Dz;
	power = P;
	lambda = L;
	travel = 0.0;
	old_travel = 0.0;
	alive = true;
	
}

void ray::fill(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L, double T)
{
	x = X;
	y= Y;
	z = Z;
	dx = Dx;
	dy = Dy;
	dz = Dz;
	power = P;
	lambda = L;
	travel  = T;
	alive = true;
	
}



//direction
double ray::D(int i)
{
	i = (i-1)%3+1;
	if (i ==1)
	{
		return dx;
	}
	if (i ==2)
	{
		return dy;
	}
	if (i ==3)
	{
		return dz;
	}	
	else  {return 0;}
}


//read position
double ray::R(int i)
{
	i = (i-1)%3+1;
	if (i ==1)
	{
		return x;
	}
	if (i ==2)
	{
		return y;
	}
	if (i ==3)
	{
		return z;
	}	
	else  {return 0;}
}

//wrtie position
void ray::R(int i, double r)
{
	i = (i-1)%3+1;
	if (i ==1)
	{
		x = r;
	}
	if (i ==2)
	{
		y = r;
	}
	if (i ==3)
	{
		z = r;
	}	
}

//wrtie direction
void ray::D(int i, double r)
{
	i = (i-1)%3+1;
	if (i ==1)
	{
		dx = r;
	}
	if (i ==2)
	{
		dy = r;
	}
	if (i ==3)
	{
		dz = r;
	}	
}
	
//read element
int ray::E(int i)
{
	i = (i-1)%3+1;
	if (i ==1)
	{
		return elemX;
	}
	if (i ==2)
	{
		return elemY;
	}
	if (i ==3)
	{
		return elemZ;
	}	
	else  {return 0;}
}

//write element
void ray::E(int i, int e)
{
	i = (i-1)%3+1;
	if (i ==1)
	{
		elemX = e;
	}
	if (i ==2)
	{
		elemY = e;
	}
	if (i ==3)
	{
		elemZ = e;
	}	

}
void ray::set_elem( int X, int Y, int Z)
{
	elemX = X;
	elemY = Y;
	elemZ = Z;
	
	
}


double exp1(double x) 
{
  x = 1.0 + x / 256.0;
  x *= x; x *= x; x *= x; x *= x;
  x *= x; x *= x; x *= x; x *= x;
  return x;
}


void ray::attenuate(double beta, double dist)
{
	
	power = power * exp1(-beta * dist);
	travel += dist;
}



ghost::ghost()
{
	
}


/*ghost::ghost(int Particle)
{

	particle = Particle;
	

}*/


void ghost::ReadRay(ray Ray, int Particle, bool TakePos)
{
	dx = Ray.DX();
	dy = Ray.DY();
	dz = Ray.DZ();
	
	history = Ray.history;
	
	power = Ray.Power();
	lambda = Ray.Lambda();
	
	elemX = Ray.EX();
	elemY = Ray.EY();
	elemZ = Ray.EZ();
	
	scatt = Ray.S() + 1;
	
	particle = Particle;
	
	travel = Ray.T();
	
	if (particle == -4 || TakePos)
	{
		xyz.push_back(Ray.X());
		xyz.push_back(Ray.Y());
		xyz.push_back(Ray.Z());
		
			
	}
	
}



//sort by associated particle number
bool GhostSort(ghost a, ghost b)
{
	return a.prt()>b.prt();	
}


int ray::TestElem(double Xelem_size,double Yelem_size,double Zelem_size)
{
	if (x < Xelem_size * (elemX - .00000001 )|| x > Xelem_size * (elemX+1.00000001))
	{
		{
			//printf("christ x \n");
			return 1;
		}
	}
	
	if (y < Yelem_size * (elemY  - .00000001)||y > Yelem_size * (elemY+1.00000001))
	{
		{	
			//printf("christ y \n");
			return 2;
		}
	}
	
	if (z < Zelem_size * (elemZ - .00000001) || z > Zelem_size * (elemZ+1.0000001))
	{
		{
			//printf("christ z \n");
			return 3;
		}
	}

	
	return 0;
			
}





