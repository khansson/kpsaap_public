
#include <stdio.h>
#include <vector>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "controller.h"
#include <stdlib.h>
#include <math.h> 
#include "pugixml.hpp"
#include <sstream>
#include <iomanip>

std::string toStringPrecision(double input,int n)
{
    std::stringstream stream;
    stream << std::fixed << std::setprecision(n) << input;
    return stream.str();
}

int  controller::WriteXMLConfig(char *file)
{
	
	int i = 0;
	
	pugi::xml_document doc;
	
	pugi::xml_node config = doc.append_child("KPSAAP_CONFIG");
	
	pugi::xml_node mesh = config.append_child("Mesh_Data");
	
	 // add description node with text child
	pugi::xml_node data = mesh.append_child("Domain");
	
	pugi::xml_node vec = data.append_child("X");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(domainX).c_str());
    
    vec = data.append_child("Y");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(domainY).c_str());
    
    vec = data.append_child("Z");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(domainZ).c_str());
    

    data = mesh.append_child("Element");
	
	vec = data.append_child("X");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(domain_elemX).c_str());
    
    vec = data.append_child("Y");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(domain_elemY).c_str());
    
    vec = data.append_child("Z");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(domain_elemZ).c_str());
    
    data = mesh.append_child("Offset");
	
	vec = data.append_child("X");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(offsetX).c_str());
    
    vec = data.append_child("Y");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(offsetY).c_str());
    
    vec = data.append_child("Z");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(offsetZ).c_str());
    
    
    
	pugi::xml_node general = config.append_child("Ray_Trace");
	
	vec = general.append_child("Number_of_Trials");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(NUM_TRIALS).c_str());
    
    vec = general.append_child("Rays_per_Trial");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(NUM_RAYS).c_str());
    
    vec = general.append_child("Iteration_Limit");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(iteration_limit).c_str());
    
    vec = general.append_child("Method");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(TraceType).c_str());
    
    vec = general.append_child("Particle_Heat_Capacity");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(prt_cp).c_str());
    
    vec = general.append_child("Particle_Initial_Temperature");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(initial_temp).c_str());
    
    pugi::xml_node emit = config.append_child("Emission");
    vec = emit.append_child("Method");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(EmissionType).c_str());
    
    vec = emit.append_child("Ray_Power");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(Optics.RayPower).c_str());
    
    vec = emit.append_child("Ray_Wavelength");
    vec.append_child(pugi::node_pcdata).set_value(toStringPrecision(Optics.EmissionLambda,9).c_str());
    
    vec = emit.append_child("Bundle_Size");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(bundle_size).c_str());
    
    vec = emit.append_child("Beam_Size");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(BeamRadius).c_str());
    
    vec = emit.append_child("Beam_Radius");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(BeamMu).c_str());
    
    vec = emit.append_child("Cone_Angle");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(cone_angle).c_str());
    
    
    
    
    
    pugi::xml_node scatt = config.append_child("Scattering");
    vec = scatt.append_child("Method");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(Scat).c_str());
    
    vec = scatt.append_child("Particle_n");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(Optics.partn).c_str());
    
    vec = scatt.append_child("Particle_k");
    vec.append_child(pugi::node_pcdata).set_value(std::to_string(Optics.partk).c_str());
    
    
    
    for (i = 0; i< ParticleDimPos.size();i++)	
	{
		pugi::xml_node param = scatt.append_child("Parameter");
		
		vec = param.append_child("Position_in_CSV_file");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(ParticleDimPos.at(i)).c_str());
		
		vec = param.append_child("Default_Value");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(ParticleDefaultVal.at(i)).c_str());
		
		vec = param.append_child("Number_of_Bins");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(ParticleBins.at(i)).c_str());
		
		vec = param.append_child("Normalization");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(ParticleNormals.at(i)).c_str());
	}
			
    
    pugi::xml_node boundaries = config.append_child("Boundaries");
    
    pugi::xml_node bound = boundaries.append_child("UpX");
    vec = bound.append_child("Type");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(bounds[0]).c_str());
	
	vec = bound.append_child("Thickness");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(thicknesses[0]).c_str());
	
		
	bound = boundaries.append_child("DownX");
	vec = bound.append_child("Type");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(bounds[1]).c_str());
	
	vec = bound.append_child("Thickness");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(thicknesses[1]).c_str());
	
	bound = boundaries.append_child("UpY");
	vec = bound.append_child("Type");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(bounds[2]).c_str());
	
	vec = bound.append_child("Thickness");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(thicknesses[2]).c_str());
	
	bound = boundaries.append_child("DownY");
	vec = bound.append_child("Type");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(bounds[3]).c_str());
	
	vec = bound.append_child("Thickness");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(thicknesses[3]).c_str());
	
	bound = boundaries.append_child("UpZ");
	vec = bound.append_child("Type");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(bounds[4]).c_str());
	
	vec = bound.append_child("Thickness");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(thicknesses[4]).c_str());
	
	bound = boundaries.append_child("DownZ");
	vec = bound.append_child("Type");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(bounds[5]).c_str());
	
	vec = bound.append_child("Thickness");
	vec.append_child(pugi::node_pcdata).set_value(std::to_string(thicknesses[5]).c_str());
	
    
    
    
    
    for(i = 0; i < TraceOuts.size();i++)
    {
		pugi::xml_node collect = config.append_child("Collection");
    
		vec = collect.append_child("Method");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(TraceOuts.at(i)->type()).c_str());
    
 
		vec = collect.append_child("X_Samples");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(TraceOuts.at(i)->mapxsamples).c_str());
		
		vec = collect.append_child("Y_Samples");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(TraceOuts.at(i)->mapysamples).c_str());
		
		vec = collect.append_child("Scatter_Samples");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(TraceOuts.at(i)->mapscattsamples).c_str());
		
		vec = collect.append_child("Window_Size");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(TraceOuts.at(i)->WindowSize).c_str());
		
		vec = collect.append_child("Window_Offset");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(TraceOuts.at(i)->WindowOffset).c_str());
		
		vec = collect.append_child("Collection_Radius");
		vec.append_child(pugi::node_pcdata).set_value(std::to_string(TraceOuts.at(i)->CollectionRadius).c_str());
		
		
		
		
    
	}
    

   
    
    doc.print(std::cout);
    doc.save_file(file);
    return 1;
}


//from Controller


void controller::WriteRemesh::SetNames()
{
	ParamNames.clear();
	ParamNames.push_back("X_Elem");
	ParamNames.push_back("Y_Elem");
	ParamNames.push_back("Z_Elem");
	
	return;
}

int controller::WriteRemesh::Run(int posflag)
{
	M_->InitiateDomain(M_->domainX, Params.at(0), M_->domainY, Params.at(1), M_->domainZ, Params.at(2), 0.0);
	M_->Sort_Particles();
	
	return 1;
}

void controller::WriteHeats::SetNames()
{
	return;
}

int controller::WriteHeats::Run(int posflag)
{
	FILE* HeatLOG = fopen ("Heats.csv" , "w");
	
	int i,j;
	
	for(i = 0;i < M_->ParticleQ.size() ; i++)
	{
		
		for (j = 0;j < M_->ParticleQ.at(i).size() ; j++)
		{
			fprintf(HeatLOG,"%f,",M_->ParticleQ.at(i).at(j));
		}
		fprintf(HeatLOG,"\n");
		
		
	}
	
	fclose(HeatLOG);
	return 0;
}

void controller::WriteScatterStats::SetNames()
{
	return;
}

int controller::WriteScatterStats::Run(int posflag)
{
	FILE* Scatt_LOG = fopen ("Scatts.csv" , "w");
	int t,j;
	
	for(t = 0;t<M_->Scatt_vec.size();t++)
	{
		for (j = 0; j< M_->Scatt_vec.at(t).size();j++)
		{
			fprintf(Scatt_LOG,"%d,",M_->Scatt_vec.at(t).at(j));
		}
		fprintf(Scatt_LOG,"\n");
		
		
	}	
	return 1;
}

void controller::WriteHeatsII::SetNames()
{
	return;
}

int controller::WriteHeatsII::Run(int posflag)
{
	
	if (M_->domain_elemX>1000 || M_->domain_elemY>1000||M_->domain_elemZ>1000)
	{
		M_->InitiateDomain(M_->domainX, 160, M_->domainY, 160 , M_->domainZ, 160, 0.0);
		M_->Sort_Particles();
	}
	
	
	FILE* HeatLOG = fopen ("Heats.csv" , "w");
	
	int i,j,k,p,t;
	
	for(i = 0;i < M_->mesh_exact.size() ; i++)
	{
		
		for (j = 0;j < M_->mesh_exact.at(i).size() ; j++)
		{
			for (k = 0;k < M_->mesh_exact.at(i).at(j).size() ; k++)
			{
				for(p = 0;p<M_->mesh_exact.at(i).at(j).at(k).size();p++)
				{
					fprintf(HeatLOG,"%i,%i,%i",i,j,k);
					for(t = 0;t<M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).size();t++)
					{
						fprintf(HeatLOG,",%f",M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(t));
					}				
					fprintf(HeatLOG,"\n");
				}
			}
		}	
		
	}
	
	fclose(HeatLOG);
	return 0;
}

void controller::WriteHeatsTEC_ASCII::SetNames()
{
	return;
}

int controller::WriteHeatsTEC_ASCII::Run(int posflag)
{
	FILE* HeatLOG = fopen ("Heats.plt" , "w");
	
	int i = 0,j = 0,k = 0, p,h;
	
	int count = 0;
	
	fprintf(HeatLOG,"TITLE = \"CELL-CENTERED FIELD DATA\"\n");
	fprintf(HeatLOG,"VARIABLES = \"X\", \"Y\", \"Z\", \"HEAT\", \"TEMP\",  \"NUMBER\" \n");
	
	
	for(h = 0;h < M_->ParticleQ.at(0).size();h++)
	{
	i = 0;
	j = 0;
	k = 0;
	fprintf(HeatLOG,"ZONE I=%i, J=%i, K=%i,  DATAPACKING=BLOCK, VARLOCATION=([4,5,6]=CELLCENTERED)\n",(int)M_->mesh_exact.at(i).at(j).size()+1,(int)M_->mesh_exact.at(i).size()+1,(int)M_->mesh_exact.size()+1);
	
	
	
		count = 0;
		for(i = 0;i < M_->mesh_exact.size() +1; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(0).size() +1; j++)
			{
				for (k = 0;k < M_->mesh_exact.at(0).at(0).size() +1; k++)
				{
					count++;
					fprintf(HeatLOG,"%f ",i*M_->Xelem_size);
					if(count == 2000)
					{
						count = 0;
						fprintf(HeatLOG,"\n");
					}
	
				}
			}	
			
		}
		
		fprintf(HeatLOG,"\n");
		count = 0;
		for(i = 0;i < M_->mesh_exact.size() +1; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(0).size() +1; j++)
			{
				for (k = 0;k < M_->mesh_exact.at(0).at(0).size() +1; k++)
				{
					count++;
					fprintf(HeatLOG,"%f ",j*M_->Yelem_size);
					if(count == 2000)
					{
						count = 0;
						fprintf(HeatLOG,"\n");
					}
	
				}
			}	
			
		}
		fprintf(HeatLOG,"\n");
		count = 0;
		for(i = 0;i < M_->mesh_exact.size() +1; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(0).size() +1; j++)
			{
				for (k = 0;k < M_->mesh_exact.at(0).at(0).size() +1; k++)
				{
					count++ ;
					fprintf(HeatLOG,"%f ",k*M_->Zelem_size);
					if(count == 2000)
					{
						count = 0;
						fprintf(HeatLOG,"\n");
					}
				}
			}	
			
		}
		
		fprintf(HeatLOG,"\n");
		
		double heat;
		count = 0;
		for(i = 0;i < M_->mesh_exact.size() ; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(i).size() ; j++)
			{
				for (k = 0;k < M_->mesh_exact.at(i).at(j).size() ; k++)
				{
					count++;
					heat = 0;
					for(p = 0;p<M_->mesh_exact.at(i).at(j).at(k).size();p++)
					{
						heat += M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h);
	
					}
					fprintf(HeatLOG,"%f ",heat);
					if(count == 2000)
					{
						count = 0;
						fprintf(HeatLOG,"\n");
					}
				}
			}	
			
		}
		fprintf(HeatLOG,"\n");
		heat = 0;
		count = 0;
		for(i = 0;i < M_->mesh_exact.size() ; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(i).size() ; j++)
			{
				for (k = 0;k < M_->mesh_exact.at(i).at(j).size() ; k++)
				{
					count++;
					heat = 0;
					for(p = 0;p<M_->mesh_exact.at(i).at(j).at(k).size();p++)
					{
						heat += M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h) /( .25 * M_->sqr(M_->ParticleParm(M_->mesh_exact.at(i).at(j).at(k).at(p),0)) * M_PI * M_->prt_cp ) ;
	
					}
					fprintf(HeatLOG,"%f ",heat);
					if(count == 2000)
					{
						count = 0;
						fprintf(HeatLOG,"\n");
					}
				}
			}	
			
		}
		fprintf(HeatLOG,"\n");
		count = 0;
		for(i = 0;i < M_->mesh_exact.size() ; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(i).size() ; j++)
			{
				for (k = 0;k < M_->mesh_exact.at(i).at(j).size() ; k++)
				{
					count++;
					fprintf(HeatLOG,"%i ",(int)M_->mesh_exact.at(i).at(j).at(k).size());
					if(count == 2000)
					{
						count = 0;
						fprintf(HeatLOG,"\n");
					}
				}
			}	
			
		}
		fprintf(HeatLOG,"\n");
		
	}
	
	fclose(HeatLOG);
	return 0;
}

void controller::WriteMaps_vtk::SetNames()
{
	return;
}


int controller::WriteMaps_vtk::Run(int posflag)
{
	int i;
	for(i = 0; i< M_->TraceOuts.size(); i++)
	{
		M_->TraceOuts.at(i)->WriteMap_vtk();
		M_->TraceOuts.at(i)->WriteTransmissionStats();
		if(M_->PathLengths)
		{
			M_->TraceOuts.at(i)->WriteLegnthsHistogram();
		}
		
	}
	return 1;
}

void controller::WriteFreePathsStats::SetNames()
{
	return;
}

int controller::WriteFreePathsStats::Run(int posflag)
{
	

	int i = 0,j = 0,k = 0, l = 0;
	double count = 0;
	
	
	char buffer[50];
	
	FILE * FPT;
	sprintf(buffer,"FreePaths.csv");
    FPT = fopen(buffer,"w");
	
	
	
	
	
	for(i = 0; i<M_->FreePaths.at(0).size();i ++)
	{
		count = 0;
		for(j = 0;j<M_->FreePaths.size();j++)
		{
			count +=  M_->FreePaths.at(j).at(i);
		} 
		
		fprintf(FPT,"%e,%f\n",i/(double)M_->PathBins*M_->MaxPath,count);
	}

	
	return 1;
}

void controller::WriteBeamTime::SetNames()
{
	return;
}

int controller::WriteBeamTime::Run(int posflag)
{
	int i = 0,j = 0,k = 0, p,h;
	
	double total = 0;
	double core_small = 0;
	double core_large = 0;
	std::vector<double> slice;
	double wall_one =0, wall_two = 0;
	
	int n = 8;
	
	double small = 0.2;
	double large = 0.4;
	double wall = 1/40.0;
	
	slice.clear();
	slice.resize(n);
	
	for(h = 1;h < M_->ParticleQ.at(0).size();h++)
	{
		for(i = 0;i < M_->mesh_exact.size() ; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(i).size() ; j++)
			{
				for (k = 0;k < M_->mesh_exact.at(i).at(j).size() ; k++)
				{
					for(p = 0;p<M_->mesh_exact.at(i).at(j).at(k).size();p++)
					{
								
						
						
						total += M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h)- M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);
						
						if( fabs(i - M_->mesh_exact.size()/2) < small * M_->mesh_exact.size()/2)
						{
							if( fabs(j - M_->mesh_exact.at(i).size() /2) < small * M_->mesh_exact.at(i).size() /2)
							{
								core_small+= M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h) - M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);
							}
						}
						
						if( fabs(i - M_->mesh_exact.size()/2) < large * M_->mesh_exact.size()/2)
						{
							if( fabs(j - M_->mesh_exact.at(i).size() /2) < large * M_->mesh_exact.at(i).size() /2)
							{
								core_large+= M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h)- M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);
							}
						}
						
						if( (k  ) < wall * M_->mesh_exact.at(i).at(j).size() )
						{
							wall_one+= M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h)- M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);
						}
						
						if( ( M_->mesh_exact.at(i).at(j).size()  - k ) <= wall * M_->mesh_exact.at(i).at(j).size() )
						{
							wall_two+= M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h)- M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);
						}
					
						
						slice.at(floor(k / (double)M_->mesh_exact.at(i).at(j).size() * 8)) += M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h)- M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);

	
					}

				}
			}	
			
		}
	
	
	}
	
	FILE * OUTPUT = fopen("BEAM_OUT.dat","w");
	fprintf(OUTPUT,"%f,",M_->TimeStamp);
	fprintf(OUTPUT," %f,",M_->TraceOuts.at(0)->PowerSeen()/M_->PE());
	fprintf(OUTPUT,"%f,",total);
	fprintf(OUTPUT,"%f,",core_large);
	fprintf(OUTPUT,"%f,",core_small);
	for(i = 0;i<slice.size();i++)
	{
		fprintf(OUTPUT,"%f,",slice.at(i));
	}
	fprintf(OUTPUT,"%f,",wall_one);
	fprintf(OUTPUT,"%f,",wall_two);
	fclose(OUTPUT);
	return 0;
}

void controller::WriteBeamTimeII::SetNames()
{
	return;
}

int controller::WriteBeamTimeII::Run(int posflag)
{
	int i = 0,j = 0,k = 0, p,h;
	
	double total = 0;
	double core_small = 0;
	int total_n = 0;
	int core_small_n = 0;
	std::vector<double> slice;
	std::vector<int> slice_n;
	double bit;
	int n = 80;
	
	double small = 1;

	
	
	M_->InitiateDomain(M_->domainX, (int)(1/small), M_->domainY, (int)(1/small), M_->domainZ, n, 0.0);
	M_->Sort_Particles();
	
	
	slice.clear();
	slice.resize(n);
	slice_n.clear();
	slice_n.resize(n);
	
	if (M_->ParticleQ.size() == 0)
	{
		return 1;
	}
	
	for(h = 0;h < M_->ParticleQ.at(0).size()-1;h++)
	{
		for(i = 0;i < M_->mesh_exact.size() ; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(i).size() ; j++)
			{
				for (k = 0;k < M_->mesh_exact.at(i).at(j).size() ; k++)
				{
					for(p = 0;p<M_->mesh_exact.at(i).at(j).at(k).size();p++)
					{
								
						
						/*
						if(ParticleQ.at(mesh_exact.at(i).at(j).at(k).at(p)).at(h)- ParticleQ.at(mesh_exact.at(i).at(j).at(k).at(p)).at(0) != 0 )
						{
							printf("energy\n");
						} */
						
						bit = (M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h)- M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).back());
						total_n++;
						
						
						total =  total + bit;
						
						
						if( fabs(i - M_->mesh_exact.size()/2) < small * M_->mesh_exact.size()/2)
						{
							if( fabs(j - M_->mesh_exact.at(i).size() /2) < small * M_->mesh_exact.at(i).size() /2)
							{
								core_small+= bit;
								core_small_n++;
							}
						}
						

						

					
						
						slice.at(k)  =  slice.at(k) + bit;
						
						slice_n.at(k)++;
	
					}

				}
			}	
			
		}
	
	
	}
	
	FILE * OUTPUT = fopen("BEAM_OUT.dat","w");
	fprintf(OUTPUT,"%f, ",M_->TimeStamp);
	fprintf(OUTPUT,"%f, ",M_->TraceOuts.at(0)->PowerSeen()/M_->PE());
	fprintf(OUTPUT,"%f, ",total);
	fprintf(OUTPUT,"%d, ",total_n);
	fprintf(OUTPUT,"%f, ",core_small);
	fprintf(OUTPUT,"%d, ",core_small_n);
	for(i = 0;i<slice.size();i++)
	{
		fprintf(OUTPUT,"%f, ",slice.at(i));
	}
	
	for(i = 0;i<slice.size();i++)
	{
		fprintf(OUTPUT,"%d, ",slice_n.at(i));
	}

	fclose(OUTPUT);
	return 0;
}

void controller::WriteBeamTimeIII::SetNames()
{
	return;
}

int controller::WriteBeamTimeIII::Run(int posflag)
{
	int i = 0,j = 0,k = 0, p,h;
	
	double total = 0;
	double core_small = 0;
	int total_n = 0;
	int core_small_n = 0;
	std::vector<double> slice;
	std::vector<int> slice_n;
	
	
	std::vector<double> side1;
	std::vector<int> side1_n;
	
	std::vector<double> side2;
	std::vector<int> side2_n;
	
	
	int n = 64;
	int nn = 32;
	
	double small = 0.2;

	
	
	M_->InitiateDomain(M_->domainX, (int)(1/small), M_->domainY, (int)(1/small), M_->domainZ, n, 0.0);
	M_->Sort_Particles();
	
	
	slice.clear();
	slice.resize(n);
	slice_n.clear();
	slice_n.resize(n);
	
	side1.clear();
	side1.resize(nn);
	side1_n.clear();
	side1_n.resize(nn);
	
	side2.clear();
	side2.resize(nn);
	side2_n.clear();
	side2_n.resize(nn);
	
	for(h = 1;h < M_->ParticleQ.at(0).size();h++)
	{
		for(i = 0;i < M_->mesh_exact.size() ; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(i).size() ; j++)
			{
				for (k = 0;k < M_->mesh_exact.at(i).at(j).size() ; k++)
				{
					for(p = 0;p<M_->mesh_exact.at(i).at(j).at(k).size();p++)
					{
								
						
						
						total += M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h)- M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);
						total_n++;
						
						if( fabs(i - M_->mesh_exact.size()/2) < small * M_->mesh_exact.size()/2)
						{
							if( fabs(j - M_->mesh_exact.at(i).size() /2) < small * M_->mesh_exact.at(i).size() /2)
							{
								core_small+= M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h) - M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);
								core_small_n++;
							}
						}
						

						

					
						
						slice.at(floor(k )) += M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h)- M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);
						
						slice_n.at(floor(k ))++;
	
					}

				}
			}	
			
		}
	
	
	}
	
	
	
	M_->InitiateDomain(M_->domainX, (int)(1/small), M_->domainY, (int)(1/small), M_->domainZ, n*nn, 0.0);
	M_->Sort_Particles();
	
	for(h = 1;h < M_->ParticleQ.at(0).size();h++)
	{
		for(i = 0;i < M_->mesh_exact.size() ; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(i).size() ; j++)
			{
				for (k = 0;k < nn; k++) //mesh_exact.at(i).at(j).size()
				{
					for(p = 0;p<M_->mesh_exact.at(i).at(j).at(k).size();p++)
					{
						
						
						side1.at(floor(k)) += M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h)- M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);
						
						side1_n.at(floor(k ))++;
	
					}

				}
			}	
			
		}
	
	
	}
	
	
	for(h = 1;h < M_->ParticleQ.at(0).size();h++)
	{
		for(i = 0;i < M_->mesh_exact.size() ; i++)
		{
			
			for (j = 0;j < M_->mesh_exact.at(i).size() ; j++)
			{
				for (k = M_->mesh_exact.at(i).at(j).size() -1 ;k >= M_->mesh_exact.at(i).at(j).size() - nn; k--) //mesh_exact.at(i).at(j).size()
				{
					for(p = 0;p<M_->mesh_exact.at(i).at(j).at(k).size();p++)
					{
								
												
						side2.at(floor(M_->mesh_exact.at(i).at(j).size() -1 - k )) += M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(h)- M_->ParticleQ.at(M_->mesh_exact.at(i).at(j).at(k).at(p)).at(0);
						
						side2_n.at(floor(M_->mesh_exact.at(i).at(j).size() -1 - k ))++;
	
					}

				}
			}	
			
		}
	
	
	}
	
	
	
	
	FILE * OUTPUT = fopen("BEAM_OUT.dat","w");
	fprintf(OUTPUT,"%f, ",M_->TimeStamp);
	fprintf(OUTPUT,"%f, ",M_->TraceOuts.at(0)->PowerSeen()/M_->PE());
	fprintf(OUTPUT,"%f, ",total);
	fprintf(OUTPUT,"%d, ",total_n);
	fprintf(OUTPUT,"%f, ",core_small);
	fprintf(OUTPUT,"%d, ",core_small_n);
	for(i = 0;i<slice.size();i++)
	{
		fprintf(OUTPUT,"%f, ",slice.at(i));
	}
	
	for(i = 0;i<slice.size();i++)
	{
		fprintf(OUTPUT,"%d, ",slice_n.at(i));
	}
	
	for(i = 0;i<side1.size();i++)
	{
		fprintf(OUTPUT,"%f, ",side1.at(i));
	}
	
	for(i = 0;i<side1.size();i++)
	{
		fprintf(OUTPUT,"%d, ",side1_n.at(i));
	}
	for(i = 0;i<side2.size();i++)
	{
		fprintf(OUTPUT,"%f, ",side2.at(i));
	}
	
	for(i = 0;i<side2.size();i++)
	{
		fprintf(OUTPUT,"%d,",side2_n.at(i));
	}

	fclose(OUTPUT);
	
	return 0;
}


void controller::WriteGLine_Z::SetNames()
{
	
	ParamNames.clear();
	ParamNames.push_back("X_Elem");
	ParamNames.push_back("Y_Elem");
	
}

int controller::WriteGLine_Z::Run(int posflag)
{
	
	int i,j,k;
	char buffer[50];

	sprintf(buffer,"GLine_Z_%d.plt",posflag);
   
	FILE * OUTPUT = fopen(buffer,"w");
	std::vector<double> parm;
	int X = (int)Params.at(0);
	int Y = (int)Params.at(1);
	
	if (X >= M_->mesh_exact.size())
	{
		
		return 1;
	}
	
	if (Y>= M_->mesh_exact.at(X).size())
	{
		
		return 1;
	}
	int ParticleID;
	std::vector<double> G;
	std::vector<int> N;
	G.resize(M_->mesh_exact.at(X).at(Y).size());
	N.resize(M_->mesh_exact.at(X).at(Y).size());
	
	
	for(i = 0; i<  M_->mesh_exact.at(X).at(Y).size();i++)
	{
		
		for(j = 0; j <  M_->mesh_exact.at(X).at(Y).at(i).size();j++)
		{
			ParticleID = M_->mesh_exact.at(X).at(Y).at(i).at(j);
			for(k = 0; k< M_->ParticleQ.at(ParticleID).size()-1; k++)
			{
				
				parm = M_->ParmVec(ParticleID,M_->Optics.EmissionLambda);
				G.at(i) += (M_->ParticleQ.at(ParticleID).at(k) - M_->ParticleQ.at(ParticleID).back())/(pow(M_->ParticleParm(ParticleID,0),2)*0.25*M_->Optics.Ideal_Array.search(parm)->Qa);
				N.at(i) += 1;
				
				
			}
			
			
			
			
			
		}
		
		
		if(N.at(i) != 0)
		{
			G.at(i) /= N.at(i);
		}
		
		
	}
	
	
	fprintf(OUTPUT,"TITLE = \"CELL-CENTERED FIELD DATA\"\n");
	fprintf(OUTPUT,"variables = \"x\",\"y\",\"z\",\"G\",\"Np\"\n");
	fprintf(OUTPUT,"zone T=\"MCRT isosca MLR2 line 1\"\n");
	
	for (i =0 ; i<G.size();i++)
	{
		fprintf(OUTPUT,"%f %f %f %e %d\n",(X+0.5)*M_->Xelem_size,(Y+0.5)*M_->Yelem_size,(i+0.5)*M_->Zelem_size, G.at(i)*M_->EnergyScaling, N.at(i));
	}
	
	
	

	fclose(OUTPUT);
	
	return 0;
}

// TraceOuts


int TraceOut_Base::WriteTransmissionStats()
{
	
	
	int i = 0,j = 0,k = 0, l = 0;
	int count = 0;
	
	
	char buffer[50];
	
	FILE * FPT;
	sprintf(buffer,"Tranmission_%s.vtk",name().c_str());
    FPT = fopen(buffer,"w");
    
    
    
	fprintf(FPT,"Transmission,  Tally Varience, Trial Variance\n");
	fprintf(FPT,"%f,",PowerSeen()/master->PE());
	
	
	
	double sum=0;
	
	double sqr = 0;
	
	for(i = 0; i<SensorMap.size(); i++)
	{
		
		for(j = 0; j<SensorMap.at(i).size(); j++)
		{
			
			for(k = 0; k<SensorMap.at(i).at(j).size(); k++)
			{
				for(l = 0; l<SensorMap.at(i).at(j).at(k).size(); l++)
				{
					sum +=SensorMap.at(i).at(j).at(k).at(l).sum;
					count +=SensorMap.at(i).at(j).at(k).at(l).count;
					sqr +=SensorMap.at(i).at(j).at(k).at(l).sum_sqr;
				}
				
			}
		}
	}
	
	fprintf(FPT,"%f,",(sqrt(fabs(sum*sum - sqr))/(count+1))/master->PE());
	
	std::vector<double> sums;
	for(i = 0; i<SensorMap.size(); i++)
	{
		sum = 0;
		for(j = 0; j<SensorMap.at(i).size(); j++)
		{
			
			for(k = 0; k<SensorMap.at(i).at(j).size(); k++)
			{
				for(l = 0; l<SensorMap.at(i).at(j).at(k).size(); l++)
				{
					sum +=SensorMap.at(i).at(j).at(k).at(l).sum;
					
				}
				
			}
		}
		sums.push_back(sum);
		
		
		
	}
	
	sum = 0;
	sqr = 0;
	count = sums.size();
	
	for (i = 0; i < sums.size();i++)
	{
		sum += sums.at(i);
		sqr += sums.at(i) * sums.at(i);
	} 
	fprintf(FPT,"%f\n",(sqrt(fabs(sum*sum - sqr))/(count+1))/master->PE());
}

int TraceOut_Base::WriteMap_vtk()
{
	
	
	
	
	int i = 0,j = 0,k = 0, l = 0;
	int count = 0;
	
	
	char buffer[50];
	
	FILE * FPT;
	sprintf(buffer,"ImpactMap_%s.vtk",name().c_str());
    FPT = fopen(buffer,"w");
	
	fprintf(FPT,"# vtk DataFile Version 3.0\n");
	fprintf(FPT,"vtk output\n");
	fprintf(FPT,"ASCII\n");
	fprintf(FPT,"DATASET STRUCTURED_GRID\n");
	fprintf(FPT,"DIMENSIONS %d %d %d\n",mapxsamples+1,mapysamples+1,1);
	fprintf(FPT,"POINTS %d float\n",(mapxsamples + 1)* (mapysamples + 1));
	
	if (name()=="Sphere_C"||name()=="Sphere_Inf_C")
	{
		for(i = 0; i<(mapxsamples + 1);i++)
		{
			for(j = 0;j<(mapysamples + 1);j++)
			{
				for(k = 0; k<1;k++)
				{
					fprintf(FPT,"%f %f %f\n",sin((double)i/(double)mapxsamples*M_PI)*cos((double)j/(double)mapysamples*2.0*M_PI), sin((double)i/(double)mapxsamples*M_PI)*sin((double)j/(double)mapysamples*2.0*M_PI), cos((double)i/(double)mapxsamples*M_PI));
				}
			}
		}
		
	}
	else
	{
	
		for(i = 0; i<(mapxsamples + 1);i++)
		{
			for(j = 0;j<(mapysamples + 1);j++)
			{
				for(k = 0; k<1;k++)
				{
					fprintf(FPT,"%d %d %f\n",i, j, 0.0);
				}
			}
		}
		
	}
	
		fprintf(FPT,"CELL_DATA %d\n",mapxsamples*mapysamples);
		
		for(i = 0; i < SensorMap.size();i ++)
		{
		
			fprintf(FPT,"SCALARS Trial_%d float %d\n",i, SensorMap.at(0).at(0).at(0).size());
			fprintf(FPT,"LOOKUP_TABLE default\n");
			for (j = 0; j < SensorMap.at(i).size() ; j++)
			{
				for(k = 0; k < SensorMap.at(i).at(j).size() ; k++)
				{
					for( l = 0; l < SensorMap.at(i).at(j).at(k).size(); l++)
					{
					
						fprintf(FPT,"%f ",SensorMap.at(i).at(j).at(k).at(l).sum);
					}
					fprintf(FPT,"\n");
				}
			}
		}
		
		fprintf(FPT,"SCALARS Statistics_sum_mean_stdev float %d\n", 3);
		fprintf(FPT,"LOOKUP_TABLE default\n");
		i = 0;
		
		double sum,sqr, tot;
		
		for (j = 0; j < mapysamples ; j++)
		{
			for(k = 0; k < mapxsamples ; k++)
			{
				tot = 0;
				count = 0;
				for(i = 0; i < SensorMap.size();i ++)
				{
					
					sum = 0;
					sqr = 0;
					for( l = 0; l < SensorMap.at(i).at(j).at(k).size(); l++)
					{
					
						sum +=SensorMap.at(i).at(j).at(k).at(l).sum;
					}
					tot += sum;
					count += 1;
					sqr += sum*sum;
				}
				fprintf(FPT,"%f %f %f\n", tot, tot/count, sqrt(fabs(sqr/(double)count-tot*tot/(double)(count*count))));
			}
		}
		fclose(FPT);
    
	
	
	
	
	return 0;
}


int TraceOut_Base::WriteLegnthsHistogram()
{
	int i = 0,j = 0,k = 0, l = 0;
	int count = 0;
	
	
	char buffer[50];
	
	FILE * FPT;
	sprintf(buffer,"PathLegnths_%s.csv",name().c_str());
    FPT = fopen(buffer,"w");
	double interval = 100;
	double minn, maxx, delta, test;
	std::vector<int> hist(interval);
	
	minn = *std::min_element(Lengths.at(0).begin(),Lengths.at(0).end());
	maxx = *std::max_element(Lengths.at(0).begin(),Lengths.at(0).end())*1.01;
	for(i = 1; i<Lengths.size();i ++)
	{
		test = *std::min_element(Lengths.at(i).begin(),Lengths.at(i).end());
		if (test< minn)
		{
			minn = test;
		}
		
		test = *std::max_element(Lengths.at(i).begin(),Lengths.at(i).end());
		if (test> maxx)
		{
			maxx = test;
		}
	
	}
	
	maxx *= 1.01;
	delta = maxx - minn;
	
	for(i = 0; i<Lengths.size();i ++)
	{
		for(j = 0;j<Lengths.at(i).size();j++)
		{
			hist.at(floor((Lengths.at(i).at(j) - minn)/delta*interval)) += 1;
		}
	
	}

	for(i = 0; i<hist.size();i ++)
	{
		fprintf(FPT,"%f,%d\n", minn + delta / interval * (double)i, hist.at(i));
		
		
	}
	return 0;
	
}

int TraceOut_Base::WriteLegnthsRaw()
{
	int i = 0,j = 0,k = 0, l = 0;
	int count = 0;
	
	
	char buffer[50];
	
	FILE * FPT;
	sprintf(buffer,"PathLegnths_raw_%s.csv",name().c_str());
    FPT = fopen(buffer,"w");
	double interval = 100;
	double minn, maxx, delta;
	std::vector<int> hist(interval);
	
	
	
	
	for(i = 0; i<Lengths.size();i ++)
	{
		for(j = 0;j<Lengths.at(i).size();j++)
		{
			fprintf(FPT,"%f\n", Lengths.at(i).at(j));
		}
		
		
	}
	return 0;
	
}

int TraceOut_Base::WriteMap_tec()
{
	int i = 0,j = 0,k = 0;
	int count = 0;
	FILE* FPT = fopen ("ImpactMap.plt" , "w");
	fprintf(FPT,"TITLE = \"CELL-CENTERED IMPACT MAP\"\n");
	fprintf(FPT,"VARIABLES = \"X\", \"Y\", \"ENERGY\", \"COUNT\" \n");
	
	
	
	
	for (k = 0;k<SensorMap.size();k++)
	{
		fprintf(FPT,"ZONE T=\"Trial %i\", I=%i, J=%i,  DATAPACKING=BLOCK, VARLOCATION=([3,4]=CELLCENTERED)\n",k+1,(int)SensorMap.at(0).at(0).size()+1, (int)SensorMap.at(0).size()+1);
	
		for(i = 0;i < SensorMap.at(0).size()+1 ; i++)
		{	
	
			for (j = 0;j < SensorMap.at(0).at(0).size()+1 ; j++)
			{
				count++;
				fprintf(FPT,"%f ", j * 1.0);
				if (count == 2000)
				{
					count = 0;
					fprintf(FPT,"\n");
				}
			}
	

			
		}
		fprintf(FPT,"\n");
		count = 0;
		for(i = 0;i < SensorMap.at(0).size()+1 ; i++)
		{	
	
			for (j = 0;j < SensorMap.at(0).at(0).size() +1; j++)
			{
				count++;
				fprintf(FPT,"%f ",i*1.0);
				if (count == 2000)
				{
					count = 0;
					fprintf(FPT,"\n");
				}
	

			}	

			
		}
		fprintf(FPT,"\n");
	
		count = 0;
		for(i = 0;i < SensorMap.at(k).size() ; i++)
		{	
			for (j = 0;j < SensorMap.at(k).at(i).size() ; j++)
			{
				count++;
				fprintf(FPT,"%f ",(SensorMap.at(k).at(i).at(j).at(0).sum));
				if (count == 2000)
				{
					count = 0;
					fprintf(FPT,"\n");
				}
	
				
			}	
			

		}
		fprintf(FPT,"\n");
	
		count = 0;
		for(i = 0;i < SensorMap.at(k).size() ; i++)
		{	
			for (j = 0;j < SensorMap.at(k).at(i).size() ; j++)
			{
				count++;
				fprintf(FPT,"%d ",(SensorMap.at(k).at(i).at(j).size()));
				if (count == 2000)
				{
					count = 0;
					fprintf(FPT,"\n");
				}
			}
		}
	}
	fprintf(FPT,"\n");
	fclose(FPT);
	return 0;
}


int TraceOut_Base::WriteMap_csv()
{
	int i = 0,j = 0,k = 0,l;
	int count = 0;
	char buffer[50];
	
	double sum = 0;
	for (k = 0;k<SensorMap.size();k++)
	{
		
		sprintf(buffer,"ImpactMap_%s_%i.csv",name().c_str(),k+1);
		FILE* FPT = fopen ( buffer, "w");

		for(i = 0;i < SensorMap.at(k).size() ; i++)
		{	
			for (j = 0;j < SensorMap.at(k).at(i).size() ; j++)
			{
				count++;
				
				for (j = 0;j < SensorMap.at(k).at(i).at(j).size() ; j++)
				{
					sum += SensorMap.at(k).at(i).at(j).at(l).sum;
				}
				fprintf(FPT,"%f, ",sum);
				sum = 0;
				
			}	
			fprintf(FPT,"\n");
		
		}
		
		fclose(FPT);
	}
	
		
	
	return 0;
}


/*int controller::WriteEMap()
{
	int i = 0,j = 0,k = 0;
	int count = 0;
	FILE* FPT = fopen ("EmitMap.plt" , "w");
	fprintf(FPT,"TITLE = \"CELL-CENTERED IMPACT MAP\"\n");
	fprintf(FPT,"VARIABLES = \"X\", \"Y\", \"ENERGY\", \"COUNT\" \n");
	
	
	
	
	for (k = 0;k<FullEmitMap.size();k++)
	{
		fprintf(FPT,"ZONE T=\"Trial %i\", I=%i, J=%i,  DATAPACKING=BLOCK, VARLOCATION=([3,4]=CELLCENTERED)\n",k+1,(int)FullEmitMap.at(0).at(0).size()+1, (int)FullEmitMap.at(0).size()+1);
	
		for(i = 0;i < FullEmitMap.at(0).size()+1 ; i++)
		{	
	
			for (j = 0;j < FullEmitMap.at(0).at(0).size()+1 ; j++)
			{
				count++;
				fprintf(FPT,"%f ", j * 1.0);
				if (count == 2000)
				{
					count = 0;
					fprintf(FPT,"\n");
				}
			}
	

			
		}
		fprintf(FPT,"\n");
		count = 0;
		for(i = 0;i < FullEmitMap.at(0).size()+1 ; i++)
		{	
	
			for (j = 0;j < FullEmitMap.at(0).at(0).size() +1; j++)
			{
				count++;
				fprintf(FPT,"%f ",i*1.0);
				if (count == 2000)
				{
					count = 0;
					fprintf(FPT,"\n");
				}
	

			}	

			
		}
		fprintf(FPT,"\n");
	
		count = 0;
		for(i = 0;i < FullEmitMap.at(k).size() ; i++)
		{	
			for (j = 0;j < FullEmitMap.at(k).at(i).size() ; j++)
			{
				count++;
				fprintf(FPT,"%f ",(FullEmitMap.at(k).at(i).at(j).normm()));
				if (count == 2000)
				{
					count = 0;
					fprintf(FPT,"\n");
				}
	
				
			}	
			

		}
		fprintf(FPT,"\n");
	
		count = 0;
		for(i = 0;i < FullEmitMap.at(k).size() ; i++)
		{	
			for (j = 0;j < FullEmitMap.at(k).at(i).size() ; j++)
			{
				count++;
				fprintf(FPT,"%d ",(FullEmitMap.at(k).at(i).at(j).n));
				if (count == 2000)
				{
					count = 0;
					fprintf(FPT,"\n");
				}
			}
		}
	}
	fprintf(FPT,"\n");
	fclose(FPT);
	return 0;
}*/


