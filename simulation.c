
#include <stdio.h>


#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <deque>
#include "controller.h"
#include <stdlib.h>
#include <math.h> 
#include "ray.h"
#include "ranf.h"
#include "optics.h"
#include <algorithm>
#include <chrono>





/// trial -> ratys per trial -> rays per bundle

int controller::BeginSimulation(int trials, int rays_per_trials)
{
	int i,j;
	
	LogCount = 0;
	seed_array = new unsigned int[trials];
	for(i = 0; i< trials; i++)
	{
		seed_array[i] = 0x11111111;
		
		
	}
	
	LogCount = 0;
	
	//mapxsamples = 400;
	//mapysamples = 400;
	
	//cone_half_angle = 9 * 2* 3.14159265 / 360;
	//ParticleRadius = .00000650; // still need for scattering offset?
	//MinPowerThreshold = 1;
	if(ParticleX.size() > 0)
	{
	printf("generating idealization array\n");
	Generate_Ideals();

	
		(this->Optics.*(this->Optics.OpticData::ScatInitFunc))();
	}
	else
	{
		printf("No Particles.. no Ideals\n");
	}
	
	for(i = 0; i <TraceOuts.size();i++)
	{
		TraceOuts.at(i)->Initialize(this);
	}
	
	PowerEmitted.resize(NUM_TRIALS);
	printf("emission setup\n");
	if (EmissionType == 0|| EmissionType == 4)
	{
		emissionZ = - glass_thickness;
		emissionYmax = domainY;// + (glass_thickness + domainX) * tan(cone_half_angle);
		emissionYmin = 0;//- (glass_thickness + domainX) * tan(cone_half_angle);
		emissionXmin = 0; 
		emissionXmax = domainX;
	}
	else if(EmissionType == 1|| EmissionType == 2 || EmissionType == 3)
	{
		gaussx = domainX/2;
		gaussy = domainY/2;
		//BeamRadius = 0.001;
	}
	else
	{
		printf("NEVER!!!!");
		return 1;
	}
	
	printf("Trace initialization\n");
	
	
	if(LogRays)
	{
		printf("OI  Logging Rays.  you really wanna use all this memory?\n");
	}
	if(PathLengths)
	{
		printf("OI! Path Logging is on! you really wanna use all this memory?\n");
	}
	
	if(PathCutOff>0)
	{
		printf("OI! Cutting Off all rays with a travel distance greater that %f\n", PathCutOff);
		
	}
	
	//printf("adress: %p\n",(this->Init_TraceRay));
	(this->*Init_TraceRay)();
	
	Lost_at_emission = 0;
	Lost_in_transmission = 0;
	Lost_at_particle= 0;
	
	Lost_in_numbers = 0;
	
	int count = 0;
	
	CollectionRadius = std::max(domainX,std::max(domainY,domainZ)) * 2;
	
	
	Scatt_vec.resize(NUM_TRIALS);
	FreePaths.resize(NUM_TRIALS);
	PathBins = 10000;
	for(i= 0;i<FreePaths.size();i++)
	{
		FreePaths.at(i).resize(PathBins);
	}
	MaxPath = (domainX + domainY + domainZ)*2*Optics.RayPower;
	if (MaxPath>PathCutOff&& PathCutOff!=0)
	{
		MaxPath = PathCutOff;
	}
	
	if(!ReadInitalTemperatures)
	{
		for(j = 0;j<ParticleQ.size();j++)
		{
			ParticleQ.at(j).push_back(initial_temp*prt_cp);
		}
	}
	

	
	
	for(j = 0;j<ParticleQ.size();j++)
	{
		for (i= 0 ; i < NUM_TRIALS; i++)
		{
			ParticleQ.at(j).push_back(ParticleQ.at(j).front());
		}
	}
	
	//EmitMap.initialized = false;
	//EmitMap.Initialize(this);
	
	
	
	
	
	
	#pragma omp parallel for // need the seed to be private for different trials
	for (i = 0;i<trials;i++)
	{
		

		

		
		printf("!!!!!!!!!!!!!!!!!!!!!   TRIAL %i     !!!!!!!!!!!!!!!!!!!!!\n",i+1);
		if(LogRays)
		{
			RunTrial_II(i);
		}
		else
		{
			RunTrial_III(i);
		}
		double ps = TraceOuts.at(0)->PowerSeen(i);
		printf("Results:\n");
		printf("Power Emitted: %f\n",PowerEmitted.at(i));
		printf("Power Seen: %f\n",ps);
		
		
	}
	
	delete[] seed_array;
	
	return 0;
}



int controller::RunTrial_II(int trial)
{
	
	
	int rays_remaining;
	rays_remaining = NUM_RAYS;
	
	
	int i,j;
	
	
	
	
	
	
	
	std::vector<ray> ray_vec;
	std::deque<ghost> ghost_vec;
		
	int SanityAmmount = 500000;
	end = time(0);
	TimeElapsed = difftime(end,start);
	printf("----  %i minutes and %i seconds have elapsed ----\n", (int) floor(TimeElapsed/60), (int) round(TimeElapsed - 60*floor(TimeElapsed/60))); 
	
	printf("Times at every %d Rays\n", SanityAmmount);
	
	printf("---  ---  Begin Trace  ---  --- \n");
	
	ghost_vec.clear();
	int bundle;
	
	int count = 0;
	int spare;
	int flag;
	int num;
	
	int S;
	int countII = 0;
	
	
	while (rays_remaining > 0)
	{
		
		if ((rays_remaining%SanityAmmount) == 0)
		{
			end = time(0);
			TimeElapsed = difftime(end,start);
			printf("----  %i minutes and %i seconds have elapsed ----\n", (int) floor(TimeElapsed/60), (int) round(TimeElapsed - 60*floor(TimeElapsed/60)));
		}
		bundle = std::min(bundle_size,rays_remaining);
		
		ray_vec.clear();
		ray_vec.resize(bundle);
		
		//make a bundle of joy
		
		
		for(i = 0;i < bundle; i++)
		{
			do
			{
				(this->*TraceInFunc)(&(ray_vec.at(i)), trial);	
			}while(!(ray_vec.at(i).TestDomain(domainX,domainY,domainZ)));
			if(LogRays) {ray_vec[i].SaveHistory(0);}
			//EmitMap.TraceRayOut(&ray_vec[i],trial);
		}
		
		
		
			
		
		
		
		//printf("%i\n",((int) ghost_vec.size()));
		//trace!!!!!!!
		count = 0;
		countII = 0;
		
		while(count<ray_vec.size())
		{
			count = 0;
			countII++;
			
			
			for(i = 0;i < ray_vec.size(); i++)
			{
								

				
				if (ray_vec[i].dead())
				{
					count += 1;
					continue;
				}				
				
				
				
				if(PathCutOff>0)
				{
					//printf("%f\n",(ray_vec[i].T() - ray_vec[i].oT()));
					if ((ray_vec[i].T() - ray_vec[i].oT())>PathCutOff)
					{
						//printf("die!\n");
						ray_vec[i].obliterate();
						continue;
					}
				}
				
				/*
				if(ray_vec[i].EX() > domain_elemX || ray_vec[i].EX() < 0 || ray_vec[i].EY() > domain_elemY || ray_vec[i].EY() < 0 || ray_vec[i].EZ() > domain_elemZ || ray_vec[i].EZ() < 0)
				{
					printf("narrowly avoied seg fault\n");
				
				}*/
				
				
				
				spare = (this->*TraceRay)(&ray_vec[i], trial);			
				
				if(LogRays) {ray_vec[i].SaveHistory(1, spare);}
				
				if (spare>=0||spare == -4) // homogenized scattering
				{
					ghost_vec.resize(ghost_vec.size()+1);
					if(ray_vec[i].S()>0)
					{
						//printf("%d, %f, %d \n",ray_vec[i].S(),ray_vec[i].T() - ray_vec[i].oT(),((int)floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath*PathBins)));
						FreePaths.at(trial).at((floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath*PathBins))<PathBins?((int)floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath*PathBins)):PathBins-1)+= ray_vec[i].P();
						if(ray_vec[i].T() - ray_vec[i].oT() < 0.0000030 && LogRays)
						{
							ray_vec[i].WriteHistory(i + countII*bundle_size, trial);
							
						}
					}
					ghost_vec.back().ReadRay((ray_vec[i]), spare, ScatterPos[Scat]);
				}
				
				if (spare == -2) // hit domain sides
				{
										
										
					BoundaryOut(&ray_vec[i],trial);
					
					if (ray_vec[i].dead())
					{
						S = ray_vec[i].S();
							
						if (S +1>( Scatt_vec.at(trial).size()))
						{
								
							Scatt_vec.at(trial).resize(S +1);
								
						}
							
						Scatt_vec.at(trial).at(S) += 1;
					}
																						
				}
				
				if (spare == -3) // homogenized total absorbtion
				{
					S = ray_vec[i].S();
					if (S +1>( Scatt_vec.at(trial).size()))
					{
						Scatt_vec.at(trial).resize(S +1);
						
					}
					Scatt_vec.at(trial).at(S)+=1;
																										
				}
				
				
				flag = 0;
				
				
				while  (flag != 1) // do scattering
				{
					flag = (this->*ScatFunc)(num, &ghost_vec, &ray_vec, trial);
				}	
				
				
				
			}
			
			if (countII > domain_elemX* domain_elemY * domain_elemZ * 1000.0)
			{
				if(LogRays)
				{
					for(i = 0;i < ray_vec.size(); i++)
					{		
						if(!ray_vec[i].dead())
						{
							ray_vec[i].WriteHistory(LogCount,trial);
							LogCount++;
						}
					}
				}
				//printf("%d,%d,%d\n", domain_elemX, domain_elemY, domain_elemZ);
				printf("total death: %d of %d (%d > %d)\n",count,ray_vec.size(), countII,  domain_elemX* domain_elemY * domain_elemZ * 1000.0);
				break;
			}
			
		}
		
		
		
		rays_remaining = rays_remaining - bundle;
	}
	
	
	
	
	
	
	
	return 0;
}




int controller::RunTrial_III(int trial)
{
	
	//printf("%i\n",((int) ghost_vec.size()));
	int rays_remaining;
	rays_remaining = NUM_RAYS;
	
	//bundle_size = 10;
	
	
	
	//map initiation
	int i,j;
	//FILE* BeamIn = fopen ("BeamIn.csv" , "w");
	
	
	int death_count;
	
	
	//EmitMap.clear();
	std::vector<dcomp_counted> dumdum;
	dumdum.resize(mapxsamples);
	for(i = 0;i<mapysamples;i++)
	{
		//EmitMap.push_back(dumdum);
	}
	
	std::vector<ray> ray_vec;
	std::deque<ghost> ghost_vec;
		
	int SanityAmmount = 500000;
	end = time(0);
	TimeElapsed = difftime(end,start);
	printf("----  %i minutes and %i seconds have elapsed ----\n", (int) floor(TimeElapsed/60), (int) round(TimeElapsed - 60*floor(TimeElapsed/60))); 
	
	
	printf("---  ---  Begin Primary Trace  ---  --- \n");
	printf("Times at every %d Rays\n", SanityAmmount);
	
	ghost_vec.clear();
	int bundle;
	
	int count = 0;
	int spare;
	int flag;
	int num;
	
	int S;
	
	
	
	
	while (rays_remaining > 0)
	{
		
		if ((rays_remaining%SanityAmmount) == 0)
		{
			end = time(0);
			TimeElapsed = difftime(end,start);
			printf("----  %i minutes and %i seconds have elapsed ----\n", (int) floor(TimeElapsed/60), (int) round(TimeElapsed - 60*floor(TimeElapsed/60)));
		}
		bundle = std::min(bundle_size,rays_remaining);
		
		ray_vec.clear();
		ray_vec.resize(bundle);
		
		//make a bundle of joy

		
		for(i = 0;i < bundle; i++)
		{
			(this->*TraceInFunc)(&(ray_vec.at(i)), trial);
			
			
		}
		
		
		
			
		
		
		
		//printf("%i\n",((int) ghost_vec.size()));
		//trace!!!!!!!
		count = 0;
		
		death_count = 0;
		while(count<bundle)
		{
			
			death_count++;
			//if(death_count> domain_elemX + domain_elemY + domain_elemZ)
			if(death_count > domain_elemX * domain_elemY * domain_elemZ+1000)
				{
					for(i = 0;i < ray_vec.size(); i++)
					{
						ray_vec[i].obliterate();
						//printf("killed bundle \n");
					}
				}
				
				
				
				
				
				
				
			count = 0;
			for(i = 0;i < ray_vec.size(); i++)
			{
				
				

				if (ray_vec[i].dead())
				{
					count += 1;
					
					
					continue;
					
				}
								
				if(PathCutOff>0)
				{
					//printf("%f\n",(ray_vec[i].T() - ray_vec[i].oT()));
					if ((ray_vec[i].T() - ray_vec[i].oT())>PathCutOff)
					{
						//printf("die!\n");
						ray_vec[i].obliterate();
						continue;
					}
				}
				/*
				if(ray_vec[i].EX() > domain_elemX || ray_vec[i].EX() < 0 || ray_vec[i].EY() > domain_elemY || ray_vec[i].EY() < 0 || ray_vec[i].EZ() > domain_elemZ || ray_vec[i].EZ() < 0)
				{
					printf("narrowly avoied seg fault\n");
				
				}*/
				
				
				
				spare = (this->*TraceRay)(&ray_vec[i], trial);			
				
				
				
				if (spare>=0||spare == -4)
				{
					ghost_vec.resize(ghost_vec.size()+1);
					//printf("%d,%d,%d,%d\n",((floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath)<PathBins)?(floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath)):PathBins-1), FreePaths.at(trial).size(),FreePaths.size(),trial);
					//FreePaths.at(trial).at((floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath*PathBins)<PathBins)?(floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath*PathBins)):PathBins-1)+=ray_vec[i].P();
					//FreePaths.at(trial).at((floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath*PathBins)<PathBins)?(floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath*PathBins)):PathBins-1)+=ray_vec[i].P();
					ghost_vec.back().ReadRay((ray_vec[i]), spare, ScatterPos[Scat]);
				}
				
				if (spare == -2)
				{
					
					BoundaryOut(&ray_vec[i],trial);
					
					if (ray_vec[i].dead())
						{
							S = ray_vec[i].S();
							
							if (S +1>( Scatt_vec.at(trial).size()))
							{
								
								Scatt_vec.at(trial).resize(S +1);
								
							}
							
							Scatt_vec.at(trial).at(S) += 1;
						}
																						
				}
				
				if (spare == -3)
				{
					S = ray_vec[i].S();
					if (S +1>( Scatt_vec.at(trial).size()))
					{
						Scatt_vec.at(trial).resize(S +1);
						
					}
					
					Scatt_vec.at(trial).at(S)+=1;
																										
				}
				
							
				
			}
			
		}
		
		
		
		rays_remaining = rays_remaining - bundle;
	}
	
	
	
	printf("---  ---  Primary Trace  Complete ---  --- \n");
	printf("LOST AT EMISION : %i\n",Lost_at_emission);
	printf("LOST IN TRANSMISSION : %i\n",Lost_in_transmission);
	printf("LOST AT ABSORBTION : %i\n",Lost_at_particle);
	printf("LOST IN NUMERMICAL BS : %i\n",Lost_in_numbers);
	
	
	 
	
	int countII;
	//Scattering stuff
	
	int trace = 1;
	
	while(true)
	{
		
		end = time(0);
		TimeElapsed = difftime(end,start);
		printf("----  %i minutes and %i seconds have elapsed ----\n", (int) floor(TimeElapsed/60), (int) round(TimeElapsed - 60*floor(TimeElapsed/60)));
		printf("---  --- Sorting Ghosts  ---  --- \n");
		std::sort(ghost_vec.begin(), ghost_vec.end(), GhostSort);
		
		
		rays_remaining = ghost_vec.size();
		
		if (rays_remaining == 0)
		{
			break;
		}
		
		printf("---  ---  Secondary Trace %d ---  --- \n",trace);
		printf("%d rays to be traced\n",rays_remaining);
		trace++;
		
		if(trace > iteration_limit)
		{
			printf("Iteration Limit Exceeded");
			break;
		}
		
		
		while (rays_remaining > 0)
		{
			
			if ((rays_remaining%SanityAmmount) == 0)
			{
				end = time(0);
				TimeElapsed = difftime(end,start);
				printf("----  %i minutes and %i seconds have elapsed ----\n", (int) floor(TimeElapsed/60), (int) round(TimeElapsed - 60*floor(TimeElapsed/60)));
			}
			bundle = std::min(bundle_size,rays_remaining);
			
			ray_vec.clear();
			
			
			//make a bundle of joy
			
			
			for(i = 0;i < bundle; i++)
			{
				flag = (this->*ScatFunc)(num, &ghost_vec, &ray_vec, trial);	
				
				if(!flag)	
				{
					rays_remaining--;
				}		
			}
			
			
			
				
			
			
			
			//printf("%i\n",((int) ghost_vec.size()));
			//trace!!!!!!!
			count = 0;
			countII = 0;
			
			death_count = 0;
			while(count<ray_vec.size())
			{
				count = 0;
				countII++;
				
				death_count++;
				//if(death_count> domain_elemX + domain_elemY + domain_elemZ)
				if(death_count > domain_elemX * domain_elemY * domain_elemZ+1000)
					{
						for(i = 0;i < ray_vec.size(); i++)
						{
							ray_vec[i].obliterate();
							//printf("killed bundle \n");
						}
					}
					
					
					
				for(i = 0;i < ray_vec.size(); i++)
				{
									
	
					
					if (ray_vec[i].dead())
					{
						count += 1;
						continue;
					}				
					
					
					if(PathCutOff>0)
					{
						//printf("%f\n",(ray_vec[i].T() - ray_vec[i].oT()));
						if ((ray_vec[i].T() - ray_vec[i].oT())>PathCutOff)
						{
							//printf("die!\n");
							ray_vec[i].obliterate();
							continue;
						}
					}
					/*
					if(ray_vec[i].EX() > domain_elemX || ray_vec[i].EX() < 0 || ray_vec[i].EY() > domain_elemY || ray_vec[i].EY() < 0 || ray_vec[i].EZ() > domain_elemZ || ray_vec[i].EZ() < 0)
					{
						printf("narrowly avoied seg fault\n");
					
					}*/
					
					
					
					spare = (this->*TraceRay)(&ray_vec[i], trial);			
					
					if (spare>=0||spare == -4) // particle or homogenized scattering
					{
						ghost_vec.resize(ghost_vec.size()+1);
						FreePaths.at(trial).at((floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath*PathBins)<PathBins)?(floor((ray_vec[i].T() - ray_vec[i].oT())/MaxPath*PathBins)):PathBins-1)+=ray_vec[i].P(); 
						ghost_vec.back().ReadRay((ray_vec[i]), spare, ScatterPos[Scat]);
					}
					
					if (spare == -2) // hit domain sides
					{
						
						
						
						BoundaryOut(&ray_vec[i],trial);
						
						if (ray_vec[i].dead())
						{
							S = ray_vec[i].S();
							
							if (S +1>( Scatt_vec.at(trial).size()))
							{
								
								Scatt_vec.at(trial).resize(S +1);
								
							}
							
							Scatt_vec.at(trial).at(S) += 1;
						}
																							
					}
					
					if (spare == -3) // homogenized total absorbtion
					{
						S = ray_vec[i].S();
						if (S +1>( Scatt_vec.at(trial).size()))
						{
							Scatt_vec.at(trial).resize(S +1);
							
						}
						Scatt_vec.at(trial).at(S)+=1;
																											
					}
					
					
					
					
					
										
					
					
				}
				
				
	
				
				if (countII > domain_elemX* domain_elemY * domain_elemZ * 10000.0)
				{
					printf("total death: %d of %d (%d)\n",count,ray_vec.size(), countII);
					break;
				}
				
			}
			
			
			
			
		}
		
		
		
		
		
	}
	
	
	
	
	return 0;
}



int controller::RunTrial(int trial) // Broken?
{
	
	//printf("%i\n",((int) ghost_vec.size()));
	int rays_remaining;
	rays_remaining = NUM_RAYS;
	
	//bundle_size = 10;
	
	
	
	//map initiation
	int i,j;
	//FILE* BeamIn = fopen ("BeamIn.csv" , "w");
	
	
	
	
	
	//EmitMap.clear();
	std::vector<dcomp_counted> dumdum;
	dumdum.resize(mapxsamples);
	for(i = 0;i<mapysamples;i++)
	{
		//EmitMap.push_back(dumdum);
	}
	
	std::vector<ray> ray_vec;
	std::deque<ghost> ghost_vec;
		
	int SanityAmmount = 500000;
	end = time(0);
	TimeElapsed = difftime(end,start);
	printf("----  %i minutes and %i seconds have elapsed ----\n", (int) floor(TimeElapsed/60), (int) round(TimeElapsed - 60*floor(TimeElapsed/60))); 
	
	
	printf("---  ---  Begin Primary Trace  ---  --- \n");
	printf("Times at every %d Rays\n", SanityAmmount);
	
	ghost_vec.clear();
	int bundle;
	
	int count = 0;
	int spare;
	int flag;
	int num;
	
	int S;
	
	
	
	int death_count;
	
	
	while (rays_remaining > 0)
	{
		
		if ((rays_remaining%SanityAmmount) == 0)
		{
			end = time(0);
			TimeElapsed = difftime(end,start);
			printf("----  %i minutes and %i seconds have elapsed ----\n", (int) floor(TimeElapsed/60), (int) round(TimeElapsed - 60*floor(TimeElapsed/60)));
		}
		bundle = std::min(bundle_size,rays_remaining);
		
		ray_vec.clear();
		ray_vec.resize(bundle);
		
		//make a bundle of joy

		
		for(i = 0;i < bundle; i++)
		{
			(this->*TraceInFunc)(&(ray_vec.at(i)), trial);
			
			
		}
		
		
		
			
		
		
		
		//printf("%i\n",((int) ghost_vec.size()));
		//trace!!!!!!!
		count = 0;
		death_count = 0;
		
		while(count<bundle)
		{
			death_count++;
			if(death_count > domain_elemX * domain_elemY * domain_elemZ+10)
				{
					for(i = 0;i < ray_vec.size(); i++)
					{
						ray_vec[i].obliterate();
						printf("killed bundle \n");
					}
				}
			
			count = 0;
			for(i = 0;i < ray_vec.size(); i++)
			{
				
				

				if (ray_vec[i].dead())
				{
					count += 1;
					
					
					continue;
					
				}
								
				
				/*
				if(ray_vec[i].EX() > domain_elemX || ray_vec[i].EX() < 0 || ray_vec[i].EY() > domain_elemY || ray_vec[i].EY() < 0 || ray_vec[i].EZ() > domain_elemZ || ray_vec[i].EZ() < 0)
				{
					printf("narrowly avoied seg fault\n");
				
				}*/
				
				
				
				spare = (this->*TraceRay)(&ray_vec[i], trial);			
				
				if (spare>=0||spare == -4)
				{
					ghost_vec.resize(ghost_vec.size()+1);
					ghost_vec.back().ReadRay((ray_vec[i]), spare,  ScatterPos[Scat]);
				}
				
				if (spare == -2)
				{
					S = ray_vec[i].S();
					
					if (S +1>( Scatt_vec.at(trial).size()))
					{
						Scatt_vec.at(trial).resize(S +1);
						
					}
					Scatt_vec.at(trial).at(S)+=1;
					
					for(j = 0;j<TraceOuts.size();j++)
					{
						TraceOuts.at(j)->TraceRayOut(&ray_vec[i],trial);
					}
					
																						
				}
				
				if (spare == -3)
				{
					S = ray_vec[i].S();
					if (S +1>( Scatt_vec.at(trial).size()))
					{
						Scatt_vec.at(trial).resize(S +1);
						
					}
					
					Scatt_vec.at(trial).at(S)+=1;
																										
				}
				
							
				
			}
			
		}
		
		
		
		rays_remaining = rays_remaining - bundle;
	}
	
	
	
	
	
	
	printf("---  ---  Primary Trace  Complete ---  --- \n");
	printf("LOST AT EMISION : %i\n",Lost_at_emission);
	printf("LOST IN TRANSMISSION : %i\n",Lost_in_transmission);
	printf("LOST AT ABSORBTION : %i\n",Lost_at_particle);
	printf("LOST IN NUMERMICAL BS : %i\n",Lost_in_numbers);
	
	
	 
	
	
	//Scattering stuff
	
	
	
	int sanitycounter = 0;
	int count_per_iteration;
	j = 0;
	
	
	//ray test_ray;
	
	ray_vec.clear();
	
	while (ghost_vec.size() > 0 && j < iteration_limit)
	{
		end = time(0);
		TimeElapsed = difftime(end,start);
		printf("----  %i minutes and %i seconds have elapsed ----\n", (int) floor(TimeElapsed/60), (int) round(TimeElapsed - 60*floor(TimeElapsed/60)));
		printf("---  --- Sorting Ghosts  ---  --- \n");
		std::sort(ghost_vec.begin(), ghost_vec.end(), GhostSort);
		printf("---  ---  Secondary Trace %d ---  --- \n",j);
		j++;
		
		num = 0;
		count_per_iteration = ghost_vec.size();
		while (flag == 0|| num < count_per_iteration)
		{
			
			ray_vec.clear();
			for(i = 0;i < bundle_size; i++)
			{		
				flag = (this->*ScatFunc)(num, &ghost_vec, &ray_vec, trial);
				num++;
				sanitycounter++;	
				if(ray_vec.back().TestElem(Xelem_size,Yelem_size,Zelem_size) ==  true)
				{
					printf("houston we have a problem\n");
					ray_vec.back().TestElem(Xelem_size,Yelem_size,Zelem_size);
				}
								
			}
			if(sanitycounter == 1000000)
			{
				end = time(0);
				sanitycounter = 0;
				TimeElapsed = difftime(end,start);
				printf("----  %i minutes and %i seconds have elapsed ----\n", (int) floor(TimeElapsed/60), (int) round(TimeElapsed - 60*floor(TimeElapsed/60)));
			}
			
			
			bundle = ray_vec.size();
			count = 0;
			
			//printf("%i\n",ghost_vec.size());
			death_count = 0;
			while(count<bundle)
			{
				count = 0;
				death_count++;
				
				if(death_count > domain_elemX * domain_elemY * domain_elemZ+10)
				{
					for(i = 0;i < ray_vec.size(); i++)
					{
						ray_vec[i].obliterate();
						printf("killed bundle \n");
					}
				}
				
				for(i = 0;i < ray_vec.size(); i++)
				{

					if (ray_vec[i].dead())
					{
						count += 1;
					
						
						continue;
					
					}
								
					
					/*
					if(ray_vec[i].TestElem(Xelem_size,Yelem_size,Zelem_size) ==  true)
					{
						printf("houston we have a problem\n");
						ray_vec[i].TestElem(Xelem_size,Yelem_size,Zelem_size);
					}
					
					
					if(ray_vec[i].X() > domainX || ray_vec[i].X() < 0  || ray_vec[i].Y() > domainY || ray_vec[i].Y() < 0 || ray_vec[i].Z() > domainZ || ray_vec[i].Z() < 0)
					{
						printf("shit\n");
					}*/
					
					//test_ray = ray_vec[i];
					
					spare = (this->*TraceRay)(&ray_vec[i], trial);	
					
					/*
					if(ray_vec[i].TestElem(Xelem_size,Yelem_size,Zelem_size) ==  true)
					{
						printf("houston we have a problem\n");
						ray_vec[i].TestElem(Xelem_size,Yelem_size,Zelem_size);
					}
					
					
					if(ray_vec[i].TestElem(Xelem_size,Yelem_size,Zelem_size) ==  true)
					{
						printf("houston we have a problem\n");
						ray_vec[i].TestElem(Xelem_size,Yelem_size,Zelem_size);
						(this->*TraceRay)(&test_ray);
					}*/		
					
					
					if (spare == -2)
					{
						S = ray_vec[i].S();
						if (S+1 > Scatt_vec.at(trial).size())
						{
							Scatt_vec.at(trial).resize(S +1);
							
						}
						Scatt_vec.at(trial).at(S)+=1;
						for(j = 0;j<TraceOuts.size();j++)
						{
							TraceOuts.at(j)->TraceRayOut(&ray_vec[i],trial);
						}																	
					}
					
					if (spare == -3)
					{
						S = ray_vec[i].S();
						if (S+1 > Scatt_vec.at(trial).size())
						{
							Scatt_vec.at(trial).resize(S +1);
							
						}
						Scatt_vec.at(trial).at(S)+=1;
															
					}
				
				
				}
				
			}
			
		}
		
		
		
	}
	
	//fclose(BeamIn);
	
	
	
	
	//FullEmitMap.push_back(EmitMap);
	return 0;
}







