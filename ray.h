
#ifndef _RAY_
#define _RAY_
#include <vector>



class ray
{
	
	private:
	double x;
	double y;
	double z;
	
	int scatt;
	
	//directions, should be a unit vector.
	double dx;
	double dy;
	double dz;
	
	double power;
	double travel;
	double old_travel;
	double lambda;
	
	
	
	int elemX;
	int elemY;
	int elemZ;
	
	bool alive;
	bool scatted;
	
	public:
	std::vector<std::vector<double>> history;

	
	ray(){scatt = 0; old_travel = 0.0;};
	ray(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L);
	ray(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L, double T);
	ray(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L, int S);
	ray(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L, double T, int S);
	void fill(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L);
	void fill(double X, double Y,double Z,double Dx, double Dy, double Dz, double P, double L, double T);
	double DX() {return dx;};
	double DY() {return dy;};
	double DZ() {return dz;};
	double D(int i);
	double X() {return x;};
	double Y() {return y;};
	double Z() {return z;};
	double R(int i);
	void R(int i, double r);
	void D(int i, double r);
	double Lambda() {return lambda;};
	double Power() {return power;};
	void Power(double p) {power = p;};
	double P() {return power;};
	void X(double newX) {x = newX;};
	void Y(double newY) {y = newY;};
	void Z(double newZ) {z = newZ;};
	bool dead() {return !alive;};
	void set_elem( int X, int Y, int Z);
	void obliterate() {alive = false;};
	void resurrecte() {alive = true;};
	int EX() {return elemX;};
	int EY() {return elemY;};
	int EZ() {return elemZ;};	
	int S() {return scatt;};
	void S_up() {scatt++;};
	void EX(int a) { elemX = a;};
	void EY(int a) { elemY = a;};
	void EZ(int a) { elemZ = a;};	
	void E(int i, int e);
	int E(int i);
	double T(){return travel;};
	void T(double t){travel = t;};
	void aT(double t){travel += t;}; //add travel
	double oT(){return old_travel;};
	void oT(double t){old_travel = t;};
	double Phase(){return 0;};
	void SaveHistory(int i);
	void SaveHistory(int i, int j);
	void PrintData();
	void WriteHistory(int count, int trial);
	int TestElem(double Xelem_size,double Yelem_size,double Zelem_size);
	bool TestDomain(double xx,double yy, double zz);
	bool just_scatted(){return scatted;};
	void just_scatted(bool val) {scatted = val;};
	void attenuate(double beta, double distance);
	void attenuate(double R) {power *= R;};
};



class ghost
{
	private:
	double dx;
	double dy;
	double dz;
	
	double power;
	double lambda;
	
	int elemX;
	int elemY;
	int elemZ;
	
	int particle;
	
	double travel;
	
	
	int scatt;
	
	std::vector<std::vector<double>> history;
	
	public:
	
	std::vector<double> xyz;
	ghost();
	//ghost(int Particle);
	void ReadRay(ray Ray, int Particle, bool TakePos);
	double P() {return power;};
	void P(double pow) {power = pow;};
	int prt() {return particle;}
	void prt(int p) {particle = p;};
	double DX() {return dx;};
	double DY() {return dy;};
	double DZ() {return dz;};
	double Lambda() {return lambda;};
	int EX() {return elemX;};
	int EY() {return elemY;};
	int EZ() {return elemZ;};
	void T(double t){travel = t;};
	int S() {return scatt;};
	double T(){return travel;};
	void GiveHistory(ray *Ray){Ray->history = history;};
};

bool GhostSort(ghost a, ghost b);






#endif
