

#include <stdio.h>


#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <deque>
#include "controller.h"
#include <stdlib.h>
#include <math.h> 
#include "ray.h"
#include "ranf.h"
#include "optics.h"
#include <algorithm>
#include <chrono>


int controller::Init_TraceRay_Dull()
{
	return 1;
}

int controller::TraceRay_Dull(ray *Ray, int trial)
{
	
	
	int i;
	int keep = -1;
	int ParticleID;
	double impact;
	double scalefactor;
	
	
	double dist = domainX;
	double test;
	
	// smash into particles
	
	int dir1, dir2, dir3;
	
	double dir1pos, dir2pos, dir3pos;
	
	double Radius;
	
	std::vector<double> parm;

	
	#if TIMER
	 
	std::chrono::nanoseconds time_span;
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	#endif
	
	if(Ray->EX() >= domain_elemX || Ray->EX() < 0 || Ray->EY() >= domain_elemY || Ray->EY() < 0 || Ray->EZ() >= domain_elemZ || Ray->EZ() < 0)
	{
		printf("narrowly avoied seg fault\n");
		Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
		Lost_in_transmission +=1;
		return -2;
	}
	
	
	for (i = 0; i < mesh_overlapped[Ray->EX()][Ray->EY()][Ray->EZ()].size();i++)
	{
		ParticleID = mesh_overlapped[Ray->EX()][Ray->EY()][Ray->EZ()][i];
			
		
		
		if((ParticleZ[ParticleID]-Ray->Z()) * Ray->DZ() + (ParticleY[ParticleID]-Ray->Y()) * Ray->DY() + (ParticleX[ParticleID]-Ray->X()) * Ray->DX() > 0)
		{	
			Radius = ParticleParm(ParticleID,0)/2;
			test = sqr(ParticleZ[ParticleID]-Ray->Z()) + sqr(ParticleY[ParticleID]-Ray->Y()) + sqr( ParticleX[ParticleID]-Ray->X());
			if ((dist*dist) > test && test > Radius * Radius)
			{
				//mag of cross product of ray diection with vector from ray to particle	
				
		
				
				
				
				parm = ParmVec(ParticleID,Ray->Lambda());
				scalefactor = Optics.Ideal_Array.search(parm)->Qe;
				impact =   sqr(Ray->DY()*(ParticleZ[ParticleID]-Ray->Z())-(Ray->DZ()*(ParticleY[ParticleID]-Ray->Y()))) + sqr(Ray->DZ()*(ParticleX[ParticleID]-Ray->X())-(Ray->DX()*(ParticleZ[ParticleID]-Ray->Z()))) + sqr(Ray->DX()*(ParticleY[ParticleID]-Ray->Y())-(Ray->DY()*(ParticleX[ParticleID]-Ray->X()))) ;
				
				if (impact < (Radius*Radius * scalefactor))
				{			
					keep = i;
					dist = sqrt(test);
				}
			}
		}
		
	}
	
	
	
	
	
	
	if (keep != -1)
	{
		ParticleID = mesh_overlapped[Ray->EX()][Ray->EY()][Ray->EZ()][keep];
		
		Ray->obliterate();
		Lost_at_particle += 1;
		//printf("A Ray died a horrible death\n");
		//printf("%i\n",((int) ghost_vec.size()));
		//printf("%i\n",((int) ghost_vec.size()));
		#if TIMER
		t2 = std::chrono::high_resolution_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1);
		fprintf(TIMERFILE,"%i,%i,-1\n",time_span,i);
		#endif
		
		
		return ParticleID;
	
	}
	else
	// smash into walls
	{
		#if TIMER
		t2 = std::chrono::high_resolution_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1);
		fprintf(TIMERFILE,"%i,%i,",time_span,i);
		#endif
		dir1 = 1;
		dir2 = 2;
		dir3 = 3;
		if(fabs(Ray->DY())>fabs(Ray->DX()))
		{
			dir1 = 2;
			dir2 = 1;
		}
		
		
		if (fabs(Ray->D(dir3))>fabs(Ray->D(dir1)))
		{
			i = dir1;
			dir1 = dir3;
			dir3 = i;
		}
		if(fabs(Ray->D(dir3))>fabs(Ray->D(dir2)))
		{
			i = dir2;
			dir2 = dir3;
			dir3 = i;
		}
		
		if(Ray->D(dir1)>0)
		{
			scalefactor =  ((Ray->E(dir1)+1)*elem_size(dir1)-Ray->R(dir1))/Ray->D(dir1);
			i = 1;
		}
		else
		{
			scalefactor =  ((Ray->E(dir1))*elem_size(dir1)-Ray->R(dir1))/Ray->D(dir1);
			i = 0;
		}
		
		if(std::isnan(scalefactor))
		{
			printf("this is a pile of shit, %i ,%i, %f, %f, %f \n",dir1, Ray->E(dir1),elem_size(dir1),Ray->R(dir1),Ray->D(dir1));
			
		}
		
		 
		if(fabs(scalefactor)<.00000000000000001) //I'm getting this numberical error when the ray is stuck in the corner than makes it alernate between two elemenets forever
		{
			scalefactor = 0;
		}
		
		dir2pos = (Ray->D(dir2)*scalefactor+(Ray->R(dir2)-Ray->E(dir2)*elem_size(dir2)));
		dir3pos = Ray->D(dir3)*scalefactor+(Ray->R(dir3)-Ray->E(dir3)*elem_size(dir3));
		
		if(fabs(dir2pos)<.00000000000000001) //I'm getting this numberical error when the ray is stuck in the corner than makes it alernate between two elemenets forever
		{
			dir2pos = 0;
		}
		if(fabs(dir3pos)<.00000000000000001) //I'm getting this numberical error when the ray is stuck in the corner than makes it alernate between two elemenets forever
		{
			dir3pos = 0;
		}
		
		if(dir2pos>=0 && dir2pos<=elem_size(dir2) &&dir3pos >= 0 && dir3pos <= elem_size(dir3)) // smash on dir1 wall
		{
			Ray->R(dir1,(Ray->E(dir1)+i)*elem_size(dir1));
			Ray->R(dir2, dir2pos + Ray->E(dir2)*elem_size(dir2));
			Ray->R(dir3, dir3pos + Ray->E(dir3)*elem_size(dir3));
			Ray->E(dir1,Ray->E(dir1) + i * 2 - 1);
			
			if( Ray->E(dir1) >= domain_elem(dir1))
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir1);
				Lost_in_transmission +=1;
				
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
			}
			
			if( Ray->E(dir1) < 0)
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir1);
				Lost_in_transmission +=1;
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
			}
			
			#if TIMER
			t1 = std::chrono::high_resolution_clock::now();
			time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
			fprintf(TIMERFILE,"%i\n",time_span);
			#endif
			Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
			return -1;
		}
		if(dir3pos <= elem_size(dir3)/elem_size(dir2)*dir2pos && dir3pos >= elem_size(dir3)-elem_size(dir3)/elem_size(dir2)*dir2pos) // smash on right dir2
		{
			scalefactor =  ((Ray->E(dir2)+1)*elem_size(dir2)-Ray->R(dir2))/Ray->D(dir2);
			dir1pos = (Ray->D(dir1)*scalefactor+(Ray->R(dir1)-Ray->E(dir1)*elem_size(dir1)));
			dir3pos = (Ray->D(dir3)*scalefactor+(Ray->R(dir3)-Ray->E(dir3)*elem_size(dir3)));
			
			Ray->R(dir2,(Ray->E(dir2)+1)*elem_size(dir2));
			Ray->R(dir1, dir1pos + Ray->E(dir1)*elem_size(dir1));
			Ray->R(dir3, dir3pos + Ray->E(dir3)*elem_size(dir3));
			Ray->E(dir2,Ray->E(dir2) + 1);
			
			if( Ray->E(dir2) >= domain_elem(dir2))
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir2);
				Lost_in_transmission +=1;
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
			}
			
			#if TIMER
			t1 = std::chrono::high_resolution_clock::now();
			time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
			fprintf(TIMERFILE,"%i\n",time_span);
			#endif
			Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
			return -1;
			
		}
		
		if(dir3pos >= elem_size(dir3)/elem_size(dir2)*dir2pos && dir3pos <= elem_size(dir3)-elem_size(dir3)/elem_size(dir2)*dir2pos) //smash on left dir 2
		{
			scalefactor =  ((Ray->E(dir2))*elem_size(dir2)-Ray->R(dir2))/Ray->D(dir2);
			dir1pos = (Ray->D(dir1)*scalefactor+(Ray->R(dir1)-Ray->E(dir1)*elem_size(dir1)));
			dir3pos = (Ray->D(dir3)*scalefactor+(Ray->R(dir3)-Ray->E(dir3)*elem_size(dir3)));
			
			Ray->R(dir2,(Ray->E(dir2))*elem_size(dir2));
			Ray->R(dir1, dir1pos + Ray->E(dir1)*elem_size(dir1));
			Ray->R(dir3, dir3pos + Ray->E(dir3)*elem_size(dir3));
			Ray->E(dir2,Ray->E(dir2) -1);
			
			if( Ray->E(dir2) < 0)
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir2);
				Lost_in_transmission +=1;
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
			}
			
			#if TIMER
			t1 = std::chrono::high_resolution_clock::now();
			time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
			fprintf(TIMERFILE,"%i\n",time_span);
			#endif
			Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
			return -1;
			
		}
		
		if(dir3pos > elem_size(dir3)/elem_size(dir2)*dir2pos && dir3pos > elem_size(dir3)-elem_size(dir3)/elem_size(dir2)*dir2pos) //smash on uper dir3
		{
			scalefactor =  ((Ray->E(dir3)+1)*elem_size(dir3)-Ray->R(dir3))/Ray->D(dir3);
			dir1pos = (Ray->D(dir1)*scalefactor+(Ray->R(dir1)-Ray->E(dir1)*elem_size(dir1)));
			dir2pos = (Ray->D(dir2)*scalefactor+(Ray->R(dir2)-Ray->E(dir2)*elem_size(dir2)));
			
			Ray->R(dir3,(Ray->E(dir3)+1)*elem_size(dir3));
			Ray->R(dir1, dir1pos + Ray->E(dir1)*elem_size(dir1));
			Ray->R(dir2, dir2pos + Ray->E(dir2)*elem_size(dir2));
			Ray->E(dir3,Ray->E(dir3) + 1);
			
			if( Ray->E(dir3) >= domain_elem(dir3))
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir3);
				Lost_in_transmission +=1;
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
			}
			#if TIMER
			t1 = std::chrono::high_resolution_clock::now();
			if(Ray->EX() >= domain_elemX || Ray->EX() < 0 || Ray->EY() >= domain_elemY || Ray->EY() < 0 || Ray->EZ() >= domain_elemZ || Ray->EZ() < 0)
	{
		printf("narrowly avoied seg fault\n");
		return -2
	}time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
			fprintf(TIMERFILE,"%i\n",time_span);
			#endif
			Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
			return -1;
			
		}
		if(dir3pos < elem_size(dir3)/elem_size(dir2)*dir2pos && dir3pos < elem_size(dir3)-elem_size(dir3)/elem_size(dir2)*dir2pos) // dmash on lower dir3
		{
			scalefactor =  ((Ray->E(dir3))*elem_size(dir3)-Ray->R(dir3))/Ray->D(dir3);
			dir1pos = (Ray->D(dir1)*scalefactor+(Ray->R(dir1)-Ray->E(dir1)*elem_size(dir1)));
			dir2pos = (Ray->D(dir2)*scalefactor+(Ray->R(dir2)-Ray->E(dir2)*elem_size(dir2)));
			
			Ray->R(dir3,(Ray->E(dir3))*elem_size(dir3));
			Ray->R(dir1, dir1pos + Ray->E(dir1)*elem_size(dir1));
			Ray->R(dir2, dir2pos + Ray->E(dir2)*elem_size(dir2));
			Ray->E(dir3,Ray->E(dir3) -1);
			
			if( Ray->E(dir3) < 0)
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir3);
				Lost_in_transmission +=1;
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
				
			}
			
			#if TIMER
			t1 = std::chrono::high_resolution_clock::now();
			time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
			fprintf(TIMERFILE,"%i\n",time_span);
			#endif
			Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
			return -1;
			
		}
	}
	
	printf("jesus christ\n");
	
	
	Ray->obliterate();
	Lost_in_numbers += 1;
	
	
	
	Ray->R(dir1,Ray->R(dir1)+ranf(trial)*.000001*elem_size(dir1));
	Ray->R(dir2,Ray->R(dir2)+ranf(trial)*.000001*elem_size(dir2));
	Ray->R(dir3,Ray->R(dir3)+ranf(trial)*.000001*elem_size(dir3));
	
	#if TIMER
	t1 = std::chrono::high_resolution_clock::now();
	time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
	fprintf(TIMERFILE,"%i\n",time_span);
	#endif
	return 0;
}


int controller::Init_TraceRay_Phase()
{
	return 1;
}

int controller::TraceRay_Phase(ray *Ray, int trial)
{
	
	
	int i;
	int keep = -1;
	int ParticleID;
	double impact;
	double scalefactor;
	
	
	double dist = domainX;
	double test;
	
	// smash into particles
	
	int dir1, dir2, dir3;
	double ndir1, ndir2, ndir3; //new direction positions
	double dir1pos, dir2pos, dir3pos;
	
	double Radius;
	
	std::vector<double> parm;

	
	#if TIMER
	std::chrono::high_resolution_clock::time_point t2;
	std::chrono::nanoseconds time_span;
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	#endif
	
	if(Ray->EX() >= domain_elemX || Ray->EX() < 0 || Ray->EY() >= domain_elemY || Ray->EY() < 0 || Ray->EZ() >= domain_elemZ || Ray->EZ() < 0)
	{
		printf("narrowly avoied seg fault\n");
		Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
		Lost_in_transmission +=1;
		return -2;
	}
	
	
	
	for (i = 0; i < mesh_overlapped[Ray->EX()][Ray->EY()][Ray->EZ()].size();i++)
	{
		ParticleID = mesh_overlapped[Ray->EX()][Ray->EY()][Ray->EZ()][i];
			
		
		
		if((ParticleZ[ParticleID]-Ray->Z()) * Ray->DZ() + (ParticleY[ParticleID]-Ray->Y()) * Ray->DY() + (ParticleX[ParticleID]-Ray->X()) * Ray->DX() > 0)
		{	
			test = sqr(ParticleZ[ParticleID]-Ray->Z()) + sqr(ParticleY[ParticleID]-Ray->Y()) + sqr( ParticleX[ParticleID]-Ray->X());
			if ((dist*dist) > test)
			{
				//mag of cross product of ray diection with vector from ray to particle	
				
		
				Radius = ParticleParm(ParticleID,0)/2;
				parm = ParmVec(ParticleID,Ray->Lambda());
				impact =   1.0/Optics.Ideal_Array.search(parm)->Qe*sqr(Ray->DY()*(ParticleZ[ParticleID]-Ray->Z())-(Ray->DZ()*(ParticleY[ParticleID]-Ray->Y()))) + sqr(Ray->DZ()*(ParticleX[ParticleID]-Ray->X())-(Ray->DX()*(ParticleZ[ParticleID]-Ray->Z()))) + sqr(Ray->DX()*(ParticleY[ParticleID]-Ray->Y())-(Ray->DY()*(ParticleX[ParticleID]-Ray->X()))) ;
				
				if (impact < (Radius*Radius))
				{			
					keep = i;
					dist = sqrt(test);
				}
			}
		}
		
	}
	
	
	
	
	
	
	if (keep != -1)
	{
		ParticleID = mesh_overlapped[Ray->EX()][Ray->EY()][Ray->EZ()][keep];
		Ray->aT(Ray->DX() * (ParticleX[ParticleID]-Ray->X()) +  Ray->DY() * (ParticleY[ParticleID]-Ray->Y()) + Ray->DZ() * (ParticleZ[ParticleID]-Ray->Z()));
		Ray->obliterate();
		Lost_at_particle += 1;
		//printf("A Ray died a horrible death\n");
		//printf("%i\n",((int) ghost_vec.size()));
		//ghost_vec.resize(ghost_vec.size()+1);
		//ghost_vec.back().ReadRay((*Ray), ParticleID);
		//printf("%i\n",((int) ghost_vec.size()));
		#if TIMER
		t2 = std::chrono::high_resolution_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1);
		fprintf(TIMERFILE,"%i,%i,-1\n",time_span,i);
		#endif
		
		
		return ParticleID;
	
	}
	else
	// smash into walls
	{
		#if TIMER
		t2 = std::chrono::high_resolution_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1);
		fprintf(TIMERFILE,"%i,%i,",time_span,i);
		#endif
		dir1 = 1;
		dir2 = 2;
		dir3 = 3;
		if(fabs(Ray->DY())>fabs(Ray->DX()))
		{
			dir1 = 2;
			dir2 = 1;
		}
		
		
		if (fabs(Ray->D(dir3))>fabs(Ray->D(dir1)))
		{
			i = dir1;
			dir1 = dir3;
			dir3 = i;
		}
		if(fabs(Ray->D(dir3))>fabs(Ray->D(dir2)))
		{
			i = dir2;
			dir2 = dir3;
			dir3 = i;
		}
		
		if(Ray->D(dir1)>0)
		{
			scalefactor =  ((Ray->E(dir1)+1)*elem_size(dir1)-Ray->R(dir1))/Ray->D(dir1);
			i = 1;
		}
		else
		{
			scalefactor =  ((Ray->E(dir1))*elem_size(dir1)-Ray->R(dir1))/Ray->D(dir1);
			i = 0;
		}
		
		if(std::isnan(scalefactor))
		{
			printf("this is a pile of shit, %i ,%i, %f, %f, %f \n",dir1, Ray->E(dir1),elem_size(dir1),Ray->R(dir1),Ray->D(dir1));
			
		}
		
		 
		if(fabs(scalefactor)<.00000000000000001) //I'm getting this numberical error when the ray is stuck in the corner than makes it alernate between two elemenets forever
		{
			scalefactor = 0;
		}
		
		dir2pos = (Ray->D(dir2)*scalefactor+(Ray->R(dir2)-Ray->E(dir2)*elem_size(dir2)));
		dir3pos = Ray->D(dir3)*scalefactor+(Ray->R(dir3)-Ray->E(dir3)*elem_size(dir3));
		
		if(fabs(dir2pos)<.00000000000000001) //I'm getting this numberical error when the ray is stuck in the corner than makes it alernate between two elemenets forever
		{
			dir2pos = 0;
		}
		if(fabs(dir3pos)<.00000000000000001) //I'm getting this numberical error when the ray is stuck in the corner than makes it alernate between two elemenets forever
		{
			dir3pos = 0;
		}
		
		if(dir2pos>=0 && dir2pos<=elem_size(dir2) &&dir3pos >= 0 && dir3pos <= elem_size(dir3)) // smash on dir1 wall
		{
			ndir1 = (Ray->E(dir1)+i)*elem_size(dir1);
			ndir2 = dir2pos + Ray->E(dir2)*elem_size(dir2);
			ndir3 = dir3pos + Ray->E(dir3)*elem_size(dir3);
			Ray->aT(sqrt(sqr(Ray->R(dir1) - ndir1) + sqr(Ray->R(dir2) - ndir2) + sqr(Ray->R(dir3) - ndir3)));
			Ray->R(dir1, ndir1);
			Ray->R(dir2, ndir2);
			Ray->R(dir3, ndir3);
			Ray->E(dir1,Ray->E(dir1) + i * 2 - 1);
			
			
			if( Ray->E(dir1) >= domain_elem(dir1))
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir1);
				Lost_in_transmission +=1;
				
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
			}
			
			if( Ray->E(dir1) < 0)
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir1);
				Lost_in_transmission +=1;
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
			}
			
			#if TIMER
			t1 = std::chrono::high_resolution_clock::now();
			time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
			fprintf(TIMERFILE,"%i\n",time_span);
			#endif
			Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
			
			return -1;
		}
		if(dir3pos <= elem_size(dir3)/elem_size(dir2)*dir2pos && dir3pos >= elem_size(dir3)-elem_size(dir3)/elem_size(dir2)*dir2pos) // smash on right dir2
		{
			scalefactor =  ((Ray->E(dir2)+1)*elem_size(dir2)-Ray->R(dir2))/Ray->D(dir2);
			dir1pos = (Ray->D(dir1)*scalefactor+(Ray->R(dir1)-Ray->E(dir1)*elem_size(dir1)));
			dir3pos = (Ray->D(dir3)*scalefactor+(Ray->R(dir3)-Ray->E(dir3)*elem_size(dir3)));
			
			ndir1 =  dir1pos + Ray->E(dir1)*elem_size(dir1);
			ndir2 = (Ray->E(dir2)+1)*elem_size(dir2);
			ndir3 = dir3pos + Ray->E(dir3)*elem_size(dir3);
			Ray->aT(sqrt(sqr(Ray->R(dir1) - ndir1) + sqr(Ray->R(dir2) - ndir2) + sqr(Ray->R(dir3) - ndir3)));
			Ray->R(dir1, ndir1);
			Ray->R(dir2, ndir2);
			Ray->R(dir3, ndir3);
			Ray->E(dir2,Ray->E(dir2) + 1);
			
			if( Ray->E(dir2) >= domain_elem(dir2))
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir2);
				Lost_in_transmission +=1;
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
			}
			
			#if TIMER
			t1 = std::chrono::high_resolution_clock::now();
			time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
			fprintf(TIMERFILE,"%i\n",time_span);
			#endif
			Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
			
			return -1;
			
		}
		
		if(dir3pos >= elem_size(dir3)/elem_size(dir2)*dir2pos && dir3pos <= elem_size(dir3)-elem_size(dir3)/elem_size(dir2)*dir2pos) //smash on left dir 2
		{
			scalefactor =  ((Ray->E(dir2))*elem_size(dir2)-Ray->R(dir2))/Ray->D(dir2);
			dir1pos = (Ray->D(dir1)*scalefactor+(Ray->R(dir1)-Ray->E(dir1)*elem_size(dir1)));
			dir3pos = (Ray->D(dir3)*scalefactor+(Ray->R(dir3)-Ray->E(dir3)*elem_size(dir3)));
			
			ndir1 =  dir1pos + Ray->E(dir1)*elem_size(dir1);
			ndir2 = (Ray->E(dir2))*elem_size(dir2);
			ndir3 = dir3pos + Ray->E(dir3)*elem_size(dir3);
			Ray->aT(sqrt(sqr(Ray->R(dir1) - ndir1) + sqr(Ray->R(dir2) - ndir2) + sqr(Ray->R(dir3) - ndir3)));
			Ray->R(dir1, ndir1);
			Ray->R(dir2, ndir2);
			Ray->R(dir3, ndir3);

			Ray->E(dir2,Ray->E(dir2) -1);
			
			if( Ray->E(dir2) < 0)
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir2);
				Lost_in_transmission +=1;
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
			}
			
			#if TIMER
			t1 = std::chrono::high_resolution_clock::now();
			time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
			fprintf(TIMERFILE,"%i\n",time_span);
			#endif
			Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
			
			return -1;
			
		}
		
		if(dir3pos > elem_size(dir3)/elem_size(dir2)*dir2pos && dir3pos > elem_size(dir3)-elem_size(dir3)/elem_size(dir2)*dir2pos) //smash on uper dir3
		{
			scalefactor =  ((Ray->E(dir3)+1)*elem_size(dir3)-Ray->R(dir3))/Ray->D(dir3);
			dir1pos = (Ray->D(dir1)*scalefactor+(Ray->R(dir1)-Ray->E(dir1)*elem_size(dir1)));
			dir2pos = (Ray->D(dir2)*scalefactor+(Ray->R(dir2)-Ray->E(dir2)*elem_size(dir2)));
			
			ndir1 =  dir1pos + Ray->E(dir1)*elem_size(dir1);
			ndir2 = dir2pos + Ray->E(dir2)*elem_size(dir2);
			ndir3 = (Ray->E(dir3)+1)*elem_size(dir3);
			Ray->aT(sqrt(sqr(Ray->R(dir1) - ndir1) + sqr(Ray->R(dir2) - ndir2) + sqr(Ray->R(dir3) - ndir3)));
			Ray->R(dir1, ndir1);
			Ray->R(dir2, ndir2);
			Ray->R(dir3, ndir3);

			Ray->E(dir3,Ray->E(dir3) + 1);
			
			if( Ray->E(dir3) >= domain_elem(dir3))
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir3);
				Lost_in_transmission +=1;
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
			}
			#if TIMER
			t1 = std::chrono::high_resolution_clock::now();
			time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
			fprintf(TIMERFILE,"%i\n",time_span);
			#endif
			Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
			
			return -1;
			
		}
		if(dir3pos < elem_size(dir3)/elem_size(dir2)*dir2pos && dir3pos < elem_size(dir3)-elem_size(dir3)/elem_size(dir2)*dir2pos) // dmash on lower dir3
		{
			scalefactor =  ((Ray->E(dir3))*elem_size(dir3)-Ray->R(dir3))/Ray->D(dir3);
			dir1pos = (Ray->D(dir1)*scalefactor+(Ray->R(dir1)-Ray->E(dir1)*elem_size(dir1)));
			dir2pos = (Ray->D(dir2)*scalefactor+(Ray->R(dir2)-Ray->E(dir2)*elem_size(dir2)));
			
			ndir1 =  dir1pos + Ray->E(dir1)*elem_size(dir1);
			ndir2 =  dir2pos + Ray->E(dir2)*elem_size(dir2);
			ndir3 =  (Ray->E(dir3))*elem_size(dir3);
			Ray->aT(sqrt(sqr(Ray->R(dir1) - ndir1) + sqr(Ray->R(dir2) - ndir2) + sqr(Ray->R(dir3) - ndir3)));
			Ray->R(dir1, ndir1);
			Ray->R(dir2, ndir2);
			Ray->R(dir3, ndir3);
			
			Ray->E(dir3,Ray->E(dir3) -1);
			
			if( Ray->E(dir3) < 0)
			{
				Ray->obliterate();
				//printf("A Ray was lost in %d dir\n", dir3);
				Lost_in_transmission +=1;
				#if TIMER
				t1 = std::chrono::high_resolution_clock::now();
				time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
				fprintf(TIMERFILE,"%i\n",time_span);
				#endif
				Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
				return -2;
				
			}
			
			#if TIMER
			t1 = std::chrono::high_resolution_clock::now();
			time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
			fprintf(TIMERFILE,"%i\n",time_span);
			#endif
			Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
			
			return -1;
			
		}
	}
	
	printf("jesus christ\n");
	
	
	Ray->obliterate();
	Lost_in_numbers += 1;
	
	
	
	Ray->R(dir1,Ray->R(dir1)+ranf(trial)*.000001*elem_size(dir1));
	Ray->R(dir2,Ray->R(dir2)+ranf(trial)*.000001*elem_size(dir2));
	Ray->R(dir3,Ray->R(dir3)+ranf(trial)*.000001*elem_size(dir3));
	
	#if TIMER
	t1 = std::chrono::high_resolution_clock::now();
	time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t2);
	fprintf(TIMERFILE,"%i\n",time_span);
	#endif
	return 0;
}


int controller::Init_TraceRay_Dull_New()
{
	return 1;
	
}
int controller::TraceRay_Dull_New(ray *Ray, int trial)
{
	
	
	int i;
	int keep = -1;
	int ParticleID;
	double impact;
	double scalefactor;
	
	
	double dist = domainX;
	double test;
	
	// smash into particles
	
	int dir1, dir2, dir3;
	
	double dir1pos, dir2pos, dir3pos;
	
	double Radius;
	
	std::vector<double> parm;

	
	#if TIMER
	std::chrono::high_resolution_clock::time_point t2;
	std::chrono::nanoseconds time_span;
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	#endif
	
	/*
	if (Ray->TestElem(Xelem_size,Yelem_size,Zelem_size) == true)
			{
				Ray->E(1,floor(Ray->X()/Xelem_size));
				Ray->E(2,floor(Ray->Y()/Yelem_size));
				Ray->E(3,floor(Ray->Z()/Zelem_size));
				printf("We jumped an element\n");
				
								
			}*/
	
	if(Ray->EX() >= domain_elemX || Ray->EX() < 0 || Ray->EY() >= domain_elemY || Ray->EY() < 0 || Ray->EZ() >= domain_elemZ || Ray->EZ() < 0)
	{
		printf("narrowly avoied seg fault\n");
		Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
		Lost_in_transmission +=1;
		return -2;
	}
	
	
	
	
	for (i = 0; i < mesh_overlapped.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size();i++)
	{
		ParticleID = mesh_overlapped[Ray->EX()][Ray->EY()][Ray->EZ()][i];
			
		
		
		if((ParticleZ[ParticleID]-Ray->Z()) * Ray->DZ() + (ParticleY[ParticleID]-Ray->Y()) * Ray->DY() + (ParticleX[ParticleID]-Ray->X()) * Ray->DX() > 0)
		{	
			
			
			test = sqr(ParticleZ[ParticleID]-Ray->Z()) + sqr(ParticleY[ParticleID]-Ray->Y()) + sqr( ParticleX[ParticleID]-Ray->X());
			if ((dist*dist) > test )
			{
				
				Radius = ParticleParm(ParticleID,0)/2;
				parm = ParmVec(ParticleID,Ray->Lambda());
				scalefactor = Optics.Ideal_Array.search(parm)->Qe;
				
				if(test > Radius * Radius * scalefactor )
				{
						impact =   sqr(Ray->DY()*(ParticleZ[ParticleID]-Ray->Z())-(Ray->DZ()*(ParticleY[ParticleID]-Ray->Y()))) + sqr(Ray->DZ()*(ParticleX[ParticleID]-Ray->X())-(Ray->DX()*(ParticleZ[ParticleID]-Ray->Z()))) + sqr(Ray->DX()*(ParticleY[ParticleID]-Ray->Y())-(Ray->DY()*(ParticleX[ParticleID]-Ray->X()))) ;
				
				
					if (impact < (Radius*Radius * scalefactor))
					{			
						keep = i;
						
						dist = sqrt(test);
					}
				}
			}
		}
		
	}
	
	
	
	
	
	
	if (keep != -1)
	{
		ParticleID = mesh_overlapped[Ray->EX()][Ray->EY()][Ray->EZ()][keep];
		
		Ray->obliterate();
		Lost_at_particle += 1;

		test = (ParticleZ[ParticleID]-Ray->Z()) * Ray->DZ() + (ParticleY[ParticleID]-Ray->Y()) * Ray->DY() + (ParticleX[ParticleID]-Ray->X()) * Ray->DX();
		Ray->X(Ray->X() + test*Ray->DX());
		Ray->Y(Ray->Y() + test*Ray->DY());
		Ray->Z(Ray->Z() + test*Ray->DZ());
		if (test<0)
		{
			printf("huh?\n");
		}
		
		
		Ray->aT(test);
		
		
			
			
				
		//printf("A Ray died a horrible death\n");
		//printf("%i\n",((int) ghost_vec.size()));
		//ghost_vec.resize(ghost_vec.size()+1);
		//ghost_vec.back().ReadRay((*Ray), ParticleID);
		//printf("%i\n",((int) ghost_vec.size()));
		#if TIMER
		t2 = std::chrono::high_resolution_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1);
		fprintf(TIMERFILE,"%i,%i,-1\n",time_span,i);
		#endif
		
		
		return ParticleID;
	
	}
	else
	// smash into walls
	{
		
		
		
		std::vector<double> times(6); // txup,txdown,tyup,tydown,tzup,tzdown;
		std::vector<double>::iterator result;
		int index = 6;
		double maxy;
		double keepp;
		//std::vector<double> times_c(6);
		keepp = Xelem_size + Yelem_size + Zelem_size;
		times.at(0) = (Ray->DX()>0?((Ray->EX()+1)*Xelem_size - Ray->X())/(Ray->DX()):keepp);
		times.at(1) = (Ray->DX()<0?((Ray->EX())*Xelem_size - Ray->X())/(Ray->DX()):keepp);
		times.at(2) = (Ray->DY()>0?((Ray->EY()+1)*Yelem_size - Ray->Y())/(Ray->DY()):keepp);
		times.at(3) = (Ray->DY()<0?((Ray->EY())*Yelem_size - Ray->Y())/(Ray->DY()):keepp);
		times.at(4) = (Ray->DZ()>0?((Ray->EZ()+1)*Zelem_size - Ray->Z())/(Ray->DZ()):keepp);
		times.at(5) = (Ray->DZ()<0?((Ray->EZ())*Zelem_size - Ray->Z())/(Ray->DZ()):keepp);
		keepp = Xelem_size + Yelem_size + Zelem_size;
		
		//times_c = times;
		
		for(i = 0; i<times.size() ; i++)
		{
			
			//if (times.at(i) >= 0 && times.at(i) < keepp)
			if (times.at(i) < keepp)
			{
				index = i;
				keepp = times.at(i);
			}
			
		}
		
		if(index == 6)
		{
			maxy = *(max_element(times.begin(),times.end()));
			
			for(i = 0; i<times.size() ; i++)
			{
				
				if (times.at(i) <= 0 && times.at(i) != maxy)
				{
					times.at(i) = 1+maxy;
				}
				
			}
			
			
			result = min_element(times.begin(),times.end());
			index =  std::distance(times.begin(), result);
		}
		
		
		//Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
		



		Ray->X(Ray->X() + times.at(index) * (Ray->DX()));
		Ray->Y(Ray->Y() + times.at(index) * (Ray->DY()));
		Ray->Z(Ray->Z() + times.at(index) * (Ray->DZ()));
		Ray->just_scatted(0);
		if(PathLengths||PathCutOff>0)
		{
			Ray->aT(DISTANCE(0,0,0,times.at(index) * (Ray->DX()),times.at(index) * (Ray->DY()),times.at(index) * (Ray->DZ())));
			
		}
		
		if (index == 0)
		{
			Ray->EX(Ray->EX() +1 );
		}
		else if (index == 1)
		{
			Ray->EX(Ray->EX() -1 ) ;
		}
		else if (index == 2)
		{
			Ray->EY(Ray->EY() +1) ;
		}
		else if (index == 3)
		{
			Ray->EY(Ray->EY() -1 ) ;
		}
		else if (index == 4)
		{
			Ray->EZ(Ray->EZ()+1 );
		}
		else if (index == 5)
			{
				Ray->EZ(Ray->EZ() -1 ) ;
		}
		
		
		//Ray->TestElem(Xelem_size,Yelem_size,Zelem_size);
		
		
		if (Ray->EX() >= domain_elemX || Ray->EX() < 0 )
		{
			Ray->obliterate();
			//printf("A Ray was lost in 1 dir\n");
			Lost_in_transmission +=1;
			return -2;
		}
		else if (Ray->EY() >= domain_elemY || Ray->EY() < 0 )
		{
			Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
			Lost_in_transmission +=1;
			return -2;
		}
		else if (Ray->EZ() >= domain_elemZ || Ray->EZ() < 0 )
		{
			Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
			Lost_in_transmission +=1;
			return -2;
		}
		
		
		return -1;
		
	}
	
}




int controller::Init_TraceRay_Homogenized()
{
	
	printf("Homogenizing... ");
	
	
	mesh_properties.clear();
	
	Optics.emission_spectrum(&optics[0]);
	
	
	int i;
	int j;
	int k,p;
	
	mesh_properties.resize(domain_elemX);
	
	
	
	for (i = 0;i<domain_elemX;i++)
	{
		mesh_properties.at(i).resize(domain_elemY);
		
		
		for (j = 0;j<domain_elemY;j++)
		{
			mesh_properties.at(i).at(j).resize(domain_elemZ);
			
			for (k = 0;k<domain_elemZ;k++)
			{
				mesh_properties.at(i).at(j).at(k).resize(2);
			
			
			}
		}
		
	}
	
	
	
	std::vector<double> parms;
		
	int ParticleID;
		
	Particle_Ideal * Ideal;
		
	
		
	
	
	for(i = 0;i < mesh_exact.size() ; i++)
	{
		
		for (j = 0;j < mesh_exact.at(i).size() ; j++)
		{
			for (k = 0;k < mesh_exact.at(i).at(j).size() ; k++)
			{
				for(p = 0;p<mesh_exact.at(i).at(j).at(k).size();p++)
				{
					
					ParticleID = mesh_exact.at(i).at(j).at(k).at(p);
					parms = ParmVec(ParticleID,optics[1]);
					Ideal = Optics.Ideal_Array.search(parms);
					
					
					mesh_properties.at(i).at(j).at(k).at(0) += Ideal->Qa * M_PI*pow(ParticleParm(ParticleID,0),2)/4.0;
					mesh_properties.at(i).at(j).at(k).at(1) += Ideal->Qs * M_PI*pow(ParticleParm(ParticleID,0),2)/4.0;
					
				}
				
				mesh_properties.at(i).at(j).at(k).at(0) /= Xelem_size * Yelem_size * Zelem_size;
				mesh_properties.at(i).at(j).at(k).at(1) /= Xelem_size * Yelem_size * Zelem_size;
				
			}
		}
	}
	
	
	printf("Complete\n");
	return 0;
}



int controller::TraceRay_Homogenized(ray *Ray, int trial)
{
	
	
	int i;
	int keep = -1;
	int ParticleID;
	double impact;
	double scalefactor;
	
	double p_scatter;
	double p;
	double keepp;
	double energy;
	
	double dist = domainX;
	double test;
	
	bool scatt = false;
	
	// smash into particles
	
	int dir1, dir2, dir3;
	
	double dir1pos, dir2pos, dir3pos;
	
	double Radius;
	
	std::vector<double> parm;

	double maxy;
	
	#if TIMER
	std::chrono::high_resolution_clock::time_point t2;
	std::chrono::nanoseconds time_span;
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	#endif
	
	if(Ray->EX() >= domain_elemX || Ray->EX() < 0 || Ray->EY() >= domain_elemY || Ray->EY() < 0 || Ray->EZ() >= domain_elemZ || Ray->EZ() < 0)
	{
		printf("narrowly avoied seg fault\n");
		Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
		Lost_in_transmission +=1;
		return -2;
	}
	else
	{
		
		
		
		std::vector<double> times(6); // txup,txdown,tyup,tydown,tzup,tzdown;
		std::vector<double>::iterator result;
		int index;
		double keepp;
		
		//std::vector<double> times_c(6);
		
		times.at(0) = (Ray->DX()>0?((Ray->EX()+1)*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(1) = (Ray->DX()<0?((Ray->EX())*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(2) = (Ray->DY()>0?((Ray->EY()+1)*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(3) = (Ray->DY()<0?((Ray->EY())*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(4) = (Ray->DZ()>0?((Ray->EZ()+1)*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		times.at(5) = (Ray->DZ()<0?((Ray->EZ())*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		keepp = Xelem_size + Yelem_size + Zelem_size;
		
	
		
		//times_c = times;
		
		//printf("pos and dir and elem: %f,%f,%f,%f,%f,%f,%d,%d,%d\n",-(Ray->EX())*Xelem_size + Ray->X(),-(Ray->EY())*Yelem_size + Ray->Y(),-(Ray->EZ())*Zelem_size + Ray->Z(),Ray->DX(),Ray->DY(),Ray->DZ(), Ray->EX(),Ray->EY(),Ray->EZ());
		
		
		//printf("times:");
		for(i = 0; i<times.size() ; i++)
		{
			//printf("%f,", times.at(i));
			if (times.at(i) >= 0 && times.at(i) < keepp)
			{
				index = i;
				keepp = times.at(i);
			}
			
		}
		
		//printf("index: %d\n",index);
		
		if(index == 6)
		{
			printf("our worst fears \n");
		}
		
		
		
		
		if(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() > 0)
		{
			p_scatter = exp(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(1) * times.at(index));
			p = ranf(trial);
			
			if(p>p_scatter)
			{
				times.at(index) = -(log(p)/mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(1));
				
				scatt = true;
			}
			
	
			energy = Ray->Power() * (1 - expl(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * times.at(index)));
			
			
			
			ParticleQ.at(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(floor(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() * ranf(trial)))).at(trial) += energy;
			
			Ray->Power(Ray->Power() - energy);
			
		}
		Ray->X(Ray->X() + times.at(index) * (Ray->DX()));
		Ray->Y(Ray->Y() + times.at(index) * (Ray->DY()));
		Ray->Z(Ray->Z() + times.at(index) * (Ray->DZ()));
		Ray->just_scatted(0);
		
		if(PathLengths||PathCutOff>0)
		{
			Ray->aT(DISTANCE(0,0,0,times.at(index) * (Ray->DX()),times.at(index) * (Ray->DY()),times.at(index) * (Ray->DZ())));
			
		}
		
		if(scatt)
		{
			Ray->obliterate();
			Lost_at_particle += 1;
			//printf("A Ray died a horrible death\n");
			//printf("%i\n",((int) ghost_vec.size()));
			//ghost_vec.resize(ghost_vec.size()+1);
			//ghost_vec.back().ReadRay((*Ray), -1);
			
			if(Ray->X() > domainX || Ray->X() < 0  || Ray->Y() > domainY || Ray->Y() < 0 || Ray->Z() > domainZ || Ray->Z() < 0)
			{
				printf("shit");
			}
			
			return -4;
			
		}
		else
		{
			
			if (index == 0)
			{
				Ray->EX(Ray->EX() +1 );
			}
			else if (index == 1)
			{
				Ray->EX(Ray->EX() -1 ) ;
			}
			else if (index == 2)
			{
				Ray->EY(Ray->EY() +1) ;
			}
			else if (index == 3)
			{
				Ray->EY(Ray->EY() -1 ) ;
			}
			else if (index == 4)
			{
				Ray->EZ(Ray->EZ()+1 );
			}
			else if (index == 5)
				{
					Ray->EZ(Ray->EZ() -1 ) ;
			}
			
			
			if (Ray->EX() >= domain_elemX || Ray->EX() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 1 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EY() >= domain_elemY || Ray->EY() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EZ() >= domain_elemZ || Ray->EZ() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			
			
			return -1;
		}
	}
	
}



int controller::Init_TraceRay_Homogenized_II()
{
	
	printf("Homogenizing... ");
	
	
	mesh_properties.clear();
	
	Optics.emission_spectrum(&optics[0]);
	
	
	int i;
	int j;
	int k,p;
	
	mesh_properties.resize(domain_elemX);
	
	
	
	for (i = 0;i<domain_elemX;i++)
	{
		mesh_properties.at(i).resize(domain_elemY);
		
		
		for (j = 0;j<domain_elemY;j++)
		{
			mesh_properties.at(i).at(j).resize(domain_elemZ);
			
			for (k = 0;k<domain_elemZ;k++)
			{
				mesh_properties.at(i).at(j).at(k).resize(2);
			
			
			}
		}
		
	}
	
	
	
	std::vector<double> parms;
		
	int ParticleID;
		
	Particle_Ideal * Ideal;
		
	int count = 0;
		
		
	for(i = 0;i < mesh_exact.size() ; i++)
	{
		
		for (j = 0;j < mesh_exact.at(i).size() ; j++)
		{
			for (k = 0;k < mesh_exact.at(i).at(j).size() ; k++)
			{
				count = 0;
				for(p = 0;p<mesh_exact.at(i).at(j).at(k).size();p++)
				{
					
					ParticleID = mesh_exact.at(i).at(j).at(k).at(p);
					parms = ParmVec(ParticleID,optics[1]);
					Ideal = Optics.Ideal_Array.search(parms);
					
					
					mesh_properties.at(i).at(j).at(k).at(0) += Ideal->Qe * M_PI*pow(ParticleParm(ParticleID,0),2)/4.0;
					mesh_properties.at(i).at(j).at(k).at(1) += Ideal->Qa /Ideal->Qe;
					count++;
					
				}
				
				mesh_properties.at(i).at(j).at(k).at(0) /= Xelem_size * Yelem_size * Zelem_size;
				mesh_properties.at(i).at(j).at(k).at(1) /= count;
			}
		}
	}
	
	
	printf("Complete\n");
	return 0;
}



int controller::TraceRay_Homogenized_II(ray *Ray, int trial)
{
	
	
	int i;
	int keep = -1;
	int ParticleID;
	double impact;
	double scalefactor;
	
	double p_scatter;
	double p;
	
	double energy;
	
	double dist = domainX;
	double test;
	
	bool scatt = false;
	
	// smash into particles
	
	int dir1, dir2, dir3;
	
	double dir1pos, dir2pos, dir3pos;
	
	double Radius;
	
	std::vector<double> parm;

	double maxy;
	
	#if TIMER
	std::chrono::high_resolution_clock::time_point t2;
	std::chrono::nanoseconds time_span;
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	#endif
	
	if(Ray->EX() >= domain_elemX || Ray->EX() < 0 || Ray->EY() >= domain_elemY || Ray->EY() < 0 || Ray->EZ() >= domain_elemZ || Ray->EZ() < 0)
	{
		printf("narrowly avoied seg fault, %d, %d, %d\n ", Ray->EX(), Ray->EY(), Ray->EZ());
		Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
		Lost_in_transmission +=1;
		return -2;
	}
	else
	{
		
		
		
		std::vector<double> times(6); // txup,txdown,tyup,tydown,tzup,tzdown;
		std::vector<double>::iterator result;
		int index = 6;
		double keepp;
		
		
		//std::vector<double> times_c(6);
		
		times.at(0) = (Ray->DX()>0?((Ray->EX()+1)*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(1) = (Ray->DX()<0?((Ray->EX())*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(2) = (Ray->DY()>0?((Ray->EY()+1)*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(3) = (Ray->DY()<0?((Ray->EY())*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(4) = (Ray->DZ()>0?((Ray->EZ()+1)*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		times.at(5) = (Ray->DZ()<0?((Ray->EZ())*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		keepp = Xelem_size + Yelem_size + Zelem_size;
		
		//times_c = times;
		
		for(i = 0; i<times.size() ; i++)
		{
			
			if (times.at(i) >= 0 && times.at(i) < keepp)
			{
				index = i;
				keepp = times.at(i);
			}
			
		}
		
		if(index == 6)
		{
			maxy = *(max_element(times.begin(),times.end()));
			
			for(i = 0; i<times.size() ; i++)
			{
				
				if (times.at(i) <= 0 && times.at(i) != maxy)
				{
					times.at(i) = 1+maxy;
				}
				
			}
			
			
			result = min_element(times.begin(),times.end());
			index =  std::distance(times.begin(), result);
		}
		
		if(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() > 0)
		{
			p_scatter = exp(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * times.at(index));
			p = ranf(trial);
			
			if(p>p_scatter)
			{
				times.at(index) = -(log(p)/mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0));
				
				scatt = true;
				
				energy = Ray->Power() * mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(1);
			
			
			
				ParticleQ.at(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(floor(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() * ranf(trial)))).at(trial) += energy;
			
				Ray->Power(Ray->Power() - energy);
			}
			
	
			
			
		}
		Ray->X(Ray->X() + times.at(index) * (Ray->DX()));
		Ray->Y(Ray->Y() + times.at(index) * (Ray->DY()));
		Ray->Z(Ray->Z() + times.at(index) * (Ray->DZ()));
		Ray->just_scatted(0);
		if(PathLengths||PathCutOff>0)
		{
			Ray->aT(DISTANCE(0,0,0,times.at(index) * (Ray->DX()),times.at(index) * (Ray->DY()),times.at(index) * (Ray->DZ())));
			
		}
		
		if(scatt)
		{
			Ray->obliterate();
			Lost_at_particle += 1;
			//printf("A Ray died a horrible death\n");
			//printf("%i\n",((int) ghost_vec.size()));
			//ghost_vec.resize(ghost_vec.size()+1);
			//ghost_vec.back().ReadRay((*Ray), -1);
			
			if(Ray->X() > domainX || Ray->X() < 0  || Ray->Y() > domainY || Ray->Y() < 0 || Ray->Z() > domainZ || Ray->Z() < 0)
			{
				printf("shit");
			}
			
			return -4;
			
		}
		else
		{
			
			if (index == 0)
			{
				Ray->EX(Ray->EX() +1 );
			}
			else if (index == 1)
			{
				Ray->EX(Ray->EX() -1 ) ;
			}
			else if (index == 2)
			{
				Ray->EY(Ray->EY() +1) ;
			}
			else if (index == 3)
			{
				Ray->EY(Ray->EY() -1 ) ;
			}
			else if (index == 4)
			{
				Ray->EZ(Ray->EZ()+1 );
			}
			else if (index == 5)
				{
					Ray->EZ(Ray->EZ() -1 ) ;
			}
			
			
			if (Ray->EX() >= domain_elemX || Ray->EX() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 1 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EY() >= domain_elemY || Ray->EY() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EZ() >= domain_elemZ || Ray->EZ() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			
			
			return -1;
		}
	}
	
}

int controller::Init_TraceRay_Homogenized_II_offset()
{
	
	printf("Homogenizing... ");
	
	
	mesh_properties.clear();
	
	Optics.emission_spectrum(&optics[0]);
	
	
	int i;
	int j;
	int k,p;
	
	mesh_properties.resize(domain_elemX);
	
	
	
	for (i = 0;i<domain_elemX;i++)
	{
		mesh_properties.at(i).resize(domain_elemY);
		
		
		for (j = 0;j<domain_elemY;j++)
		{
			mesh_properties.at(i).at(j).resize(domain_elemZ);
			
			for (k = 0;k<domain_elemZ;k++)
			{
				mesh_properties.at(i).at(j).at(k).resize(3);
			
			
			}
		}
		
	}
	
	
	
	std::vector<double> parms;
		
	int ParticleID;
		
	Particle_Ideal * Ideal;
		
	int count = 0;
		
		
	for(i = 0;i < mesh_exact.size() ; i++)
	{
		
		for (j = 0;j < mesh_exact.at(i).size() ; j++)
		{
			for (k = 0;k < mesh_exact.at(i).at(j).size() ; k++)
			{
				count = 0;
				for(p = 0;p<mesh_exact.at(i).at(j).at(k).size();p++)
				{
					
					ParticleID = mesh_exact.at(i).at(j).at(k).at(p);
					parms = ParmVec(ParticleID,optics[1]);
					Ideal = Optics.Ideal_Array.search(parms);
					
					
					mesh_properties.at(i).at(j).at(k).at(0) += Ideal->Qe * M_PI*pow(ParticleParm(ParticleID,0),2)/4.0;
					mesh_properties.at(i).at(j).at(k).at(1) += Ideal->Qa /Ideal->Qe;
					mesh_properties.at(i).at(j).at(k).at(2) += ParticleParm(ParticleID,0);
					count++;
					
				}
				
				mesh_properties.at(i).at(j).at(k).at(0) /= Xelem_size * Yelem_size * Zelem_size;
				mesh_properties.at(i).at(j).at(k).at(1) /= count;
				mesh_properties.at(i).at(j).at(k).at(2) /= count;
			}
		}
	}
	
	
	printf("Complete\n");
	return 0;
}



int controller::TraceRay_Homogenized_II_offset(ray *Ray, int trial)
{
	
	
	int i;
	int keep = -1;
	int ParticleID;
	double impact;
	double scalefactor;
	
	double p_scatter;
	double p;
	
	double energy;
	
	double dist = domainX;
	double test;
	
	bool scatt = false;
	
	// smash into particles
	
	int dir1, dir2, dir3;
	
	double dir1pos, dir2pos, dir3pos;
	
	double Radius;
	
	std::vector<double> parm;

	double maxy;
	
	#if TIMER
	std::chrono::high_resolution_clock::time_point t2;
	std::chrono::nanoseconds time_span;
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	#endif
	
	if(Ray->EX() >= domain_elemX || Ray->EX() < 0 || Ray->EY() >= domain_elemY || Ray->EY() < 0 || Ray->EZ() >= domain_elemZ || Ray->EZ() < 0)
	{
		printf("narrowly avoied seg fault, %d, %d, %d\n ", Ray->EX(), Ray->EY(), Ray->EZ());
		Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
		Lost_in_transmission +=1;
		return -2;
	}
	else
	{
		
		
		
		std::vector<double> times(6); // txup,txdown,tyup,tydown,tzup,tzdown;
		std::vector<double>::iterator result;
		int index = 6;
		double keepp;
		
		
		//std::vector<double> times_c(6);
		
		times.at(0) = (Ray->DX()>0?((Ray->EX()+1)*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(1) = (Ray->DX()<0?((Ray->EX())*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(2) = (Ray->DY()>0?((Ray->EY()+1)*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(3) = (Ray->DY()<0?((Ray->EY())*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(4) = (Ray->DZ()>0?((Ray->EZ()+1)*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		times.at(5) = (Ray->DZ()<0?((Ray->EZ())*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		keepp = Xelem_size + Yelem_size + Zelem_size;
		
		//times_c = times;
		
		for(i = 0; i<times.size() ; i++)
		{
			
			if (times.at(i) >= 0 && times.at(i) < keepp)
			{
				index = i;
				keepp = times.at(i);
			}
			
		}
		
		if(index == 6)
		{
			maxy = *(max_element(times.begin(),times.end()));
			
			for(i = 0; i<times.size() ; i++)
			{
				
				if (times.at(i) <= 0 && times.at(i) != maxy)
				{
					times.at(i) = 1+maxy;
				}
				
			}
			
			
			result = min_element(times.begin(),times.end());
			index =  std::distance(times.begin(), result);
		}
		
		if(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() > 0)
		{
			
			p = ranf(trial);
			
			
			if(Ray->just_scatted())
			{
				p_scatter = exp(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * (times.at(index) - mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(2)));
				//p_scatter = exp(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * times.at(index));
				//printf("%f\n",mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(2));
			}
			else
			{
				p_scatter = exp(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * times.at(index));
			}
			
			if(p>p_scatter)
			{
				times.at(index) = -(log(p)/mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0));
				
				scatt = true;
				
				energy = Ray->Power() * mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(1);
			
			
			
				ParticleQ.at(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(floor(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() * ranf(trial)))).at(trial) += energy;
			
				Ray->Power(Ray->Power() - energy);
			}
			
	
			
			
		}
		Ray->X(Ray->X() + times.at(index) * (Ray->DX()));
		Ray->Y(Ray->Y() + times.at(index) * (Ray->DY()));
		Ray->Z(Ray->Z() + times.at(index) * (Ray->DZ()));
		Ray->just_scatted(0);
		if(PathLengths||PathCutOff>0)
		{
			Ray->aT(DISTANCE(0,0,0,times.at(index) * (Ray->DX()),times.at(index) * (Ray->DY()),times.at(index) * (Ray->DZ())));
			
		}
		
		if(scatt)
		{
			Ray->obliterate();
			Lost_at_particle += 1;
			//printf("A Ray died a horrible death\n");
			//printf("%i\n",((int) ghost_vec.size()));
			//ghost_vec.resize(ghost_vec.size()+1);
			//ghost_vec.back().ReadRay((*Ray), -1);
			
			if(Ray->X() > domainX || Ray->X() < 0  || Ray->Y() > domainY || Ray->Y() < 0 || Ray->Z() > domainZ || Ray->Z() < 0)
			{
				printf("shit");
			}
			
			return -4;
			
		}
		else
		{
			
			if (index == 0)
			{
				Ray->EX(Ray->EX() +1 );
			}
			else if (index == 1)
			{
				Ray->EX(Ray->EX() -1 ) ;
			}
			else if (index == 2)
			{
				Ray->EY(Ray->EY() +1) ;
			}
			else if (index == 3)
			{
				Ray->EY(Ray->EY() -1 ) ;
			}
			else if (index == 4)
			{
				Ray->EZ(Ray->EZ()+1 );
			}
			else if (index == 5)
				{
					Ray->EZ(Ray->EZ() -1 ) ;
			}
			
			
			if (Ray->EX() >= domain_elemX || Ray->EX() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 1 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EY() >= domain_elemY || Ray->EY() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EZ() >= domain_elemZ || Ray->EZ() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			
			
			return -1;
		}
	}
	
}



int controller::Init_TraceRay_Homogenized_III()
{
	
	printf("Homogenizing... ");
	
	
	mesh_properties.clear();
	
	Optics.emission_spectrum(&optics[0]);
	
	
	int i;
	int j;
	int k,p;
	
	mesh_properties.resize(domain_elemX);
	
	
	
	for (i = 0;i<domain_elemX;i++)
	{
		mesh_properties.at(i).resize(domain_elemY);
		
		
		for (j = 0;j<domain_elemY;j++)
		{
			mesh_properties.at(i).at(j).resize(domain_elemZ);
			
			for (k = 0;k<domain_elemZ;k++)
			{
				mesh_properties.at(i).at(j).at(k).resize(2);
			
			
			}
		}
		
	}
	
	
	
	std::vector<double> parms;
		
	int ParticleID;
		
	Particle_Ideal * Ideal;
		
	int count = 0;
		
	
	
	for(i = 0;i < mesh_exact.size() ; i++)
	{
		
		for (j = 0;j < mesh_exact.at(i).size() ; j++)
		{
			for (k = 0;k < mesh_exact.at(i).at(j).size() ; k++)
			{
				count = 0;
				for(p = 0;p<mesh_exact.at(i).at(j).at(k).size();p++)
				{
					
					ParticleID = mesh_exact.at(i).at(j).at(k).at(p);
					parms = ParmVec(ParticleID,optics[1]);
					Ideal = Optics.Ideal_Array.search(parms);
					
					mesh_properties.at(i).at(j).at(k).at(0) += Ideal->Qe * M_PI*pow(ParticleParm(ParticleID,0),2)/4.0;
					mesh_properties.at(i).at(j).at(k).at(1) += Ideal->Qa /Ideal->Qe;
					count++;
					
				}
				
				mesh_properties.at(i).at(j).at(k).at(0) /= Xelem_size * Yelem_size * Zelem_size;
				mesh_properties.at(i).at(j).at(k).at(1) /= count;
			}
		}
	}
	
	
	printf("Complete\n");
	
	return 1;
	
}



int controller::TraceRay_Homogenized_III(ray *Ray, int trial)
{
	
	
	int i;
	int keep = -1;
	int ParticleID;
	double impact;
	double scalefactor;
	
	double p_scatter;
	double p;
	
	double energy;
	
	double dist = domainX;
	double test;
	
	bool scatt = false;
	
	// smash into particles
	
	int dir1, dir2, dir3;
	
	double dir1pos, dir2pos, dir3pos;
	
	double Radius;
	
	std::vector<double> parm;

	double maxy;
	
	#if TIMER
	std::chrono::high_resolution_clock::time_point t2;
	std::chrono::nanoseconds time_span;
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	#endif
	
	if(Ray->EX() >= domain_elemX || Ray->EX() < 0 || Ray->EY() >= domain_elemY || Ray->EY() < 0 || Ray->EZ() >= domain_elemZ || Ray->EZ() < 0)
	{
		printf("narrowly avoied seg fault\n");
		Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
		Lost_in_transmission +=1;
		return -2;
	}
	else
	{
		
		
		
		std::vector<double> times(6); // txup,txdown,tyup,tydown,tzup,tzdown;
		std::vector<double>::iterator result;
		int index = 6;
		int keep;
		double keepp;
		/*std::vector<double> times_c(6);
		
		if(Ray->X() > domainX || Ray->X() < 0  || Ray->Y() > domainY || Ray->Y() < 0 || Ray->Z() > domainZ || Ray->Z() < 0)
		{
			printf("shit");
		}
		
		
		ray Test_Ray = *Ray;*/
		
		times.at(0) = (Ray->DX()>0?((Ray->EX()+1)*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(1) = (Ray->DX()<0?((Ray->EX())*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(2) = (Ray->DY()>0?((Ray->EY()+1)*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(3) = (Ray->DY()<0?((Ray->EY())*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(4) = (Ray->DZ()>0?((Ray->EZ()+1)*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		times.at(5) = (Ray->DZ()<0?((Ray->EZ())*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		keepp = Xelem_size + Yelem_size + Zelem_size;
		
		//times_c = times;
		
		for(i = 0; i<times.size() ; i++)
		{
			
			if (times.at(i) >= 0 && times.at(i) < keepp)
			{
				index = i;
				keepp = times.at(i);
			}
			
		}
		
		if(index == 6)
		{
			maxy = *(max_element(times.begin(),times.end()));
			
			for(i = 0; i<times.size() ; i++)
			{
				
				if (times.at(i) <= 0 && times.at(i) != maxy)
				{
					times.at(i) = 1+maxy;
				}
				
			}
			
			
			result = min_element(times.begin(),times.end());
			index =  std::distance(times.begin(), result);
		}
		
		
		
		if(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() > 0)
		{
			
			p = ranf(trial);
			
			p_scatter = exp(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * times.at(index));
			
			if(p>p_scatter)
			{
							
				
				
				double q = ranf(trial);
				
				if(q > mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(1))
				{
					//scatter
					
					times.at(index) = -(log(p)/mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0));
				
					scatt = true;
					
				}
				else
				{
					//absorb
					ParticleQ.at(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(floor(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() * ranf(trial)))).at(trial) += Ray->Power();
					
					Lost_at_particle += 1;
					
					Ray->S_up();
					
					Ray->obliterate();
					
					return -3;
					
				}
				
				
				
				
			}
			
	
			
			
		}
		Ray->X(Ray->X() + times.at(index) * (Ray->DX()));
		Ray->Y(Ray->Y() + times.at(index) * (Ray->DY()));
		Ray->Z(Ray->Z() + times.at(index) * (Ray->DZ()));
		Ray->just_scatted(0);
		if(PathLengths||PathCutOff>0)
		{
			Ray->aT(DISTANCE(0,0,0,times.at(index) * (Ray->DX()),times.at(index) * (Ray->DY()),times.at(index) * (Ray->DZ())));
			
		}
		
		if(scatt)
		{
			Ray->obliterate();
			Lost_at_particle += 1;
			//printf("A Ray died a horrible death\n");
			//printf("%i\n",((int) ghost_vec.size()));
			//ghost_vec.resize(ghost_vec.size()+1);
			//ghost_vec.back().ReadRay((*Ray), -1);
			
			if(Ray->X() > domainX || Ray->X() < 0  || Ray->Y() > domainY || Ray->Y() < 0 || Ray->Z() > domainZ || Ray->Z() < 0)
			{
				printf("shit");
			}
			
			return -4;
			
		}
		else
		{
			
			
			if (index == 0)
			{
				Ray->EX(Ray->EX() +1 );
			}
			else if (index == 1)
			{
				Ray->EX(Ray->EX() -1 ) ;
			}
			else if (index == 2)
			{
				Ray->EY(Ray->EY() +1) ;
			}
			else if (index == 3)
			{
				Ray->EY(Ray->EY() -1 ) ;
			}
			else if (index == 4)
			{
				Ray->EZ(Ray->EZ()+1 );
			}
			else if (index == 5)
				{
					Ray->EZ(Ray->EZ() -1 ) ;
			}
			
			/*
			times_c.at(0) = ((Ray->EX()+1)*Xelem_size - Ray->X())/(Ray->DX());
			times_c.at(1) = ((Ray->EX())*Xelem_size - Ray->X())/(Ray->DX());
			times_c.at(2) = ((Ray->EY()+1)*Yelem_size - Ray->Y())/(Ray->DY());
			times_c.at(3) = ((Ray->EY())*Yelem_size - Ray->Y())/(Ray->DY());
			times_c.at(4) = ((Ray->EZ()+1)*Zelem_size - Ray->Z())/(Ray->DZ());
			times_c.at(5) = ((Ray->EZ())*Zelem_size - Ray->Z())/(Ray->DZ());
			
			if(std::max(times_c.at(0),times_c.at(1))==0 || std::max(times_c.at(2),times_c.at(3))==0 ||std::max(times_c.at(4),times_c.at(5))==0)
			{
				printf("wtf... really??\n");
			}
			
			
			if(times_c.at(0) * times_c.at(1) > 10e-10 )
			{
				printf("wtf... really??\n");
			}
			
			if(times_c.at(2) * times_c.at(3) > 10e-10 )
			{
				printf("wtf... really??\n");
			}
			
			if(times_c.at(4) * times_c.at(5) > 10e-10 )
			{
				printf("wtf... really??\n");
			}
			
			
			if(Ray->TestElem(Xelem_size,Yelem_size,Zelem_size))
			{
				printf("really??\n");
			}*/
			
			if (Ray->EX() >= domain_elemX || Ray->EX() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 1 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EY() >= domain_elemY || Ray->EY() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EZ() >= domain_elemZ || Ray->EZ() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			
			
			return -1;
		}
	}
	
}

int controller::Init_TraceRay_Homogenized_III_offset()
{
	
	printf("Homogenizing... ");
	
	
	mesh_properties.clear();
	
	Optics.emission_spectrum(&optics[0]);
	
	
	int i;
	int j;
	int k,p;
	
	mesh_properties.resize(domain_elemX);
	
	
	
	for (i = 0;i<domain_elemX;i++)
	{
		mesh_properties.at(i).resize(domain_elemY);
		
		
		for (j = 0;j<domain_elemY;j++)
		{
			mesh_properties.at(i).at(j).resize(domain_elemZ);
			
			for (k = 0;k<domain_elemZ;k++)
			{
				mesh_properties.at(i).at(j).at(k).resize(3);
			
			
			}
		}
		
	}
	
	
	
	std::vector<double> parms;
		
	int ParticleID;
		
	Particle_Ideal * Ideal;
		
	int count = 0;
		
	
	
	for(i = 0;i < mesh_exact.size() ; i++)
	{
		
		for (j = 0;j < mesh_exact.at(i).size() ; j++)
		{
			for (k = 0;k < mesh_exact.at(i).at(j).size() ; k++)
			{
				count = 0;
				for(p = 0;p<mesh_exact.at(i).at(j).at(k).size();p++)
				{
					
					ParticleID = mesh_exact.at(i).at(j).at(k).at(p);
					parms = ParmVec(ParticleID,optics[1]);
					Ideal = Optics.Ideal_Array.search(parms);
					
					mesh_properties.at(i).at(j).at(k).at(0) += Ideal->Qe * M_PI*pow(ParticleParm(ParticleID,0),2)/4.0;
					mesh_properties.at(i).at(j).at(k).at(1) += Ideal->Qa /Ideal->Qe;
					mesh_properties.at(i).at(j).at(k).at(2) += ParticleParm(ParticleID,0);
					count++;
					
				}
				
				mesh_properties.at(i).at(j).at(k).at(0) /= Xelem_size * Yelem_size * Zelem_size;
				mesh_properties.at(i).at(j).at(k).at(1) /= count;
				mesh_properties.at(i).at(j).at(k).at(2) /= count;
			}
		}
	}
	
	
	printf("Complete\n");
	
	return 1;
	
}



int controller::TraceRay_Homogenized_III_offset(ray *Ray, int trial)
{
	
	
	int i;
	int keep = -1;
	int ParticleID;
	double impact;
	double scalefactor;
	
	double p_scatter;
	double p;
	
	double energy;
	
	double dist = domainX;
	double test;
	
	bool scatt = false;
	
	// smash into particles
	
	int dir1, dir2, dir3;
	
	double dir1pos, dir2pos, dir3pos;
	
	double Radius;
	
	std::vector<double> parm;

	double maxy;
	
	#if TIMER
	std::chrono::high_resolution_clock::time_point t2;
	std::chrono::nanoseconds time_span;
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	#endif
	
	if(Ray->EX() >= domain_elemX || Ray->EX() < 0 || Ray->EY() >= domain_elemY || Ray->EY() < 0 || Ray->EZ() >= domain_elemZ || Ray->EZ() < 0)
	{
		printf("narrowly avoied seg fault\n");
		Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
		Lost_in_transmission +=1;
		return -2;
	}
	else
	{
		
		
		
		std::vector<double> times(6); // txup,txdown,tyup,tydown,tzup,tzdown;
		std::vector<double>::iterator result;
		int index = 6;
		int keep;
		double keepp;
		/*std::vector<double> times_c(6);
		
		if(Ray->X() > domainX || Ray->X() < 0  || Ray->Y() > domainY || Ray->Y() < 0 || Ray->Z() > domainZ || Ray->Z() < 0)
		{
			printf("shit");
		}
		
		
		ray Test_Ray = *Ray;*/
		
		times.at(0) = (Ray->DX()>0?((Ray->EX()+1)*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(1) = (Ray->DX()<0?((Ray->EX())*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(2) = (Ray->DY()>0?((Ray->EY()+1)*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(3) = (Ray->DY()<0?((Ray->EY())*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(4) = (Ray->DZ()>0?((Ray->EZ()+1)*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		times.at(5) = (Ray->DZ()<0?((Ray->EZ())*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		keepp = Xelem_size + Yelem_size + Zelem_size;
		
		//times_c = times;
		
		for(i = 0; i<times.size() ; i++)
		{
			
			if (times.at(i) >= 0 && times.at(i) < keepp)
			{
				index = i;
				keepp = times.at(i);
			}
			
		}
		
		if(index == 6)
		{
			maxy = *(max_element(times.begin(),times.end()));
			
			for(i = 0; i<times.size() ; i++)
			{
				
				if (times.at(i) <= 0 && times.at(i) != maxy)
				{
					times.at(i) = 1+maxy;
				}
				
			}
			
			
			result = min_element(times.begin(),times.end());
			index =  std::distance(times.begin(), result);
		}
		
		
		
		if(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() > 0)
		{
			
			p = ranf(trial);
			
			if(Ray->just_scatted())
			{
				p_scatter = exp(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * (times.at(index) - mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(2)));
				//p_scatter = exp(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * times.at(index));
				//printf("%f\n",mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(2));
			}
			else
			{
				p_scatter = exp(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * times.at(index));
			}
			
			if(p>p_scatter)
			{
							
				
				
				double q = ranf(trial);
				
				if(q > mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(1))
				{
					//scatter
					
					times.at(index) = -(log(p)/mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0));
				
					scatt = true;
					
				}
				else
				{
					//absorb
					ParticleQ.at(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(floor(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() * ranf(trial)))).at(trial) += Ray->Power();
					
					Lost_at_particle += 1;
					
					Ray->S_up();
					
					Ray->obliterate();
					
					return -3;
					
				}
				
				
				
				
			}
			
	
			
			
		}
		Ray->X(Ray->X() + times.at(index) * (Ray->DX()));
		Ray->Y(Ray->Y() + times.at(index) * (Ray->DY()));
		Ray->Z(Ray->Z() + times.at(index) * (Ray->DZ()));
		Ray->just_scatted(0);
		if(PathLengths||PathCutOff>0)
		{
			Ray->aT(DISTANCE(0,0,0,times.at(index) * (Ray->DX()),times.at(index) * (Ray->DY()),times.at(index) * (Ray->DZ())));
			
		}
		
		if(scatt)
		{
			Ray->obliterate();
			Lost_at_particle += 1;
			//printf("A Ray died a horrible death\n");
			//printf("%i\n",((int) ghost_vec.size()));
			//ghost_vec.resize(ghost_vec.size()+1);
			//ghost_vec.back().ReadRay((*Ray), -1);
			
			if(Ray->X() > domainX || Ray->X() < 0  || Ray->Y() > domainY || Ray->Y() < 0 || Ray->Z() > domainZ || Ray->Z() < 0)
			{
				printf("shit");
			}
			
			return -4;
			
		}
		else
		{
			
			
			if (index == 0)
			{
				Ray->EX(Ray->EX() +1 );
			}
			else if (index == 1)
			{
				Ray->EX(Ray->EX() -1 ) ;
			}
			else if (index == 2)
			{
				Ray->EY(Ray->EY() +1) ;
			}
			else if (index == 3)
			{
				Ray->EY(Ray->EY() -1 ) ;
			}
			else if (index == 4)
			{
				Ray->EZ(Ray->EZ()+1 );
			}
			else if (index == 5)
				{
					Ray->EZ(Ray->EZ() -1 ) ;
			}
			
			/*
			times_c.at(0) = ((Ray->EX()+1)*Xelem_size - Ray->X())/(Ray->DX());
			times_c.at(1) = ((Ray->EX())*Xelem_size - Ray->X())/(Ray->DX());
			times_c.at(2) = ((Ray->EY()+1)*Yelem_size - Ray->Y())/(Ray->DY());
			times_c.at(3) = ((Ray->EY())*Yelem_size - Ray->Y())/(Ray->DY());
			times_c.at(4) = ((Ray->EZ()+1)*Zelem_size - Ray->Z())/(Ray->DZ());
			times_c.at(5) = ((Ray->EZ())*Zelem_size - Ray->Z())/(Ray->DZ());
			
			if(std::max(times_c.at(0),times_c.at(1))==0 || std::max(times_c.at(2),times_c.at(3))==0 ||std::max(times_c.at(4),times_c.at(5))==0)
			{
				printf("wtf... really??\n");
			}
			
			
			if(times_c.at(0) * times_c.at(1) > 10e-10 )
			{
				printf("wtf... really??\n");
			}
			
			if(times_c.at(2) * times_c.at(3) > 10e-10 )
			{
				printf("wtf... really??\n");
			}
			
			if(times_c.at(4) * times_c.at(5) > 10e-10 )
			{
				printf("wtf... really??\n");
			}
			
			
			if(Ray->TestElem(Xelem_size,Yelem_size,Zelem_size))
			{
				printf("really??\n");
			}*/
			
			if (Ray->EX() >= domain_elemX || Ray->EX() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 1 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EY() >= domain_elemY || Ray->EY() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EZ() >= domain_elemZ || Ray->EZ() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			
			
			return -1;
		}
	}
	
}



int controller::Init_TraceRay_Homogenized_II_free_stream()
{
	
	printf("Homogenizing... ");
	
	
	mesh_properties.clear();
	
	Optics.emission_spectrum(&optics[0]);
	
	
	int i;
	int j;
	int k,p;
	
	mesh_properties.resize(domain_elemX);
	
	
	
	for (i = 0;i<domain_elemX;i++)
	{
		mesh_properties.at(i).resize(domain_elemY);
		
		
		for (j = 0;j<domain_elemY;j++)
		{
			mesh_properties.at(i).at(j).resize(domain_elemZ);
			
			for (k = 0;k<domain_elemZ;k++)
			{
				mesh_properties.at(i).at(j).at(k).resize(3);
			
			
			}
		}
		
	}
	
	
	
	std::vector<double> parms;
		
	int ParticleID;
		
	Particle_Ideal * Ideal;
		
	int count = 0;
		
		
	for(i = 0;i < mesh_exact.size() ; i++)
	{
		
		for (j = 0;j < mesh_exact.at(i).size() ; j++)
		{
			for (k = 0;k < mesh_exact.at(i).at(j).size() ; k++)
			{
				count = 0;
				for(p = 0;p<mesh_exact.at(i).at(j).at(k).size();p++)
				{
					
					ParticleID = mesh_exact.at(i).at(j).at(k).at(p);
					parms = ParmVec(ParticleID,optics[1]);
					Ideal = Optics.Ideal_Array.search(parms);
					
					
					mesh_properties.at(i).at(j).at(k).at(0) += Ideal->Qe * M_PI*pow(ParticleParm(ParticleID,0),2)/4.0;
					mesh_properties.at(i).at(j).at(k).at(1) += Ideal->Qa /Ideal->Qe;
					mesh_properties.at(i).at(j).at(k).at(2) += Ideal->Qe *pow(ParticleParm(ParticleID,0),2)*M_PI*0.25;
					count++;
					
				}
				
				mesh_properties.at(i).at(j).at(k).at(0) /= Xelem_size * Yelem_size * Zelem_size;
				mesh_properties.at(i).at(j).at(k).at(1) /= count;
				mesh_properties.at(i).at(j).at(k).at(2) /= count; //*= TraceParam1 /(Xelem_size * Yelem_size * Zelem_size);
			}
		}
	}
	
	
	printf("Complete\n");
	return 0;
}



int controller::TraceRay_Homogenized_II_free_stream(ray *Ray, int trial)
{
	
	
	int i;
	int keep = -1;
	int ParticleID;
	double impact;
	double scalefactor;
	
	double p_scatter;
	double p;
	//double pstream;
	
	double energy;
	
	double dist = domainX;
	double test;
	
	bool scatt = false;
	
	// smash into particles
	
	int dir1, dir2, dir3;
	
	double dir1pos, dir2pos, dir3pos;
	
	double Radius;
	
	std::vector<double> parm;

	double maxy;
	
	#if TIMER
	std::chrono::high_resolution_clock::time_point t2;
	std::chrono::nanoseconds time_span;
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	#endif
	
	if(Ray->EX() >= domain_elemX || Ray->EX() < 0 || Ray->EY() >= domain_elemY || Ray->EY() < 0 || Ray->EZ() >= domain_elemZ || Ray->EZ() < 0)
	{
		printf("narrowly avoied seg fault, %d, %d, %d\n ", Ray->EX(), Ray->EY(), Ray->EZ());
		Ray->obliterate();
			//printf("A Ray was lost in 2 dir\n");
		Lost_in_transmission +=1;
		return -2;
	}
	else
	{
		
		
		
		std::vector<double> times(6); // txup,txdown,tyup,tydown,tzup,tzdown;
		std::vector<double>::iterator result;
		int index = 6;
		double keepp;
		
		
		//std::vector<double> times_c(6);
		
		times.at(0) = (Ray->DX()>0?((Ray->EX()+1)*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(1) = (Ray->DX()<0?((Ray->EX())*Xelem_size - Ray->X())/(Ray->DX()):-1);
		times.at(2) = (Ray->DY()>0?((Ray->EY()+1)*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(3) = (Ray->DY()<0?((Ray->EY())*Yelem_size - Ray->Y())/(Ray->DY()):-1);
		times.at(4) = (Ray->DZ()>0?((Ray->EZ()+1)*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		times.at(5) = (Ray->DZ()<0?((Ray->EZ())*Zelem_size - Ray->Z())/(Ray->DZ()):-1);
		keepp = Xelem_size + Yelem_size + Zelem_size;
		
		//times_c = times;
		
		for(i = 0; i<times.size() ; i++)
		{
			
			if (times.at(i) >= 0 && times.at(i) < keepp)
			{
				index = i;
				keepp = times.at(i);
			}
			
		}
		
		if(index == 6)
		{
			maxy = *(max_element(times.begin(),times.end()));
			
			for(i = 0; i<times.size() ; i++)
			{
				
				if (times.at(i) <= 0 && times.at(i) != maxy)
				{
					times.at(i) = 1+maxy;
				}
				
			}
			
			
			result = min_element(times.begin(),times.end());
			index =  std::distance(times.begin(), result);
		}
		
		if(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() > 0)
		{
			
			p = ranf(trial);
			
		
			p_scatter = exp(-mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * times.at(index)) * (1.0 + TraceParam1*mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0) * mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(2)* times.at(index));
			
						
			if(p>p_scatter)
			{
				times.at(index) = -(log(p)/mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(0));
				
				scatt = true;
				
				energy = Ray->Power() * mesh_properties.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(1);
			
			
			
				ParticleQ.at(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).at(floor(mesh_exact.at(Ray->EX()).at(Ray->EY()).at(Ray->EZ()).size() * ranf(trial)))).at(trial) += energy;
			
				Ray->Power(Ray->Power() - energy);
			
			}
			
	
			
			
		}
		Ray->X(Ray->X() + times.at(index) * (Ray->DX()));
		Ray->Y(Ray->Y() + times.at(index) * (Ray->DY()));
		Ray->Z(Ray->Z() + times.at(index) * (Ray->DZ()));
		Ray->just_scatted(0);
		if(PathLengths||PathCutOff>0)
		{
			Ray->aT(DISTANCE(0,0,0,times.at(index) * (Ray->DX()),times.at(index) * (Ray->DY()),times.at(index) * (Ray->DZ())));
			
		}
		
		if(scatt)
		{
			Ray->obliterate();
			Lost_at_particle += 1;
			//printf("A Ray died a horrible death\n");
			//printf("%i\n",((int) ghost_vec.size()));
			//ghost_vec.resize(ghost_vec.size()+1);
			//ghost_vec.back().ReadRay((*Ray), -1);
			
			if(Ray->X() > domainX || Ray->X() < 0  || Ray->Y() > domainY || Ray->Y() < 0 || Ray->Z() > domainZ || Ray->Z() < 0)
			{
				printf("shit");
			}
			
			return -4;
			
		}
		else
		{
			
			if (index == 0)
			{
				Ray->EX(Ray->EX() +1 );
			}
			else if (index == 1)
			{
				Ray->EX(Ray->EX() -1 ) ;
			}
			else if (index == 2)
			{
				Ray->EY(Ray->EY() +1) ;
			}
			else if (index == 3)
			{
				Ray->EY(Ray->EY() -1 ) ;
			}
			else if (index == 4)
			{
				Ray->EZ(Ray->EZ()+1 );
			}
			else if (index == 5)
				{
					Ray->EZ(Ray->EZ() -1 ) ;
			}
			
			
			if (Ray->EX() >= domain_elemX || Ray->EX() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 1 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EY() >= domain_elemY || Ray->EY() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			else if (Ray->EZ() >= domain_elemZ || Ray->EZ() < 0 )
			{
				Ray->obliterate();
				//printf("A Ray was lost in 2 dir\n");
				Lost_in_transmission +=1;
				return -2;
			}
			
			
			return -1;
		}
	}
	
}



