

#include <stdio.h>


#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "controller.h"
#include <stdlib.h>
#include <math.h> 
#include "pugixml.hpp"


int controller::ChangeXMLConfig(int argc, char **argv)
{
	std::string arg, partfile;
	int j;
	
	
	
	pugi::xml_document doc;
	
	pugi::xml_parse_result result = doc.load_file(argv[2]);
	
	if (!result) 
    {
		printf("Failed to Load xml file: ");
		printf("%s\n",argv[2]);
		printf("\n");
		std::cout << "Load result: " << result.description() << std::endl;
		std::cout << "Error offset: " << result.offset << " (error at [..." << (argv[2] + result.offset) << "]\n\n";
		//return -1;
	}



    pugi::xml_node bit = doc.child("KPSAAP_CONFIG");
	
	
	for (j = 4 ; j < argc -1;j++)
	{
		
		bit = bit.child(argv[j]);
		if(!bit)
		{
			printf("failed to read node %s\n", argv[j]);
			
			return 1 ;
		}
		
	}
	
	
	bit.last_child().set_value(argv[argc-1]);
	
	
	
	arg = argv[3];
	if (arg == "--same" || arg == "-s")
	{
		doc.save_file(argv[2]);
	}
	else
	{
		doc.save_file(argv[3]);
	}
	
	return 0 ;
	
	
	
}


int controller::UpdateXMLDecrypt(int argc, char **argv)
{
	
	std::string arg, partfile;
	int i,j;
	
	
	
	pugi::xml_document doc;
	
	pugi::xml_parse_result result = doc.load_file(argv[2]);
	
	if (!result) 
    {
		printf("Failed to Load xml file: ");
		printf("%s\n",argv[2]);
		printf("\n");
		std::cout << "Load result: " << result.description() << std::endl;
		std::cout << "Error offset: " << result.offset << " (error at [..." << (argv[2] + result.offset) << "]\n\n";
		//return -1;
	}
	pugi::xml_node bit;
	doc.remove_child("KPSAAP_DECRYPT");
	
	bit = doc.append_child("KPSAAP_DECRYPT");
	
	
	bit = bit.append_child("SCATTERING");
	
	for(i = 0; i < NUM_FUNCTIONS; i++)
	{
		bit = bit.append_child(ScatterStrings[i]);
		bit.append_attribute("Dimensions") = ScatterDims[i];
		bit.append_attribute("NoTP") = ScatterPos[i];
		bit = bit.parent();
		
	}
	
	bit  = bit.parent();
	
	bit = bit.append_child("EMISSION");
	
	for(i = 0; i < NUM_EMIT; i++)
	{
		bit.append_child(EmitStrings[i]);	
	}
	
	bit  = bit.parent();
	
	bit = bit.append_child("COLLECTION");
	
	for(i = 0; i < NUM_COLLECT; i++)
	{
		bit.append_child(CollectStrings[i]);	
	}
	
	bit  = bit.parent();
	
	bit = bit.append_child("BOUNDARIES");
	
	for(i = 0; i < NUM_BOUNDS; i++)
	{
		bit.append_child(BoundaryStrings[i]);	
	}
	
	bit  = bit.parent();
	
	bit = bit.append_child("TRACING_METHODS");
	
	for(i = 0; i < NUM_TRACE; i++)
	{
		bit.append_child(TraceStrings[i]);	
	}
	
	bit  = bit.parent();
	
	bit = bit.append_child("OUTPUTS");
	
	WriteOut *ptr;
	
	for(i = 0; i < NUM_WRITE_OUT; i++)
	{
		#	define X(q) \
		if (static_cast<WriteOutTypes>(i) == q) \
		{ \
			ptr = new Write ##q (this);\
		} \

		WRITE_OUT_TXT
		#   undef X
		bit = bit.append_child(WriteOutStrings[i]);
		
		ptr->SetNames();
		if (ptr->ParamNames.size()>0)
		{
			for (j = 0; j < ptr->ParamNames.size(); j++)
			{
				
				bit.append_child(ptr->ParamNames.at(j).c_str());			
				
			}
				
		}
		
		bit = bit.parent();
		
	}
	
	bit  = bit.parent();
	
	doc.save_file(argv[2]);
	
	return 0;
	
}


int controller::ReadXMLConfigFile(char *file)
{
	pugi::xml_document doc;
	
	pugi::xml_parse_result result = doc.load_file(file);
	
	int i;
	
    if (!result) 
    {
		printf("Failed to Load xml file: ");
		printf(file);
		printf("\n");
		std::cout << "Load result: " << result.description() << std::endl;
		std::cout << "Error offset: " << result.offset << " (error at [..." << (file + result.offset) << "]\n\n";
		//return -1;
	}



    pugi::xml_node config = doc.child("KPSAAP_CONFIG");
    
    domainX = config.child("Mesh_Data").child("Domain").child("X").text().as_double();
    domainY = config.child("Mesh_Data").child("Domain").child("Y").text().as_double();
    domainZ = config.child("Mesh_Data").child("Domain").child("Z").text().as_double();
    
    domain_elemX = config.child("Mesh_Data").child("Element").child("X").text().as_int();
    domain_elemY = config.child("Mesh_Data").child("Element").child("Y").text().as_int();
    domain_elemZ = config.child("Mesh_Data").child("Element").child("Z").text().as_int();
    
    offsetX = config.child("Mesh_Data").child("offsetX").child("X").text().as_double();
    offsetY = config.child("Mesh_Data").child("offsetY").child("Y").text().as_double();
    offsetZ = config.child("Mesh_Data").child("offsetZ").child("Z").text().as_double();
    
    NUM_TRIALS = config.child("Ray_Trace").child("Number_of_Trials").text().as_int();
    NUM_RAYS = config.child("Ray_Trace").child("Rays_per_Trial").text().as_int();
    iteration_limit = config.child("Ray_Trace").child("Iteration_Limit").text().as_int();
    prt_cp = config.child("Ray_Trace").child("Particle_Heat_Capacity").text().as_double();
    initial_temp = config.child("Ray_Trace").child("Iteration_Limit").text().as_double();
    TraceType = static_cast<TraceTypes>(config.child("Ray_Trace").child("Method").text().as_int());
    TraceParam1 = config.child("Ray_Trace").child("Param1").text().as_double();
    //printf("Param1 = %f", TraceParam1);
    if (config.child("Ray_Trace").child("Log_Rays").empty())
    {
		LogRays =false;
	}
	else
	{
		LogRays = config.child("Ray_Trace").child("Log_Rays").text().as_int();
	}
	
	if (config.child("Ray_Trace").child("Path_Lengths").empty())
    {
		PathLengths =false;
	}
	else
	{
		PathLengths = config.child("Ray_Trace").child("Path_Lengths").text().as_int();
	}
	if (config.child("Ray_Trace").child("Path_Cutoff").empty())
    {
		PathCutOff =0;
	}
	else
	{
		PathCutOff = config.child("Ray_Trace").child("Path_Cutoff").text().as_double();
	}
    
    EmissionType = static_cast<EmissionTypes>(config.child("Emission").child("Iteration_Limit").text().as_int());
    Optics.RayPower = config.child("Emission").child("Ray_Power").text().as_double();
    Optics.EmissionLambda = config.child("Emission").child("Ray_Wavelength").text().as_double();
    bundle_size = config.child("Emission").child("Bundle_Size").text().as_int();
    BeamRadius = config.child("Emission").child("Beam_Size").text().as_double();
    BeamMu = config.child("Emission").child("Beam_Radius").text().as_double();
    cone_angle = config.child("Emission").child("Cone_Angle").text().as_double();
	if (config.child("Emission").child("Total_Power").empty())
    {
		EnergyScaling = 1;
	}
	else
	{
		EnergyScaling = config.child("Emission").child("Total_Power").text().as_double() / (NUM_TRIALS * NUM_RAYS);
	}


	Scat = static_cast<ScatterTypes>(config.child("Scattering").child("Method").text().as_int());
	Optics.partn = config.child("Scattering").child("Particle_n").text().as_double();
	Optics.partk = config.child("Scattering").child("Particle_k").text().as_double();		





	ParticleDimPos.clear();
	ParticleDefaultVal.clear();
	ParticleBins.clear();
	ParticleNormals.clear();
	for (pugi::xml_node param = config.child("Scattering").child("Parameter"); param; param = param.next_sibling("Parameter"))
	{	
		ParticleDimPos.push_back(param.child("Position_in_CSV_file").text().as_int());
		ParticleDefaultVal.push_back(param.child("Default_Value").text().as_double());
		ParticleBins.push_back(param.child("Number_of_Bins").text().as_int());
		ParticleNormals.push_back(param.child("Normalization").text().as_bool());
	}
	
	
	
	bounds[0] = static_cast<BoundaryTypes>(config.child("Boundaries").child("UpX").child("Type").text().as_int());
	thicknesses[0] = config.child("Boundaries").child("UpX").child("Thickness").text().as_double();
	bounds[1] = static_cast<BoundaryTypes>(config.child("Boundaries").child("DownX").child("Type").text().as_int());
	thicknesses[1] = config.child("Boundaries").child("DownX").child("Thickness").text().as_double();
	bounds[2] = static_cast<BoundaryTypes>(config.child("Boundaries").child("UpY").child("Type").text().as_int());
	thicknesses[2] = config.child("Boundaries").child("UpY").child("Thickness").text().as_double();
	bounds[3] = static_cast<BoundaryTypes>(config.child("Boundaries").child("DownY").child("Type").text().as_int());
	thicknesses[3] = config.child("Boundaries").child("DownY").child("Thickness").text().as_double();
	bounds[4] = static_cast<BoundaryTypes>(config.child("Boundaries").child("UpZ").child("Type").text().as_int());
	thicknesses[4] = config.child("Boundaries").child("UpZ").child("Thickness").text().as_double();
	bounds[5] = static_cast<BoundaryTypes>(config.child("Boundaries").child("DownZ").child("Type").text().as_int());
	thicknesses[5] = config.child("Boundaries").child("DownZ").child("Thickness").text().as_double();
	
	int test;
	TraceOuts.clear();
	
	//printf("here3");
	for (pugi::xml_node collect = config.child("Collection"); collect; collect = collect.next_sibling("Collection"))
	{	
		
		CollectionType = static_cast<CollectionTypes>(collect.child("Method").text().as_int());
		//printf("here2");
		test = TraceOuts.size();
		#	define X(q) \
		if (CollectionType == q) \
		{ \
			TraceOuts.push_back(new TraceOut_##q ()) ;\
		} \

		COLLECT_TXT
		#   undef X
		
		if(test == TraceOuts.size())
		{
			continue;
		}
		//printf("here");
		TraceOuts.back()->mapxsamples = collect.child("X_Samples").text().as_int();
		TraceOuts.back()->mapysamples = collect.child("Y_Samples").text().as_int();
		TraceOuts.back()->mapscattsamples = collect.child("Scatter_Samples").text().as_int();
		TraceOuts.back()->WindowSize = collect.child("Window_Size").text().as_int();
		TraceOuts.back()->WindowOffset = collect.child("Window_Offset").text().as_int();
		TraceOuts.back()->CollectionRadius = collect.child("Collection_Radius").text().as_int();
		TraceOuts.back()->initialized = true;
	}
	
	WriteOuts.clear();
	
	
	for (pugi::xml_node write = config.child("Output"); write; write = write.next_sibling("Output"))
	{	
		
		WriteOutType = static_cast<WriteOutTypes>(write.text().as_int());
		//printf("here2");
		test = WriteOuts.size();
		#	define X(q) \
		if (WriteOutType == q ) \
		{ \
			WriteOuts.push_back(new  Write ##q (this)) ;\
		} \

		WRITE_OUT_TXT
		#   undef X
		
		if(test == WriteOuts.size())
		{
			continue;
		}
		WriteOuts.back()->SetNames();
		//printf("here");
		for(i = 0; i < WriteOuts.back()->ParamNames.size(); i++)
		{
			WriteOuts.back()->Params.push_back(write.child(WriteOuts.back()->ParamNames.at(i).c_str()).text().as_double());
		}
	}
	
	
	if(ParticleDimPos.size()<ScatterDims[Scat])
	{
		printf("You Suck! the particles have less dimensions than the scattering requires (%d < %d) \n", ParticleDimPos.size(), ScatterDims[Scat]);
	   
	   return 1;
	}
	
	if(ParticleDimPos.size()>ScatterDims[Scat])
	{
		printf("There are too many dimensions, resizing\n");
		
		ParticleDimPos.resize(ScatterDims[Scat]);
		ParticleDefaultVal.resize(ScatterDims[Scat]);
		ParticleBins.resize(ScatterDims[Scat]);
		ParticleNormals.resize(ScatterDims[Scat]);
	   
	   
	}
	
	
			
	#	define X(q) \
	if(TraceType == q) \
	{ \
		TraceRay = &controller::TraceRay_##q ;\
		Init_TraceRay = &controller::Init_TraceRay_##q ;\
	} \
	
	TRACE_TXT
	#   undef X
	
	
	#	define X(q) \
	if(EmissionType == q) \
	{ \
		TraceInFunc = &controller::TraceRayIn_##q ;\
	} \

	EMIT_TXT
	#   undef X
	
	# define X(q) \
	if(Scat == q) \
	{ \
		ScatFunc = &controller::Scatter_##q ;\
		Optics.ScatInitFunc = &OpticData::Initialize_##q ;\
	} \

	SCATTER_TXT
	#   undef X
	
	return 0;
}






int controller::SetDefaults()
{
	
	NUM_RAYS = 50000000;
	
	domainX = .005; //size in m
	domain_elemX = 60; // numer of elements in X dir
	domainY = .005;
	domain_elemY = 60;
	domainZ = .04;
	domain_elemZ = 35;
	
	glass_thickness = .002;
	
	cone_angle = 9 * 2* 3.14159265 / 360;
	BeamRadius = 0.00159;
	
	mapxsamples = 400;
	mapysamples =400;
	
	ParticleRadius = .0000065;
	
	ParticleRadius2 = 00;
	
	BeamMu = .00065;
	
	MinPowerThreshold = 1;
	
	EmissionType = Gaussian_E; 
	CollectionType = Offset_Panel_C;
	ScatFunc = &controller::Scatter_One_Ray_Mie;
	TraceRay = &controller::TraceRay_Dull;
	iteration_limit = 100;
	initial_temp = 300; //K
	prt_cp = 3919520;// J/K/m^3 for nickel
	MonitorPhase = false;
	
	bundle_size = 10;
	
	offsetX = -0.04;
	offsetY = -0.02;
	offsetZ = 0.0;
	Optics.EmissionLambda = .000000972;

	DividedImpactMap = false;
	
	
	Optics.partk = 1.9;
	Optics.partn = 4.0;

	
	Optics.RayPower = 100;
	
	return 0;
}



int controller::ReadConfigFile(char *file)
{
	
    char buffer [100];
    
	NUM_TRIALS = 1;
	mapscattsamples = 5;

    FILE * FPT = fopen ("KPSAAP_CONFIG.dat" , "r");
    if (FPT == NULL) 
    {
	   printf("Error opening config file, I'm done with stupid defaults, get a real file \n");
	   
	   return 1;
	   
	   SetDefaults();  

    }
    else
    {
		std::ifstream in(file);
		
		std::string field;
		std::string line;
		std::string bits;

		std::stringstream sep, sep2;
		
		if(in) 
		{


	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			NUM_RAYS = atoi(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			domainX = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			domainY = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			domainZ = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			domain_elemX = atoi(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			domain_elemY = atoi(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			domain_elemZ = atoi(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			offsetX = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			offsetY = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			offsetZ = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			glass_thickness = atof(field.c_str());
	
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			mapxsamples = atoi(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			mapysamples = atoi(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			prt_cp = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			initial_temp = atof(field.c_str());
	
	      
			
	
			getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			EmissionType = static_cast<EmissionTypes>(atoi(field.c_str()));

			#	define X(q) \
			if(EmissionType == q) \
			{ \
				TraceInFunc = &controller::TraceRayIn_##q ;\
			} \

			EMIT_TXT
			#   undef X
	
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			bundle_size = atoi(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			BeamRadius = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			BeamMu = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			Optics.EmissionLambda = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			cone_angle = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			Optics.RayPower = atof(field.c_str());
	
	
	       
	        getline(in, line);
	        sep.str(line);
	        getline(sep, bits, '=');
	        sep2.str(bits);
	        while (getline(sep2, field, ','))
	        {
				CollectionType = static_cast<CollectionTypes>(atoi(field.c_str()));
	            #	define X(q) \
				if (CollectionType == q) \
				{ \
					TraceOuts.push_back(new TraceOut_##q ()) ;\
				} \
	
				COLLECT_TXT
				#   undef X
	        }
	        sep2.clear();
			

			
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
                WindowSize = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
                WindowOffset = atof(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
                DividedImpactMap = atoi(field.c_str());
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			TraceType = static_cast<TraceTypes>(atoi(field.c_str()));

			printf("tracetype: %d of %d\n",TraceType, NUM_TRACE-1);

			#	define X(q) \
			if(TraceType == q) \
			{ \
				TraceRay = &controller::TraceRay_##q ;\
				Init_TraceRay = &controller::Init_TraceRay_##q ;\
			} \
			
			TRACE_TXT
			#   undef X
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			Scat = static_cast<ScatterTypes>(atoi(field.c_str()));

			# define X(q) \
			if(Scat == q) \
			{ \
				ScatFunc = &controller::Scatter_##q ;\
				Optics.ScatInitFunc = &OpticData::Initialize_##q ;\
			} \

			SCATTER_TXT
			#   undef X
	
	
	        int i= 0;
	        getline(in, line);
	        sep.str(line);
	        getline(sep, bits, '=');
	        sep2.str(bits);
	        while (getline(sep2, field, ','))
	        {
	
	            ParticleDimPos.push_back(atoi(field.c_str()));
	        }
	        sep2.clear();
	
	        i=0;
	        getline(in, line);
	        sep2.str(line);
	        getline(sep2, bits, '=');
	        sep.str(bits);
	        while (getline(sep, field, ','))
	        {
	            ParticleDefaultVal.push_back(atof(field.c_str()));
	        }
	        sep2.clear();
			sep.clear();

	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, bits, '=');
	        sep2.str(bits);
	        while (getline(sep2, field, ','))
	        {
	            ParticleBins.push_back(atoi(field.c_str()));
	        }
	        sep2.clear();
	        sep.clear();
	        
	        
	        getline(in, line);
	        sep.str(line);
	        getline(sep, bits, '=');
	        sep2.str(bits);
	        while (getline(sep2, field, ','))
	        {
	            ParticleNormals.push_back(atoi(field.c_str()));
	        }
	        sep2.clear();
	        sep.clear();
	
	
	
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			iteration_limit = atof(field.c_str());
	
	
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			Optics.partn = atof(field.c_str());

	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			Optics.partk = atof(field.c_str());

	
	
	
	
	        getline(in, line);
	        sep.str(line);
	        getline(sep, field, '=');
			MinPowerThreshold = atof(field.c_str());
			
			
			
		}
	
	}
	
	int i;
	for(i = 0; i< 6;i++)
	{
		thicknesses[i] = glass_thickness;
		bounds[i] = Glass;
	}
	
	
	if(ParticleDimPos.size()<ScatterDims[Scat])
	{
		printf("You Suck! the particles have less dimensions than the scattering requires \n");
	   
	   return 1;
	}
	
	if(ParticleDimPos.size()>ScatterDims[Scat])
	{
		printf("There are too many dimensions, resizing\n");
		
		ParticleDimPos.resize(ScatterDims[Scat]);
		ParticleDefaultVal.resize(ScatterDims[Scat]);
		ParticleBins.resize(ScatterDims[Scat]);
		ParticleNormals.resize(ScatterDims[Scat]);
	   
	   return 0;
	}
	
	
	return 0;
}

double DISTANCE(double a1,double a2,double a3,double b1,double b2,double b3)
{
	return sqrt((a1-b1)*(a1-b1) + (a2-b2)*(a2-b2)+ (a3-b3)*(a3-b3));
}




controller::controller()
{
	ParticleX.clear();
}


controller::~controller()
{

}


void controller::print()
{
	printf("This is a test \n");
}

int controller::ExportConfigData()
{
	FILE* ConfigData = fopen ("ConfigData.dat" , "w");
	
	int i;
	
	
	fprintf(ConfigData,"#SCATTERING_FUNCTIONS\n");
	for (i = 0; i<NUM_FUNCTIONS; i++)
	{
		fprintf(ConfigData,"%s\n",ScatterStrings[i]);
		
	}
	
	fprintf(ConfigData,"#SCATTERING_DIMENSIONS\n");
	for (i = 0; i<NUM_FUNCTIONS; i++)
	{
		fprintf(ConfigData,"%i\n",ScatterDims[i]);
		
	}
	
	fprintf(ConfigData,"#EMIT_FUNCTIONS\n");
	for (i = 0; i<NUM_EMIT; i++)
	{
		fprintf(ConfigData,"%s\n",EmitStrings[i]);
		
	}
	
	fprintf(ConfigData,"#COLLECTION_FUNCTIONS\n");
	for (i = 0; i<NUM_COLLECT; i++)
	{
		fprintf(ConfigData,"%s\n",CollectStrings[i]);
		
	}
	
	fprintf(ConfigData,"#COLLECTION_FUNCTIONS\n");
	for (i = 0; i<NUM_TRACE; i++)
	{
		fprintf(ConfigData,"%s\n",TraceStrings[i]);
		
	}
	
	fclose(ConfigData);
	
	return 0;
}


int controller::Generate_Ideals()
{
	int i,j;
	
	
	std::vector<double> Mins;
	std::vector<double> Maxes;
	std::vector<int> Bins;
	Mins.resize(ParticleIndecies.size());
	Maxes.resize(ParticleIndecies.size());
	Bins.resize(ParticleIndecies.size());
	
	double min, max;
	double thing;
	

	
	
	for (i = 0;i < ParticleIndecies.size(); i++)
	{
		
		if (ParticleNormals.at(i))
		{
			if (ParticleIndecies.at(i) == 0)
			{
				Mins.at(i) = M_PI*ParticleDefaultVal.at(i)/Optics.MaxLambda();
				Maxes.at(i) = M_PI*ParticleDefaultVal.at(i)/Optics.MaxLambda();
				Bins.at(i) = 1;
			}
			else 
			{
				
				min = ParticleParm(0,i)/Optics.MaxLambda();
				max = ParticleParm(0,i)/Optics.MinLambda();
				for(j = 0; j<ParticleParms.size();j++)
				{
					thing = ParticleParm(j,i);
					if (thing/Optics.MaxLambda() < min)
					{
						min = thing/Optics.MaxLambda();
					}
					if (thing/Optics.MinLambda() > max)
					{
						max = thing/Optics.MaxLambda();
					}
					
					
				}
				
				Mins.at(i) = M_PI*min;
				Maxes.at(i) = M_PI*max;
				if(min != max)
				{
					Bins.at(i) = ParticleBins.at(i);
				}
				else
				{
					Bins.at(i) =1;
				}
				
			}
		}
		else
		{
		
		
		
			if (ParticleIndecies.at(i) == 0)
			{
				Mins.at(i) = ParticleDefaultVal.at(i);
				Maxes.at(i) = ParticleDefaultVal.at(i);
				Bins.at(i) = 1;
				continue;
			}
			else
			{
				
				min = ParticleParm(0,i);
				max = ParticleParm(0,i);
				for(j = 0; j<ParticleParms.size();j++)
				{
					thing = ParticleParm(j,i);
					if (thing < min)
					{
						min = thing;
					}
					if (thing > max)
					{
						max = thing;
					}
					
					
				}
				
				Mins.at(i) = min;
				Maxes.at(i) = max;
				if(min != max)
				{
					Bins.at(i) = ParticleBins.at(i);
				}
				else
				{
					Bins.at(i) = 1;
				}
				
			}
		}
	}
	
	Optics.Ideal_Array.Generate(Bins,Mins,Maxes);
	return 0;
}

double controller::ParticleParm(int i, int j) // j = 0 is always the diameter!!!!!!!!
{
	if(0 == ParticleIndecies.at(j))
	{
		return ParticleDefaultVal.at(j);
	}
	else
	{
		return ParticleParms.at(i).at(ParticleIndecies.at(j) -1 );
	}
}

std::vector<double> controller::ParmVec(int i, double lambda)
{
	std::vector<double> parm;
	int j;
	for  (j = 0; j< ParticleBins.size();j++)
	{
		if (ParticleNormals.at(j))
		{
			parm.push_back(ParticleParm(i,j)* M_PI/lambda);
		}
		else
		{
			parm.push_back(ParticleParm(i,j));
		}
	}
	

	
	return parm;
}

int controller::ReadParticleData(char *file)
{
	ParticleX.clear();
    ParticleY.clear();
    ParticleZ.clear();
	int i = 0;
	int count = 0;  //how many lines are in the file
	FILE * FPT;
    char buffer [100];
    bool flag;
    int test;

    FPT = fopen (file , "r");
    if (FPT == NULL) 
    {
	   printf("Error opening file /n");
	   
	   return 1;
    }
    while (true)
    {
	   if ( fgets (buffer , 100 , FPT) == NULL ) 
       {
		   break;
	   }
       else
       {
		   count +=1;
	   }
     }
   fclose (FPT);
    
   
   ParticleX.reserve(count);
   ParticleY.reserve(count);
   ParticleZ.reserve(count);
   

	ParticleParms.reserve(count);

	
	
	std::vector<double> ParmVec;
   
   ParticleIndecies.resize(ParticleDimPos.size());
   
   for (count = 0;count<ParticleDimPos.size();count++)
   {
	   if (ParticleDimPos.at(count) != 0)
	   {
		   ParmVec.push_back(0.0);
		   
		   ParticleIndecies.at(count) = i+1;
		   i++;
	   }
	   
   }
   
   std::ifstream in(file);
   if(in) 
   {
       std::string line;
       getline(in, line);
       while (getline(in, line)) 
       {
			flag  = false;
            std::stringstream sep(line);
            std::string field;
            count = 0;
            test = 0;
            while (getline(sep, field, ',') && !flag) 
            {
				if (count == 1 && ReadInitalTemperatures)
				{
					ParticleQ.push_back(std::vector<double>(1,atof(field.c_str())));
				}
				if (count == 4)
				{
					ParticleX.push_back(offsetX + atof(field.c_str()));
					if (ParticleX.back() < 0 || ParticleX.back() > domainX )
					{
						ParticleX.pop_back();
						flag = true;
					}
				}
				if (count == 5)
				{
					ParticleY.push_back(offsetY + atof(field.c_str()));
					if(ParticleY.back() < 0 || ParticleY.back() > domainY)
					{
						ParticleX.pop_back();
						ParticleY.pop_back();
						flag = true;
					}
				}
				if (count == 6)
				{
					ParticleZ.push_back(offsetZ + atof(field.c_str()));
					if(ParticleZ.back() < 0 || ParticleZ.back() > domainZ)
					{
						ParticleX.pop_back();
						ParticleY.pop_back();
						ParticleZ.pop_back();
						flag = true;
					}
				}
				
				
				if(count > 6)
				{
					for (i = 0;i<ParticleDimPos.size();i++)
					{
						if (ParticleDimPos.at(i) ==0)
						{
							continue;
						}
						if ((count - 7) ==  i)
						{
							ParmVec.at(ParticleIndecies.at(count - 7)-1) = atof(field.c_str());
							test++;
						}
					}
				}
				
				count += 1;
				
				
            }
            
            
            if (test != ParmVec.size() && !flag)
            {
				printf("condsider freaking the hell out");
			}
            
            if(!flag)
            {
				ParticleParms.push_back(ParmVec);
			}
            
            
			if (ParticleX.back() < 0 || ParticleX.back() > domainX || ParticleY.back() < 0 || ParticleY.back() > domainY ||  ParticleZ.back() < 0 || ParticleZ.back() > domainZ)				
			{			
				if (ReadInitalTemperatures	)
				{
					ParticleQ.pop_back();
				}
				ParticleX.pop_back();
				ParticleY.pop_back();
				ParticleZ.pop_back();
				ParticleParms.pop_back();


			}
        }
    }
    
    ParticleQ.resize(ParticleX.size());
   

   

   
   if (ParticleX.size() != ParticleY.size() || ParticleY.size() != ParticleZ.size() || ParticleY.size() != ParticleParms.size())
   {
	   printf("ERROR !!!!! PARTICLE VECTORS DO NOT MATCH \n");
	   return 1;
	   
   }
   
   
   return 0;
}

int controller::InitiateDomain(double X, int nX, double Y, int nY, double Z, int nZ, double G)
{
	domainX = X;
	domain_elemX = nX;
	domainY = Y;
	domain_elemY = nY;
	domainZ = Z;
	domain_elemZ = nZ;
	
	mesh_exact.clear();
	mesh_overlapped.clear();
	
	glass_thickness = G;
	
	Xelem_size = X/((double) nX);
	Yelem_size = Y/((double) nY);
	Zelem_size = Z/((double) nZ);
	
	
	domain_ext[0] = domainX + thicknesses[0];
	domain_ext[1] = - thicknesses[1];
	domain_ext[2] = domainY + thicknesses[2];
	domain_ext[3] = - thicknesses[3];
	domain_ext[4] = domainZ + thicknesses[4];
	domain_ext[5] = - thicknesses[5];
	
	int i;
	int j;
	
	mesh_exact.resize(nX);
	
	mesh_overlapped.resize(nX);
	
	for (i = 0;i<nX;i++)
	{
		mesh_exact.at(i).resize(nY);
		mesh_overlapped.at(i).resize(nY);
		
		for (j = 0;j<nY;j++)
		{
			mesh_exact.at(i).at(j).resize(nZ);
			mesh_overlapped.at(i).at(j).resize(nZ);
			
		}
		
	}
	return 0;
	//printf("%i\n",((int) ghost_vec.size()));
}

int controller::InitiateDomain()
{
	Xelem_size = domainX/((double) domain_elemX);
	Yelem_size = domainY/((double) domain_elemY);
	Zelem_size = domainZ/((double) domain_elemZ);
	
	mesh_exact.clear();
	mesh_overlapped.clear();
	
	int i;
	int j;
	
	domain_ext[0] = domainX + thicknesses[0];
	domain_ext[1] = - thicknesses[1];
	domain_ext[2] = domainY + thicknesses[2];
	domain_ext[3] = - thicknesses[3];
	domain_ext[4] = domainZ + thicknesses[4];
	domain_ext[5] = - thicknesses[5];
	
	
	mesh_exact.resize(domain_elemX);
	
	mesh_overlapped.resize(domain_elemX);
	
	for (i = 0;i<domain_elemX;i++)
	{
		mesh_exact.at(i).resize(domain_elemY);
		mesh_overlapped.at(i).resize(domain_elemY);
		
		for (j = 0;j<domain_elemY;j++)
		{
			mesh_exact.at(i).at(j).resize(domain_elemZ);
			mesh_overlapped.at(i).at(j).resize(domain_elemZ);
			
		}
		
	}
	return 0;
	//printf("%i\n",((int) ghost_vec.size()));
}




int controller::Sort_Particles()
{
	
	int i;
	int elemX;
	int elemY;
	int elemZ;
	
	
	
	int count = 0;
	int countII = 0;
	
	for (i = 0;i<ParticleX.size();i++)
	{
		if(ParticleX[i]<0||ParticleX[i]>=domainX)
		{
			countII +=1;
			continue;
		}
		
		if(ParticleY[i]<0||ParticleY[i]>=domainY)
		{
			countII +=1;
			continue;
		}
		
		if(ParticleZ[i]<0||ParticleZ[i]>=domainZ)
		{
			countII +=1;
			continue;
		}
		
		elemX = floor(ParticleX[i]/Xelem_size);
		elemY = floor(ParticleY[i]/Yelem_size);
		elemZ = floor(ParticleZ[i]/Zelem_size);
		
		if(elemX >= 0 && elemX < domain_elemX    &&    elemY >= 0 && elemY < domain_elemY    &&    elemZ >= 0 && elemZ < domain_elemZ)
		{
			count += 1;
			mesh_exact[elemX][elemY][elemZ].push_back(i);
			mesh_overlapped[elemX][elemY][elemZ].push_back(i);
		}
		else
		{
			printf("God is Dead\n");
			printf("%d\n",elemX);
			printf("%d\n",elemY);
			printf("%d\n",elemZ);
			continue;			
		}
		
		ParticleRadius = ParticleParm(i,0)*.75;
		if (fabs(elemX*Xelem_size-ParticleX[i])<ParticleRadius  &&  elemX > 0)
		{
			mesh_overlapped[elemX-1][elemY][elemZ].push_back(i);
		}
		if (fabs((elemX+1)*Xelem_size-ParticleX[i])<ParticleRadius  &&  elemX < (domain_elemX-1))
		{
			mesh_overlapped[elemX+1][elemY][elemZ].push_back(i);
		}
		
		
		if (fabs(elemY*Yelem_size-ParticleY[i])<ParticleRadius  &&  elemY > 0)
		{
			mesh_overlapped[elemX][elemY-1][elemZ].push_back(i);
		}
		if (fabs((elemY+1)*Yelem_size-ParticleY[i])<ParticleRadius  &&  elemY < (domain_elemY-1))
		{
			mesh_overlapped[elemX][elemY+1][elemZ].push_back(i);
		}
		
		
		if (fabs(elemZ*Zelem_size-ParticleZ[i])<ParticleRadius  &&  elemZ > 0)
		{
			mesh_overlapped[elemX][elemY][elemZ-1].push_back(i);
		}
		if (fabs((elemX+1)*Xelem_size-ParticleX[i])<ParticleRadius  &&  elemZ < (domain_elemZ-1))
		{
			mesh_overlapped[elemX][elemY][elemZ+1].push_back(i);
		}
		 
		
	}
	
	printf("Of %i Particles, %i went into the exact mesh - %i were outside the domain \n",((int) ParticleX.size()),count, countII);
	return 0;
}







double controller::elem_size(int i)
{
	i = (i-1)%3+1;
	if (i ==1)
	{
		return Xelem_size;
	}
	if (i ==2)
	{
		return Yelem_size;
	}
	if (i ==3)
	{
		return Zelem_size;
	}	
	
	else  {return 0;}
	
}


int controller::domain_elem(int i)
{
	i = (i-1)%3+1;
	if (i ==1)
	{
		return domain_elemX;
	}
	if (i ==2)
	{
		return domain_elemY;
	}
	if (i ==3)
	{
		return domain_elemZ;
	}	
	else  {return 0;}
	
}


double controller::domain(int i)
{
	
	i = (i-1)%3+1;
	if (i ==1)
	{
		return domainX;
	}
	if (i ==2)
	{
		return domainY;
	}
	if (i ==3)
	{
		return domainZ;
	}	
	else  {return 0;}
	
}

double controller::domain_int(int i)
{
	if (i ==0)
	{
		return domainX;
	}
	if (i ==1)
	{
		return 0;
	}
	if (i ==2)
	{
		return domainY;
	}	
	if (i ==3)
	{
		return 0;
	}
	if (i ==4)
	{
		return domainZ;
	}
	if (i ==5)
	{
		return 0;
	}
	else  {return 0;}
	
}
