/********************************************************************
 *                        MONACO Version 2.5                        *
 *                                                                  *
 * Copyright (c) 1993-2001 University of Michigan                   * 
 *                                                                  *
 * misc.h - Utility routines and definitions for general use        *
 *                                                                  *
 ********************************************************************/

#ifndef _RANF_
#define _RANF_

#include <math.h>






/* Random number generator */


extern unsigned int *seed_array;
float ranf(void);
float ranf(int trial);



unsigned int ranget(void);
void ranset(unsigned int set);


#endif /* _RANF_ */

