/* Don't forget to remove the ranf() definition        */
/* in libmonaco.inc on CRAY's                          */

#ifndef CRAY


#include "ranf.h"

/* Fast single precision random number generator       */
/* It's a Lehmer linear congruential generator (LCG)   */
/*      f(s) = (a*s + c) mod m                         */
/* Internal IEEE-number representation is required     */
/* Optimized for use on (32-bit) workstations          */
/* Quadruple as fast as the fortran LCG evaluation     */
/* Faster than most system supplied LCG's              */
/* a,c,m are choosen to obtain high unformility        */
/* See Communications of the ACM, June 88, Vol.31 No.6 */
/* Random numbers are in the range (0.0,1.0)           */
/* Stefan Dietrich, Cornell University                 */
/* Refer to book "The art of computer programming" by 
 * D.E. Knuth Vol 2, Chapter 3 - Random Numbers, year 1997*/

static unsigned int seed = 0x11111111;

unsigned int *seed_array;

float ranf(void)
{
    float rf;
    unsigned int *irf = (unsigned int * ) &rf;
    seed *= 0x2C4856AD;
    *irf = 0x3F800000 | (seed >> 9);
    
    /*printf("Seed = %d\n", seed);*/
    /*getchar();*/
    /*printf("rf-1 = %f\n", rf-1.0);
    getchar();*/
    return (rf-1.0);
}


float ranf(int trial)
{
    float rf;
    unsigned int *irf = (unsigned int * ) &rf;
    seed_array[trial] *= 0x2C4856AD;
    *irf = 0x3F800000 | (seed_array[trial] >> 9);
    
    /*printf("Seed = %d\n", seed);*/
    /*getchar();*/
    /*printf("rf-1 = %f\n", rf-1.0);
    getchar();*/
    return (rf-1.0);
}


unsigned int ranget(void)
{
#ifndef CRAY
  return(seed);
#else
  return 0;
#endif
}

void ranset(unsigned int set)
{
#ifndef CRAY
  seed = set;
#endif
}

float ranf_(void)
{
  float rf;
  unsigned int *irf = (unsigned int * ) &rf;
  seed *= 0x2C4856AD;
  *irf = 0x3F800000 | (seed >> 9);
  return (rf-1.0);
}

unsigned int ranget_(void) { return(seed); }
void ranset_(unsigned int set)  { seed = set;   }

/* ... and the same for random numbers between 1 and N */

static unsigned int nseed = 0x11111111;

int nranf_(int *N)
{
    float rf,ranfval;
    int I;
    unsigned int *irf = (unsigned int * ) &rf;
    nseed *= 0x2C4856AD;
    *irf = 0x3F800000 | (nseed >> 9);
    ranfval = (rf-1.0);
    I = (int) (1.0 + ranfval * (*N));     /* I could be N+1 */
    return ( (I > (*N)) ? (*N) : I );     /* if so return N */
}

unsigned int nranget_(void) { return(nseed); }
void nranset_(unsigned int set)  { nseed = set;   }

#endif

