


#include "controller.h"
#include "optics.h"
#include "ranf.h"


#include <cmath>

#include "bessel.h"
//#include <boost/math/special_functions.hpp>



Particle_Ideal * multi_vector::at(std::vector<int> ind)
{
	if(dims.size() != ind.size())
	{
		printf("The world may as well end\n");
		return 0;
	}
	
	else
	
	{
		int i;
		int tot = 0;
		int thing = 1;
		for (i = dims.size()-1;i>=0;i--)
		{
			tot +=  thing * ind.at(i) ;
			thing *= dims.at(i);
			
		}
		
		if (tot >= vec.size())
		{
			printf("goddamnit");
		}
		
		return &vec.at(tot);
	}
	
}


Particle_Ideal * multi_vector::search(std::vector<double> parm)
{	
	
	
	int i;
	std::vector<int> ind;
	std::vector<int> ind_out;
	ind.resize(dims.size());
	ind_out.resize(dims.size());
	for( i = 0 ; i < dims.size();i++)
	{
		

		if (parm.at(i) <= at(ind)->Parms.at(i))
		{
			ind_out.at(i) = 0;
			ind.at(i) = 0;
			continue;
			
		}
		
		ind.at(i) = dims.at(i)-1;
		if (parm.at(i) > at(ind)->Parms.at(i))
		{
			ind_out.at(i) = ind.at(i);
			continue;
		}
		
		ind_out.at(i) =  (dims.at(i)-1) *floor((parm.at(i) - at(ind_out)->Parms.at(i))/(at(ind)->Parms.at(i) - at(ind_out)->Parms.at(i)));
		ind.at(i) = ind_out.at(i);
		
		
	}
	
	return at(ind_out);


	
}
		

void multi_vector::Generate(std::vector<int> bins, std::vector<double> mins, std::vector<double> maxes)
{
	int i,j;
	int count = 1;
	dims = bins;
	
	
	
	for (i = 0; i < bins.size();i++)
	{
		count *= bins.at(i);
		
	}
	
	vec.resize(count);
	
	Particle_Ideal dumdum(bins.size());
	
	std::vector<std::vector<double>> vals;
	
	vals.resize(bins.size());
	
	for(i = 0 ; i < bins.size();i++)
	{
		vals.at(i).resize(bins.at(i));
		
		for(j = 0;j<bins.at(i);j++)
		{
			if(bins.at(i) != 1)
			{
				vals.at(i).at(j) = mins.at(i) + (maxes.at(i) - mins.at(i)) * ((j)/((double)bins.at(i)-1));
			}
			else
			{
				vals.at(i).at(j) = (mins.at(i) + maxes.at(i))/2;
			}
			
		}

		
	}
	
	count = 0;
	std::vector<int> indecies;
	indecies.resize(bins.size());
	while (true)
	{
		for(i = 0;i<bins.size();i++)
		{
			dumdum.Parms.at(i) = vals.at(i).at(indecies.at(i));
			
		}
		
		
		*at(indecies) = dumdum;
		
		indecies.back() += 1;
		
		for (j = indecies.size()-1;j > 0 ; j--)
		{
			
			if (indecies.at(j) >= bins.at(j))
			{
				indecies.at(j) = 0;
				
				indecies.at(j-1) += 1;
			}
			
		}
		
		j = 0;
		
		if (indecies.at(j) >= bins.at(j))
		{
			break;
		}
		
	}
	
	
	
	
}




double Borosilicate_Absorbtion( double lamda)
{
	return 41.43*.5; //per m, to get 92 percent transmitance normal though the glass


}


double Borosilicate_Reflectance( double lamda)
{
	return .92; 


}


void OpticData::emission_spectrum(double * props)
{
	props[0] = RayPower;
	props[1] = EmissionLambda;
	
	
}






int OpticData::Initialize_One_Ray_Mie()
{
	printf("Beginning Mie Calculation\n");
	IndexAir = std::complex<double>(1,0);
		

	int phasesamples = 100;
	

	FILE* FPT = fopen ("MieData.plt" , "w");
	
	FILE* FPT2 = fopen ("QData.plt" , "w");
	
	if(FPT == NULL)
	{
		printf("well this sucks");
		
	}
	
	
	
	dcomp m;
	dcomp msqr;
	double chi;
	int i,j, nmax;
	std::vector<dcomp> a,b;
	double theta, mu;
	int n;
	double pi[3], tau[3];
	double __pi = 3.1415926535;
	dcomp psichi,psimchi,psichiprime,psimchiprime;
	dcomp xichi,xichiprime;
	//double delta = .000000000001;
	dcomp S1;
	dcomp S2;
	dcomp extra;
	double hold;
	double absS1, absS2;
	
	double Qs,Qe,Qa;
		

	
	double order;
	
	std::vector<dcomp> j_mchi;
	std::vector<dcomp> j_mchiprime;
	std::vector<dcomp> y_mchiprime;
	std::vector<dcomp> y_mchi;
	std::vector<double> j_chi;
	std::vector<double> j_chiprime;
	std::vector<double> y_chiprime;
	std::vector<double> y_chi;
	
	
	std::vector<double> PhaseFunction;
	
	nmax = 300;
	
	j_mchi.resize(nmax+2);
	j_mchiprime.resize(nmax+2);
	y_mchiprime.resize(nmax+2);
	y_mchi.resize(nmax+2);
	j_chi.resize(nmax+2);
	j_chiprime.resize(nmax+2);
	y_chiprime.resize(nmax+2);
	y_chi.resize(nmax+2);
	
	
	fprintf(FPT,"TITLE = \"Mie Data\"\n");
	fprintf(FPT,"VARIABLES = \"Theta\",\"F\"\n");

	fprintf(FPT2,"TITLE = \"Q Data\"\n");
	fprintf(FPT2,"VARIABLES = \"Chi\",");
	for(i = 1; i < Ideal_Array.dim()  ; i++)
	{
		fprintf(FPT2," \"Parm%d\",", i);
	}
	fprintf(FPT2,"\"Qe\",\"Qa\",\"Qs\"\n");
	fprintf(FPT2,"ZONE T=\"Data\", DATAPACKING=Point\n");
	
	
	for (i = 0;i<Ideal_Array.size();i++)
	{	
		
		
		a.clear();
		b.clear();
		
		Qe = 0.0;
		Qs = 0.0;
		IndexParticle.push_back(std::complex<double>(partn,partk));
		chi = Ideal_Array.at(i)->Parms.at(0)/2;
		m = IndexParticle.back()/IndexAir;
		msqr = m*m;	
	
		fprintf(FPT,"ZONE T=\"");
		fprintf(FPT,"Chi");
		
		fprintf(FPT," = %f,", Ideal_Array.at(i)->Parms.at(0));
		for(j = 1; j < Ideal_Array.dim()  ; j++)
		{
			fprintf(FPT," Parm%d = %f,", j, Ideal_Array.at(i)->Parms.at(j));
		}
		
		fprintf(FPT,"\", DATAPACKING=POINT\n");
		
		
		cbessjyva(nmax+1.5,m*chi,order,&j_mchi,&y_mchi,&j_mchiprime,&y_mchiprime);
		bessjyv(nmax+1.5,chi,order,&j_chi[0],&y_chi[0],&j_chiprime[0],&y_chiprime[0]);
		
		//coefficents and Q's

		for (n = 0;n<nmax;n++)
		{
			
			/*boost version
			psichi = sqrt(__pi*chi/2.0)*boost::math::cyl_bessel_j(n+.5,chi);
			psimchi = sqrt(__pi*m*chi/2.0)*boost::math::cyl_bessel_j(n+.5,real(m)*chi);
			psichiprime = (sqrt(__pi*(chi+delta)/2.0)*boost::math::cyl_bessel_j(n+.5,real(m)*chi+delta))/delta;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
			psimchiprime = ((sqrt                                                                                                                                                                                                                                           (__pi*(m*chi+delta)/2.0)*boost::math::cyl_bessel_j(n+.5,(chi+delta)))-psichi)/delta;
			xichi = chi * (boost::math::sph_bessel(n,chi) + std::complex<double>(0,1) * boost::math::sph_neumann(n,chi));
			xichiprime = (((chi+delta) * (boost::math::sph_bessel(n,(chi+delta)) + std::complex<double>(0,1) * boost::math::sph_neumann(n,(chi+delta)))) - xichi)/delta;
				*/
				
			
			
			psichi = sqrt(__pi*chi/2.0)*j_chi.at(n);
			psimchi = sqrt(__pi*m*chi/2.0)*j_mchi.at(n);
			psichiprime =  sqrt(__pi/2.0)  *  (sqrt(chi)*j_chiprime.at(n)  +  0.5/sqrt(chi) * j_chi.at(n));                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
			psimchiprime = sqrt(__pi/2.0) * (sqrt(m*chi)*j_mchiprime.at(n) +  0.5/sqrt(m*chi) * j_mchi.at(n));
			xichi = sqrt(__pi*chi/2.0)*(j_chi.at(n) + std::complex<double>(0,1.0) *y_chi.at(n));
			xichiprime = sqrt(__pi/2.0)* (sqrt(chi) * (j_chiprime.at(n) + std::complex<double>(0,1.0) * y_chiprime.at(n)) + 0.5/sqrt(chi) * (j_chi.at(n) + std::complex<double>(0,1.0) * y_chi.at(n)));
				
				
			
			a.push_back(( m * psimchi*psichiprime -     psimchiprime * psichi)/( m * psimchi*xichiprime -     psimchiprime*xichi ));
			b.push_back((     psimchi*psichiprime - m * psimchiprime * psichi)/(     psimchi*xichiprime - m * psimchiprime*xichi ));
			
			if(a.back()!=a.back())
			{
				a.back() = std::complex<double>(0,0);
			}
			if(b.back()!=b.back())
			{
				b.back() = std::complex<double>(0,0);
			}
			
			if(n!=-0)
			{
				Qs += (2*n+1)*(norm(a.back()) + norm(b.back()));
				Qe += fabs(2*n+1)*real(a.back()+b.back());
			}
			
			
		}
		
		
		
		PhaseFunction.clear();
		//angle related bull shit
		
		for (theta = 0; (theta)<=3.1415926535;theta += 3.141592653589/phasesamples)
		{
			
			
			
			mu = cos(theta);
			
			S1 = 0;
			S2 = 0;
			
			for (n = 0;n<nmax;n++)
			{
				if(n ==0)
				{
					pi[n] = 0;
					tau[n] = 0;
				}
				else if(n == 1)
				{
					pi[n] = 1;
					tau[n] = n*mu*pi[n]-(n+1)*pi[n-1];
				}
				else
				{
					pi[n%3] = (2*n-1.0)/(n-1.0)*mu*pi[(n-1)%3]-double(n)/(n-1.0)*pi[(n-2)%3];
					tau[n%3] = n*mu*pi[n%3]-(n+1)*pi[(n-1)%3];
				}
				
				
				
				if(n!=0)
				{
					S1 += ((2.0*n+1.0)/(n*(n+1.0)))*(a.at(n)*pi[n%3]+b.at(n)*tau[n%3]);
					S2 += ((2.0*n+1.0)/(n*(n+1.0)))*(b.at(n)*pi[n%3]+a.at(n)*tau[n%3]);
				}
				
								
				
				
				
			}
			
			
			absS1 = abs(S1);
			absS2 = abs(S2);
			
			
			
			hold = fabs((absS1*absS1 + absS2*absS2)/Qs);
			PhaseFunction.push_back(hold);
			fprintf(FPT,"%f %f\n",theta*180/M_PI,hold*1000);
			
		}
		
		for(j = PhaseFunction.size()-1;j>0;j--) //collapse onto a d omega
		{
			theta = (j) * 3.141592653589/(phasesamples-1);
			PhaseFunction.at(j) = (PhaseFunction.at(j) + PhaseFunction.at(j - 1))/2 * (cos(theta -3.141592653589/(phasesamples-1)) - cos(theta));
			
			
		}
		PhaseFunction.at(0) = 0.0;
		for(j = 2; j< PhaseFunction.size();j++) //integrate
		{
			PhaseFunction.at(j) += PhaseFunction.at(j-1);
		}
		
		
		Ideal_Array.at(i)->PhaseFunction = PhaseFunction;
			
			
		Ideal_Array.at(i)->Qs = Qs * 2 / chi/chi;
		Ideal_Array.at(i)->Qe = Qe * 2 / chi/chi;
		Ideal_Array.at(i)->Qa = (Qe - Qs)  * 2 / chi/chi;
		
		fprintf(FPT2,"%f,", Ideal_Array.at(i)->Parms.at(0));
		for(j = 1; j < Ideal_Array.dim()  ; j++)
		{
			fprintf(FPT2,"%f,", Ideal_Array.at(i)->Parms.at(j));
		}
		
		fprintf(FPT2,"%f, %f, %f\n", Qe * 2 / chi/chi, (Qe - Qs) * 2 / chi/chi, Qs * 2 / chi/chi);
		
	}
	
	
	
	
	
		
	fclose(FPT);
	fclose(FPT2);
	
	
	printf("Ending Mie Calculation\n");
	return 1;
	
}


int OpticData::Initialize_One_Ray_Mie_Shell()
{
	printf("Beginning Mie Calculation\n");
	IndexAir = std::complex<double>(1,0);
		

	int phasesamples = 100;
	

	FILE* FPT = fopen ("MieData.plt" , "w");
	
	FILE* FPT2 = fopen ("QData.plt" , "w");
	
	if(FPT == NULL)
	{
		printf("well this sucks");
		
	}
	
	
	
	dcomp m, m2;
	dcomp msqr;
	double chi, chi2;
	int i,j, nmax;
	std::vector<dcomp> a,b,A,B;
	double theta, mu;
	int n;
	double pi[3], tau[3];
	double __pi = 3.1415926535;
	dcomp psichi2, psichi2prime ,psimchi,psimchiprime,psim2chi,psim2chiprime,psim2chi2;
	
	dcomp psim2chi2prime ,xim2chi2,xim2chi2prime,xim2chi,xim2chiprime,xxichi2,xxichi2prime;
	
	
	//double delta = .000000000001;
	dcomp S1;
	dcomp S2;
	dcomp extra;
	double hold;
	double absS1, absS2;
	
	double Qs,Qe,Qa;
		

	
	double order;
	
	
	std::vector<dcomp> j_chi2;
	std::vector<dcomp> j_mchi;
	std::vector<dcomp> j_m2chi;
	std::vector<dcomp> j_m2chi2;
	std::vector<dcomp> j_chi2prime;
	std::vector<dcomp> j_mchiprime;
	std::vector<dcomp> j_m2chiprime;
	std::vector<dcomp> j_m2chi2prime;
	std::vector<dcomp> y_chi2;
	std::vector<dcomp> y_mchi;
	std::vector<dcomp> y_m2chi;
	std::vector<dcomp> y_m2chi2;
	std::vector<dcomp> y_chi2prime;
	std::vector<dcomp> y_mchiprime;
	std::vector<dcomp> y_m2chiprime;
	std::vector<dcomp> y_m2chi2prime;
	
	
	
	std::vector<double> PhaseFunction;
	
	nmax = 300;
	
	j_chi2.resize(nmax+2);
	j_mchi.resize(nmax+2);
	j_m2chi.resize(nmax+2);
	j_m2chi2.resize(nmax+2);
	j_chi2prime.resize(nmax+2);
	j_mchiprime.resize(nmax+2);
	j_m2chiprime.resize(nmax+2);
	j_m2chi2prime.resize(nmax+2);
	y_chi2.resize(nmax+2);
	y_mchi.resize(nmax+2);
	y_m2chi.resize(nmax+2);
	y_m2chi2.resize(nmax+2);
	y_chi2prime.resize(nmax+2);
	y_mchiprime.resize(nmax+2);
	y_m2chiprime.resize(nmax+2);
	y_m2chi2prime.resize(nmax+2);
	
	
	
	fprintf(FPT,"TITLE = \"Mie Data\"\n");
	fprintf(FPT,"VARIABLES = \"Theta\",\"F\"\n");

	fprintf(FPT2,"TITLE = \"Q Data\"\n");
	fprintf(FPT2,"VARIABLES = \"Chi\",");
	for(i = 1; i < Ideal_Array.dim()  ; i++)
	{
		fprintf(FPT2," \"Parm%d\",", i);
	}
	fprintf(FPT2,"\"Qe\",\"Qa\",\"Qs\"\n");
	fprintf(FPT2,"ZONE T=\"Data\", DATAPACKING=Point\n");
	
	
	for (i = 0;i<Ideal_Array.size();i++)
	{	
		
		
		a.clear();
		b.clear();
		
		Qe = 0.0;
		Qs = 0.0;
		
		chi = Ideal_Array.at(i)->Parms.at(0)/2-Ideal_Array.at(i)->Parms.at(1);
		if(chi<0)
		{
			printf("shell depth too large\n");
			chi = 0;
		}
		
		chi2 = Ideal_Array.at(i)->Parms.at(0)/2;
		m = std::complex<double>(partn,partk);
		m2 = std::complex<double>(Ideal_Array.at(i)->Parms.at(2),Ideal_Array.at(i)->Parms.at(3));
		
	
		fprintf(FPT,"ZONE T=\"");
		fprintf(FPT,"Chi");
		
		fprintf(FPT," = %f,", Ideal_Array.at(i)->Parms.at(0));
		for(j = 1; j < Ideal_Array.dim()  ; j++)
		{
			fprintf(FPT," Parm%d = %f,", j, Ideal_Array.at(i)->Parms.at(j));
		}
		
		fprintf(FPT,"\", DATAPACKING=POINT\n");
		
		
		
		cbessjyva(nmax+1.5,chi,order,&j_chi2,&y_chi2,&j_chi2prime,&y_chi2prime);
		cbessjyva(nmax+1.5,m*chi,order,&j_mchi,&y_mchi,&j_mchiprime,&y_mchiprime);
		cbessjyva(nmax+1.5,m2*chi,order,&j_m2chi,&y_m2chi,&j_m2chiprime,&y_m2chiprime);
		cbessjyva(nmax+1.5,m2*chi2,order,&j_m2chi2,&y_m2chi2,&j_m2chi2prime,&y_m2chi2prime);

		
		//coefficents and Q's

		for (n = 0;n<nmax;n++)
		{
			
			/*boost version
			psichi = sqrt(__pi*chi/2.0)*boost::math::cyl_bessel_j(n+.5,chi);
			psimchi = sqrt(__pi*m*chi/2.0)*boost::math::cyl_bessel_j(n+.5,real(m)*chi);
			psichiprime = (sqrt(__pi*(chi+delta)/2.0)*boost::math::cyl_bessel_j(n+.5,real(m)*chi+delta))/delta;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
			psimchiprime = ((sqrt                                                                                                                                                                                                                                           (__pi*(m*chi+delta)/2.0)*boost::math::cyl_bessel_j(n+.5,(chi+delta)))-psichi)/delta;
			xichi = chi * (boost::math::sph_bessel(n,chi) + std::complex<double>(0,1) * boost::math::sph_neumann(n,chi));
			xichiprime = (((chi+delta) * (boost::math::sph_bessel(n,(chi+delta)) + std::complex<double>(0,1) * boost::math::sph_neumann(n,(chi+delta)))) - xichi)/delta;
				*/
			
			
			psichi2 = sqrt(chi2)*j_chi2.at(n);
			psichi2prime = (sqrt(chi2)*j_chi2prime.at(n)  +  0.5/sqrt(chi) * j_chi2.at(n));
			psimchi = sqrt(m*chi)*j_mchi.at(n);
			psimchiprime = (sqrt(m*chi)*j_mchiprime.at(n) +  0.5/sqrt(m*chi) * j_mchi.at(n));
			psim2chi= sqrt(m2*chi)*j_m2chi.at(n);
			psim2chiprime = (sqrt(m2*chi)*j_m2chiprime.at(n) +  0.5/sqrt(m2*chi) * j_m2chi.at(n));
			psim2chi2 = sqrt(m2*chi2)*j_m2chi2.at(n);
			psim2chi2prime = (sqrt(m2*chi2)*j_m2chi2prime.at(n) +  0.5/sqrt(m2*chi2) * j_m2chi2.at(n));
			xim2chi2 = - sqrt(m2 * chi2)*y_m2chi2.at(n);
			xim2chi2prime = - (sqrt(m2* chi2)*j_m2chi2prime.at(n)  +  0.5/sqrt(m2*chi2) * j_m2chi2.at(n));
			xim2chi = - sqrt(m2 * chi)*y_m2chi.at(n);
			xim2chiprime = - (sqrt(m2* chi)*j_m2chiprime.at(n)  +  0.5/sqrt(m2*chi) * j_m2chi.at(n));
			xxichi2 = sqrt(chi2)*(j_chi2.at(n) + std::complex<double>(0,1.0) *y_chi2.at(n));
			xxichi2prime = (sqrt(chi2) * (j_chi2prime.at(n) + std::complex<double>(0,1.0) * y_chi2prime.at(n)) + 0.5/sqrt(chi2) * (j_chi2.at(n) + std::complex<double>(0,1.0) * y_chi2.at(n)));
			
			
				
				
			
			A.push_back(( m2 * psimchi*psim2chiprime -     m * psim2chiprime * psimchi)/( m2 * psimchiprime*xim2chi - m * psimchi*xim2chiprime ));
			B.push_back(( m2 * psim2chi*psimchiprime -     m * psimchiprime * psim2chi)/( m2 * psimchi*xim2chiprime - m * psimchiprime*xim2chi ));
			
			a.push_back((      psichi2*(psim2chi2prime - A.back() * xim2chi2prime) - m2 * psichi2prime*(psim2chi2 - A.back() * xim2chi2))/(       xxichi2*(psim2chi2prime - A.back()*xim2chi2prime) -   m2 * xxichi2prime*(psim2chi2 - A.back()*xim2chi2) ));
			b.push_back(( m2 * psichi2*(psim2chi2prime - B.back() * xim2chi2prime) -      psichi2prime*(psim2chi2 - B.back() * xim2chi2))/(  m2 * xxichi2*(psim2chi2prime - B.back()*xim2chi2prime) -        xxichi2prime*(psim2chi2 - B.back()*xim2chi2) ));
			
			
			if(a.back()!=a.back())
			{
				a.back() = std::complex<double>(0,0);
			}
			if(b.back()!=b.back())
			{
				b.back() = std::complex<double>(0,0);
			}
			
			if(n!=-0)
			{
				Qs += (2*n+1)*(norm(a.back()) + norm(b.back()));
				Qe += fabs(2*n+1)*real(a.back()+b.back());
			}
			
			
		}
		
		
		
		PhaseFunction.clear();
		//angle related bull shit
		
		for (theta = 0; (theta)<=3.1415926535;theta += 3.141592653589/phasesamples)
		{
			
			
			
			mu = cos(theta);
			
			S1 = 0;
			S2 = 0;
			
			for (n = 0;n<nmax;n++)
			{
				if(n ==0)
				{
					pi[n] = 0;
					tau[n] = 0;
				}
				else if(n == 1)
				{
					pi[n] = 1;
					tau[n] = n*mu*pi[n]-(n+1)*pi[n-1];
				}
				else
				{
					pi[n%3] = (2*n-1.0)/(n-1.0)*mu*pi[(n-1)%3]-double(n)/(n-1.0)*pi[(n-2)%3];
					tau[n%3] = n*mu*pi[n%3]-(n+1)*pi[(n-1)%3];
				}
				
				
				
				if(n!=0)
				{
					S1 += ((2.0*n+1.0)/(n*(n+1.0)))*(a.at(n)*pi[n%3]+b.at(n)*tau[n%3]);
					S2 += ((2.0*n+1.0)/(n*(n+1.0)))*(b.at(n)*pi[n%3]+a.at(n)*tau[n%3]);
				}
				
								
				
				
				
			}
			
			
			absS1 = abs(S1);
			absS2 = abs(S2);
			
			
			
			hold = fabs((absS1*absS1 + absS2*absS2)/Qs);
			PhaseFunction.push_back(hold);
			fprintf(FPT,"%f %f\n",theta*180/M_PI,hold*1000);
			
		}
		
		for(j = PhaseFunction.size()-1;j>0;j--)
		{
			theta = (j) * 3.141592653589/(phasesamples-1);
			PhaseFunction.at(j) = (PhaseFunction.at(j) + PhaseFunction.at(j - 1))/2 * (cos(theta -3.141592653589/(phasesamples-1)) - cos(theta));
			
			
		}
		PhaseFunction.at(0) = 0.0;
		for(j = 2; j< PhaseFunction.size();j++)
		{
			PhaseFunction.at(j) += PhaseFunction.at(j-1);
		}
		
		
		Ideal_Array.at(i)->PhaseFunction = PhaseFunction;
			
			
		Ideal_Array.at(i)->Qs = Qs * 2 / chi2/chi2;
		Ideal_Array.at(i)->Qe = Qe * 2 / chi2/chi2;
		Ideal_Array.at(i)->Qa = (Qe - Qs)  * 2 / chi2/chi2;
		
		fprintf(FPT2,"%f,", Ideal_Array.at(i)->Parms.at(0));
		for(j = 1; j < Ideal_Array.dim()  ; j++)
		{
			fprintf(FPT2,"%f,", Ideal_Array.at(i)->Parms.at(j));
		}
		
		fprintf(FPT2,"%f, %f, %f\n", Qe * 2 / chi2/chi2, (Qe - Qs) * 2 / chi2/chi2, Qs * 2 / chi2/chi2);
		
	}
	
	
	
	
	
		
	fclose(FPT);
	fclose(FPT2);
	
	
	printf("Ending Mie Calculation\n");
	return 1;
	
}





int OpticData::Initialize_One_Ray_Mie_Phase()
{
	Initialize_One_Ray_Mie();
	return 0;
}

int OpticData::Initialize_One_Ray_Mie_NoTP()
{
	Initialize_One_Ray_Mie();
	return 0;
}

int OpticData::Initialize_One_Ray_Isotropic()
{
	printf("Beginning Isotropic Calculation\n");
	IndexAir = std::complex<double>(1,0);
		
	int i,j;
	double theta;
	int phasesamples = 100;
	

	
	
	
	
	
	
	
	
	
	std::vector<double> PhaseFunction;
	
	
	for (i = 0 ;i < phasesamples;i++)
	{
		PhaseFunction.push_back(1);
	}	
	
	for(j = PhaseFunction.size()-1;j>0;j--) //collapse onto a d omega
	{
		theta = (j) * 3.141592653589/(phasesamples-1);
		PhaseFunction.at(j) = (PhaseFunction.at(j) + PhaseFunction.at(j - 1))/2 * (cos(theta -3.141592653589/(phasesamples-1)) - cos(theta));
		
		
	}
	PhaseFunction.at(0) = 0.0;
	for(j = 2; j< PhaseFunction.size();j++) //integrate
	{
		PhaseFunction.at(j) += PhaseFunction.at(j-1);
	}
	
	
	
	for (i = 0;i<Ideal_Array.size();i++)
	{	
			
		
		Ideal_Array.at(i)->Qs = Ideal_Array.at(i)->Parms.at(2);
		
		Ideal_Array.at(i)->Qa = Ideal_Array.at(i)->Parms.at(1);
		Ideal_Array.at(i)->Qe = Ideal_Array.at(i)->Parms.at(2) + Ideal_Array.at(i)->Parms.at(1);
		Ideal_Array.at(i)->PhaseFunction = PhaseFunction;
	}
	
	printf("Ending Isotropic Calculation\n");
	return 1;
	
}



double  Particle_Ideal::GetAngle()
{
	double n = ranf() * PhaseFunction.back(); 
	
	int i;
	
	for(i=1;i<PhaseFunction.size();i++) //really should be a binary search
	{
		if (n<PhaseFunction.at(i))
		{
			return 3.1415926535 * (i - 1 + (n - PhaseFunction.at(i-1))/(PhaseFunction.at(i) - PhaseFunction.at(i-1)))/PhaseFunction.size();			
		}
		
	}
	
	printf("Dear God Help, %f, %f \n" , n , PhaseFunction.back());
	
	return 0;

}


