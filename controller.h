#ifndef _CONTROL_
#define _CONTROL_




#define SCATTER_TXT \
X(One_Ray_Mie) \
X(One_Ray_Mie_Phase) \
X(One_Ray_Mie_Shell) \
X(One_Ray_Isotropic) \
X(One_Ray_Mie_NoTP) \

//X(Black_Sphere) \

#define SCATTER_DIM \
X(1) \
X(1) \
X(4) \
X(3) \
X(1) \

//X(1) \

// diameter
// diameter
//diamter, shell depth, shell n, shell k
//diamter, Q a, Q s

#define SCATTER_POS \
X(0) \
X(0) \
X(0) \
X(0) \
X(1) \

//X(0) \

#define EMIT_TXT \
X(Conical_Panel_E) \
X(Gaussian_E) \
X(Gaussian_E_DIV) \
X(Lamp_Array_E)\


#define COLLECT_TXT \
X(Panel_C) \
X(Sphere_C) \
X(Offset_Panel_C) \
X(Sphere_Inf_C) \
//X(Offset_Panel_Reflection_C) \
//X(Panel_Reflection_C) \
//X(Panel_Periodic_XY_C) \


#define TRACE_TXT \
X(Dull_New) \
X(Dull) \
X(Phase) \
X(Homogenized) \
X(Homogenized_II) \
X(Homogenized_III) \
X(Homogenized_II_offset) \
X(Homogenized_III_offset) \
X(Homogenized_II_free_stream) \


#define BOUND_TXT \
X(Glass) \
X(Reflect_Full) \
X(Partial_Reflect) \
X(Periodic) \


#define WRITE_OUT_TXT \
X(Remesh)\
X(Heats)\
X(HeatsII)\
X(HeatsTEC_ASCII)\
X(Maps_vtk)\
X(BeamTime)\
X(BeamTimeII)\
X(BeamTimeIII)\
X(ScatterStats)\
X(FreePathsStats)\
X(GLine_Z)\


enum ScatterTypes 
{ 
	#   define X(a) a,
	SCATTER_TXT NUM_FUNCTIONS
	#   undef X
};
char const* const ScatterStrings[] = 
{
#   define X(a) #a,
SCATTER_TXT
#   undef X
    0
};	

const int ScatterPos [] = 
{
#   define X(a) a,
SCATTER_POS
#   undef X
    0
};


const int ScatterDims[NUM_FUNCTIONS] = 
	{
		#   define X(a) a,
		SCATTER_DIM
		#   undef X
	};	
	
enum EmissionTypes 
{ 
	#   define X(a) a,
	EMIT_TXT NUM_EMIT
	#   undef X
};


	
char const* const EmitStrings[] = 
{
#   define X(a) #a,
EMIT_TXT
#   undef X
    0
};	

enum BoundaryTypes 
{ 
	#   define X(a) a,
	BOUND_TXT NUM_BOUNDS
	#   undef X
};

char const* const BoundaryStrings[] = 
{
	#   define X(a) #a,
	BOUND_TXT
	#   undef X
    0
};

enum CollectionTypes
{ 
	#   define X(a) a,
	COLLECT_TXT NUM_COLLECT
	#   undef X
};
	
char const* const CollectStrings[] = 
{
	#   define X(a) #a,
	COLLECT_TXT
	#   undef X
    0
};	


enum TraceTypes
{ 
	#   define X(a) a,
	TRACE_TXT NUM_TRACE
	#   undef X
};
	
char const* const TraceStrings[] = 
{
	#   define X(a) #a,
	TRACE_TXT
	#   undef X
    0
};	

enum Directions
{
	UpX,DownX,UpY,DownY,UpZ,DownZ
};


enum WriteOutTypes 
{ 
	#   define X(a) a,
	WRITE_OUT_TXT NUM_WRITE_OUT
	#   undef X
};

char const* const WriteOutStrings[] = 
{
	#   define X(a) #a,
	WRITE_OUT_TXT
	#   undef X
    0
};

#include "TraceOut.h"


#include <vector>
#include <deque>
#include <math.h>
#include "ray.h"
#include <random>
#include <fstream>
#include <iostream>
#include <sstream>

#include <time.h>


#include <cmath>

double DISTANCE(double a1,double a2,double a3,double b1,double b2,double b3);


#include "optics.h"
class controller
{
	public:
	
	
	
	
	
	double radius;
	double azmuthial;
	double zenith;
	double x,y,z;
	double optics[2];
	double dis;
	double divergence;
	std::default_random_engine dre; 
	
	
	//Domain variables
	double domainX; //size in m
	int domain_elemX; // numer of elements in X dir
	double domainY;
	int domain_elemY;
	double domainZ;
	int domain_elemZ;
	double Xelem_size;
	double Yelem_size;
	double Zelem_size;
	
	
	double domain_int(int i);
	double domain_ext[6];
	BoundaryTypes bounds[6];
	double thicknesses[6];
	int BoundaryOut(ray *Ray, int direction);
	void BoundaryIn(ray *Ray, int direction);
	
	
	double glass_thickness;
	double cone_angle;
	
	
	double emissionXmin;
	double emissionXmax;
	double emissionYmin;
	double emissionYmax;
	double emissionZ;
	
	double gaussx, gaussy;
	double BeamRadius;
	double BeamMu; //one over e squared width of beam
	
	double offsetX;
	double offsetY;
	double offsetZ;
	
	
	
	double ParticleRadius;
	double ParticleRadius2;
	
	double MinPowerThreshold;
	
	std::vector<double> PowerEmitted;
	double PE() { double sum=0; for (int i = 0; i< PowerEmitted.size();i++)
		{sum += PowerEmitted.at(i);} return sum;}
	
	
	
	
	std::vector<std::vector<std::vector<std::vector<int> > > > mesh_exact;
	std::vector<std::vector<std::vector<std::vector<int> > > > mesh_overlapped;
	std::vector<std::vector<std::vector<std::vector<double> > > > mesh_properties;
	
	std::vector<std::vector<int>> Scatt_vec;
	
	//partivle positions stored in vectors filled in ReadParticleData
	std::vector<double> ParticleX;
	std::vector<double> ParticleY;
	std::vector<double> ParticleZ;
	std::vector<std::vector<double>> ParticleParms; //diameter
	std::vector<std::vector<double>> ParticleQ; //vector by particle then vector by trial
	double ParticleParm(int i, int j);
	std::vector<double> ParmVec(int i, double lambda);
	
	//particle propeties logistics
	std::vector<int> ParticleDimPos;
	std::vector<double> ParticleDefaultVal;
	std::vector<int> ParticleBins;
	std::vector<int> ParticleIndecies; // the idecies list are 1 ordered just to screw with you
	std::vector<bool> ParticleNormals;
	
	
	
	//diagnostic variables for simulation
	int Lost_at_emission;
	int Lost_in_transmission;
	int Lost_at_particle;
	int Lost_in_numbers;
	
	
	//I wanted these in the begin trial function, but I needed to reference them in the scattering functions, so they're here
	
	//functions for simulation
	double sqr(double a) {return a*a;};
	
	

	double elem_size(int i);
	int domain_elem(int i);
	double domain(int i);
	
	double PathCutOff;
	
	TraceOut_Base EmitMap;

	
	
	//Kinda Obvious
	OpticData Optics;
	int Generate_Ideals();
	//Scattering stuff
	

	
	//see scattering.c
	//const char *ScatterNames[2] = {"absorb_transmit", "absorb_transmit_BB"};
		
	
	# define X(q) \
	int Scatter_##q (int i, std::deque<ghost> *ghost_vec,std::vector<ray> *ray_vec, int trial);\
	
	SCATTER_TXT
	#   undef X
	
	int (controller::*ScatFunc)(int i, std::deque<ghost> *ghost_vec,std::vector<ray> *ray_vec, int trial);
	//Do i care about phase?
	
	
	bool TakePosition(){return ScatterPos[Scat];}
	
	
	
	//TraceOuts
	
	
	
	
	
	double CollectionRadius;
	int bundle_size;
	double WindowSize;
	double WindowOffset;
	int mapxsamples;
	int mapysamples;
	int mapscattsamples;
	
	//Traces
	
		
	# define X(q) \
	int TraceRay_##q (ray *Ray, int trial);\
	int Init_TraceRay_##q ();\
	
	TRACE_TXT
	#   undef X
	
	int (controller::*Init_TraceRay)();
	int (controller::*TraceRay)(ray *Ray, int trial);
	
	double TraceParam1;
	
	//TraceIns
	
	
	# define X(q) \
	void TraceRayIn_##q (ray *Ray, int trial);\
	
	EMIT_TXT
	#   undef X
	
	
	void (controller::*TraceInFunc)(ray *Ray, int trial);
	
	//std::vector<std::vector<std::vector<dcomp_counted>>> FullEmitMap; // data from all trials
	// std::vector<std::vector<dcomp_counted>> EmitMap;//for each trial
	
	
	int LogCount;
	
	

	
	controller();
	~controller();
	
	//not a real function
	void print();
	
	
	int ReadConfigFile(char *file);
	int ReadXMLConfigFile(char *file);
	int WriteXMLConfig(char *file);
	int UpdateXMLDecrypt(int argc, char **argv);
	
	int SetDefaults();	
	int ReadParticleData(char *file);
	
	int InitiateDomain(double X, int nX, double Y, int nY, double Z, int nZ, double G);
	int InitiateDomain();

	
	int Sort_Particles();
	
	
	int BeginSimulation(int trials, int rays_per_trials);
	
	int RunTrial(int trial);
	int RunTrial_II(int trial);
	int RunTrial_III(int trial);
	
	int NUM_RAYS;
	int NUM_TRIALS;
	
	
	double EnergyScaling;
	

	
	
	
	class WriteOut
	{
		private:
		
		
		public:
		virtual void SetNames() {};
		controller * M_;
		~WriteOut(){};
		WriteOut(){};
		WriteOut(controller * M) {M_ = M;};
		std::vector<double> Params;
		std::vector<std::string> ParamNames;
		WriteOut(std::vector<double> parms){SetNames();};
		virtual int Run(int posflag){return 1;};
	};
	
	# define X(q) \
	class Write ##q :public controller::WriteOut \
	{\
		private:\
		public:\
		void SetNames();\
		Write ##q (controller * M) {M_ = M;};\
		int Run(int posflag);\
	};\
	
	
	WRITE_OUT_TXT
	#   undef X
	
	
	
	std::vector<WriteOut*> WriteOuts;
	
	int WriteData()
	{
		for(int i = 0; i < WriteOuts.size();i++)
		{ WriteOuts.at(i)->Run(i);}
		return 1;
	}
	
	std::vector<TraceOut_Base*> TraceOuts;
	int clean(){for(int i = 0; i < TraceOuts.size() ;i++)
		{delete TraceOuts.at(i);} 
		for(int i = 0; i < WriteOuts.size() ;i++)
		{delete WriteOuts.at(i);}return 1;};
	
	
	//for config file me thinks
	int ExportConfigData();
	
	ScatterTypes Scat;
	int iteration_limit;
	double initial_temp;
	double prt_cp;
	bool ReadInitalTemperatures;
	EmissionTypes EmissionType;
	CollectionTypes CollectionType;
	TraceTypes TraceType;
	WriteOutTypes WriteOutType;
	double particle_heat_capacity;
	bool DividedImpactMap;
	bool MonitorPhase;
	
	double TimeStamp;
	
	bool LogRays;
	bool PathLengths;
	
	
	std::vector<std::vector<double>> FreePaths;
	int PathBins;
	double MaxPath;
	
	
	int ChangeXMLConfig(int argc, char **argv);
	
	
	//Disagnotic
	
	time_t start; 
	time_t end;
	double TimeElapsed;
	
	#if LOGRAYS
	FILE* RAYLOG;
	#endif
	
	#if TIMER
	FILE* TIMERFILE;
	#endif
};
	














#endif
