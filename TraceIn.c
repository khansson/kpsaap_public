

#include <stdio.h>


#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <deque>
#include "controller.h"
#include <stdlib.h>
#include <math.h> 
#include "ray.h"
#include "ranf.h"
#include "optics.h"
#include <algorithm>
#include <chrono>
#include <random>
#include <time.h>





void controller::BoundaryIn(ray *Ray, int direction)
{
	int dir = direction/2+1;
	int sign = -1*direction%2;
	
	
	
	
	double depth = domain_int(direction) - Ray->R(dir);
	

	//printf("%d, %d, %f, %f, %f\n",direction, dir, domain_int(direction), Ray->R(dir), depth);
		
	double transverse[2];
	double dis;
	
		
	transverse[0] = Ray->R(dir +1) + Ray->D(dir +1) /  Ray->D(dir) * depth;
	transverse[1] = Ray->R(dir +2) + Ray->D(dir +2) /  Ray->D(dir) * depth;
	
	
	//printf("%f,%f,%f,%f,%f,%f, %f\n", transverse[0], transverse[1], domain_ext[((dir)%3*2)+1], domain_ext[((dir)%3*2)], domain_ext[((dir+1)%3*2)+1] ,domain_ext[((dir+1)%3*2)], Ray->D(dir) * depth);
	
	if (transverse[0] < domain_ext[((dir)%3*2)+1] || transverse[0] > domain_ext[((dir)%3*2)] ||  transverse[1] <  domain_ext[((dir+1)%3*2)+1] || transverse[1] > domain_ext[((dir+1)%3*2)]|| Ray->D(dir) * depth < 0 || Ray->D(dir) == 0)
	{
		//printf("death\n");
		Ray->obliterate();
		//Ray->PrintData();
		//printf("%d\n",i);
		Lost_at_emission += 1;
		
	}
	else
	{
		dis = DISTANCE(depth,transverse[0],transverse[1],0,Ray->R(dir +1),Ray->R(dir +2));
		//printf("%f\n",dis);
		
		Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
		Ray->aT(dis);
		Ray->R(dir,domain_int(direction));
		Ray->R(dir +1, transverse[0]);
		Ray->R(dir +2, transverse[1]);
	
		if (transverse[0]< domain(dir +1) &&  transverse[0]> 0 &&transverse[1]< domain(dir +2) &&  transverse[1]> 0)
		{
			//printf("do we ever get here?\n");
			Ray->E(dir, 0);
			Ray->E(dir+1, floor(transverse[0]/elem_size(dir+1)));
			Ray->E(dir+2, floor(transverse[1]/elem_size(dir+2)));
			//printf("%d, %f,%f,%f\n", dir, 0.0, transverse[0]/elem_size(dir+1), transverse[1]/elem_size(dir+2));
			//printf("%d, %d, %d\n ", Ray->EX(), Ray->EY(), Ray->EZ());
		}
		else if (transverse[0] < domain_ext[((dir)%3*2)] && transverse[0] > domain_int(((dir)%3*2)))
		{
			
			BoundaryIn(Ray, ((dir)%3*2));
			
			return;
			
		}
		
		else if (transverse[0] >  domain_ext[((dir)%3*2)+1] && transverse[0] <  domain_int(((dir)%3*2)+1))
			
		{
			
					
			BoundaryIn(Ray, ((dir)%3*2)+1);
			
			return;
		}
		else if (transverse[1] < domain_ext[((dir+1)%3*2)] && transverse[1] > domain_int(((dir+1)%3*2)))
		{
			
			BoundaryIn(Ray, ((dir+1)%3*2));
			
			return;
			
		}
		else if (transverse[1] >  domain_ext[((dir+1)%3*2)+1] && transverse[1] <  domain_int(((dir+1)%3*2)+1))	
		{
			
			BoundaryIn(Ray, ((dir+1)%3*2+1));
			
			return;
			
			
		}
	}
	
	return;
	
}




void controller::TraceRayIn_Conical_Panel_E(ray *Ray, int trial)
{
	
	
	
	Optics.emission_spectrum(&optics[0]);	
	PowerEmitted.at(trial) += (optics[0]);
	
	double p;
	
	
	azmuthial = 2* M_PI *ranf(trial);
	do
	{
		zenith = ranf(trial)*std::min(M_PI/4, cone_angle);
		p = ranf(trial)* sin(std::min(M_PI/4, cone_angle));
	}while(p>sin(zenith));
		
	Ray->fill((domain_ext[0] - domain_ext[1])*ranf() + domain_ext[1], (domain_ext[2] - domain_ext[3])*ranf() + domain_ext[3], domain_ext[5] , sin(azmuthial)*sin(zenith), cos(azmuthial)*sin(zenith),cos(zenith) ,optics[0],optics[1]);
	
	//printf("this is the point\n");
	
	BoundaryIn(Ray,5);
	
	return;
	
	/*
	y = Ray->DY() * glass_thickness/Ray->DZ()+ Ray->Y();
	x = Ray->DX() * glass_thickness/Ray->DZ()+ Ray->X();
	z = 0;
	if(y < domainY && y > 0 && x < domainX && x > 0)
	{
		dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
		Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
		Ray->aT(dis);
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		Ray->set_elem(x/Xelem_size, y/Yelem_size, 0);
		//printf("type 1 %f\n",Ray->Power());
		#if LOGRAYS
		{			
			fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
		}			
		#endif
		 return; 
	}
	else
	{
		y = Ray->DY() * (glass_thickness+domainZ)/Ray->DZ()+ Ray->Y();
		x = Ray->DX() * (glass_thickness+domainZ)/Ray->DZ()+ Ray->X();
		if(y < domainY && y > 0 && x < domainX && x > 0)
		{
			x = Ray->DX() * (domainY - Ray->Y())/Ray->DY() + Ray->X();
			z = Ray->DZ() * (domainY - Ray->Y())/Ray->DY() + Ray->Z();
			y = domainY;
			
			if(x < domainX && x > 0 && z < domainZ && z > 0)
			{
				
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(x/Xelem_size, domain_elemY -1, z/Zelem_size);
				//printf("type 2 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			
			x = Ray->DX() * (0 - Ray->Y())/Ray->DY() + Ray->X();
			z = Ray->DZ() * (0 - Ray->Y())/Ray->DY() + Ray->Z();
			y = 0.0;
			
			if(x < domainX && x > 0 && z < domainZ && z > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(x/Xelem_size, 0, z/Zelem_size);
				//printf("type 3 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			x = domainX;
			z = Ray->DZ() * (domainX - Ray->X())/Ray->DX() + Ray->Z();
			y = Ray->DY() * (domainX - Ray->X())/Ray->DX() + Ray->Y();
			
			if(z < domainZ && z > 0 && y < domainY && y > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(domain_elemX -1, y/Yelem_size, z/Zelem_size );
				//printf("type 4 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			x = 0.0;
			z = Ray->DZ() * (0 - Ray->X())/Ray->DX() + Ray->Z();
			y = Ray->DY() * (0 - Ray->X())/Ray->DX() + Ray->Y();
			
			if(z < domainZ && z > 0 && y < domainY && y > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(0, y/Yelem_size, z/Zelem_size );
				//printf("type 5 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			printf("I shouldn't be here, should I?\n");
			Ray->obliterate();
			 
			
			
		}
		else
		{
			Ray->obliterate();
			//printf("%d\n",i);
			Lost_at_emission += 1;
		}
	}
	
	#if LOGRAYS	
	if(Ray->dead())
	{
		fprintf(RAYLOG,"0.0,0.0,0.0,0,");
	}
	#endif   
	* */
	
}
void controller::TraceRayIn_Gaussian_E(ray *Ray, int trial)
{
	
	Optics.emission_spectrum(&optics[0]);	
	PowerEmitted.at(trial) += (optics[0]);
	
	azmuthial = 2* 3.14159265 *ranf(trial);
	do
	{
		radius = BeamMu*sqrt(-log(ranf(trial)));
	}while (radius>BeamRadius);
	Ray->fill(gaussx + radius * cos(azmuthial),  gaussy + radius * sin(azmuthial), domain_ext[5] , 0.0,0.0,1.0 ,optics[0],optics[1]);
	
	
	BoundaryIn(Ray,5);
	
	return;
	
	/*
	y = Ray->DY() * glass_thickness/Ray->DZ()+ Ray->Y();
	x = Ray->DX() * glass_thickness/Ray->DZ()+ Ray->X();
	z = 0;
	if(y < domainY && y > 0 && x < domainX && x > 0)
	{
		dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
		Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
		Ray->aT(dis);
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		Ray->set_elem(x/Xelem_size, y/Yelem_size, 0);
		//printf("type 1 %f\n",Ray->Power());
		#if LOGRAYS
		{			
			fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
		}			
		#endif
		 return; 
	}
	else
	{
		y = Ray->DY() * (glass_thickness+domainZ)/Ray->DZ()+ Ray->Y();
		x = Ray->DX() * (glass_thickness+domainZ)/Ray->DZ()+ Ray->X();
		if(y < domainY && y > 0 && x < domainX && x > 0)
		{
			x = Ray->DX() * (domainY - Ray->Y())/Ray->DY() + Ray->X();
			z = Ray->DZ() * (domainY - Ray->Y())/Ray->DY() + Ray->Z();
			y = domainY;
			
			if(x < domainX && x > 0 && z < domainZ && z > 0)
			{
				
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(x/Xelem_size, domain_elemY -1, z/Zelem_size);
				//printf("type 2 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			
			x = Ray->DX() * (0 - Ray->Y())/Ray->DY() + Ray->X();
			z = Ray->DZ() * (0 - Ray->Y())/Ray->DY() + Ray->Z();
			y = 0.0;
			
			if(x < domainX && x > 0 && z < domainZ && z > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(x/Xelem_size, 0, z/Zelem_size);
				//printf("type 3 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			x = domainX;
			z = Ray->DZ() * (domainX - Ray->X())/Ray->DX() + Ray->Z();
			y = Ray->DY() * (domainX - Ray->X())/Ray->DX() + Ray->Y();
			
			if(z < domainZ && z > 0 && y < domainY && y > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(domain_elemX -1, y/Yelem_size, z/Zelem_size );
				//printf("type 4 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			x = 0.0;
			z = Ray->DZ() * (0 - Ray->X())/Ray->DX() + Ray->Z();
			y = Ray->DY() * (0 - Ray->X())/Ray->DX() + Ray->Y();
			
			if(z < domainZ && z > 0 && y < domainY && y > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(0, y/Yelem_size, z/Zelem_size );
				//printf("type 5 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			printf("I shouldn't be here, should I?\n");
			Ray->obliterate();
			 
			
			
		}
		else
		{
			Ray->obliterate();
			//printf("%d\n",i);
			Lost_at_emission += 1;
		}
	}
	
	#if LOGRAYS	
	if(Ray->dead())
	{
		fprintf(RAYLOG,"0.0,0.0,0.0,0,");
	}
	#endif
	*/
	
}
void controller::TraceRayIn_Gaussian_E_DIV(ray *Ray, int trial)
{
	
	Optics.emission_spectrum(&optics[0]);	
	PowerEmitted.at(trial) += (optics[0]);
	
	azmuthial = 2* 3.14159265 *ranf(trial);
	do
	{
		radius = BeamMu*sqrt(-log(ranf(trial)));
	}while (radius>BeamRadius);
	divergence = optics[1]/(M_PI*BeamMu*BeamMu)*radius;
	Ray->fill(gaussx + radius * cos(azmuthial),  gaussy + radius * sin(azmuthial), emissionZ , cos(azmuthial)*sin(divergence),sin(azmuthial)*sin(divergence),cos(divergence) ,optics[0],optics[1]);
	
	
	BoundaryIn(Ray,5);
	
	return;
	/*
	y = Ray->DY() * glass_thickness/Ray->DZ()+ Ray->Y();
	x = Ray->DX() * glass_thickness/Ray->DZ()+ Ray->X();
	z = 0;
	if(y < domainY && y > 0 && x < domainX && x > 0)
	{
		dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
		Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
		Ray->aT(dis);
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		Ray->set_elem(x/Xelem_size, y/Yelem_size, 0);
		//printf("type 1 %f\n",Ray->Power());
		#if LOGRAYS
		{			
			fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
		}			
		#endif
		return; 
	}
	else
	{
		y = Ray->DY() * (glass_thickness+domainZ)/Ray->DZ()+ Ray->Y();
		x = Ray->DX() * (glass_thickness+domainZ)/Ray->DZ()+ Ray->X();
		if(y < domainY && y > 0 && x < domainX && x > 0)
		{
			x = Ray->DX() * (domainY - Ray->Y())/Ray->DY() + Ray->X();
			z = Ray->DZ() * (domainY - Ray->Y())/Ray->DY() + Ray->Z();
			y = domainY;
			
			if(x < domainX && x > 0 && z < domainZ && z > 0)
			{
				
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(x/Xelem_size, domain_elemY -1, z/Zelem_size);
				//printf("type 2 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			
			x = Ray->DX() * (0 - Ray->Y())/Ray->DY() + Ray->X();
			z = Ray->DZ() * (0 - Ray->Y())/Ray->DY() + Ray->Z();
			y = 0.0;
			
			if(x < domainX && x > 0 && z < domainZ && z > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(x/Xelem_size, 0, z/Zelem_size);
				//printf("type 3 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			x = domainX;
			z = Ray->DZ() * (domainX - Ray->X())/Ray->DX() + Ray->Z();
			y = Ray->DY() * (domainX - Ray->X())/Ray->DX() + Ray->Y();
			
			if(z < domainZ && z > 0 && y < domainY && y > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(domain_elemX -1, y/Yelem_size, z/Zelem_size );
				//printf("type 4 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			x = 0.0;
			z = Ray->DZ() * (0 - Ray->X())/Ray->DX() + Ray->Z();
			y = Ray->DY() * (0 - Ray->X())/Ray->DX() + Ray->Y();
			
			if(z < domainZ && z > 0 && y < domainY && y > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(0, y/Yelem_size, z/Zelem_size );
				//printf("type 5 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			printf("I shouldn't be here, should I?\n");
			Ray->obliterate();
			 
			
			
		}
		else
		{
			Ray->obliterate();
			//printf("%d\n",i);
			Lost_at_emission += 1;
		}
	}
	
	#if LOGRAYS	
	if(Ray->dead())
	{
		fprintf(RAYLOG,"0.0,0.0,0.0,0,");
	}
	#endif
	
	return; 
	* 
	* */
}

void controller::TraceRayIn_Lamp_Array_E(ray *Ray, int trial)
{
	
	
	
	Optics.emission_spectrum(&optics[0]);	
	
	
	
	
	/*
	std::normal_distribution<double> distribution(0.0,cone_angle/2);
	
	
	do
	{
		zenith = distribution(dre);
	}while(zenith<0 || zenith> M_PI/4);
	x = domainX / 2 + ((floor(ranf(trial)*2.0) * (-2) + 1) * (.0009 + 0.0066 * floor(ranf(trial)* 12.0 )+ .0048 * ranf(trial)));
	
	y = domainY /2 - .0048/2 + .0048 * ranf(trial);
	z = - 0.1155;
	
	
	
	azmuthial = 2* 3.14159265 *ranf(trial);	
	
	Ray->fill(x,  y, z, cos(azmuthial)*sin(zenith),sin(azmuthial)*sin(zenith),cos(zenith) ,optics[0]*sin(zenith)*cos(zenith),optics[1]);
	
	PowerEmitted.at(trial) += (optics[0]*sin(zenith)*cos(zenith));
	
	
	*/
	
	
	/*
	
	zenith = ranf(trial)*M_PI/4;
	
	x = domainX / 2   + ((floor(ranf(trial)*2.0) * (-2) + 1) * (.0009 + 0.0066 * floor(ranf(trial)* 12.0 )+ .0048 * ranf(trial)));
	
	y = domainY /2 - .0048/2 + .0048 * ranf(trial);
	z = - 0.1155;
	
	double pow = (optics[0]*expl(-zenith*zenith*2/(cone_angle*cone_angle))*sin(zenith)*cos(zenith));
	
	PowerEmitted.at(trial) += pow;
	
	
	
	azmuthial = 2* 3.14159265 *ranf(trial);	
	
	
	Ray->fill(x,  y, z, cos(azmuthial)*sin(zenith),sin(azmuthial)*sin(zenith),cos(zenith) ,pow,optics[1]);
	
	*/
	
	double p;
	do
	{
		zenith = ranf(trial)*M_PI/4;
		p = ranf(trial)*.05;
	}while(p>expl(-zenith*zenith*2/(cone_angle*cone_angle))*sin(zenith));
	
	x = domainX / 2 + ((floor(ranf(trial)*2.0) * (-2) + 1) * (.0009 + 0.0066 * floor(ranf(trial)* 12.0 )+ .0048 * ranf(trial)));
	
	y = domainY /2 - .0048/2 + .0048 * ranf(trial);
	z = - 0.1155;
	
	double pow = 1.0;
	
	PowerEmitted.at(trial) += pow;
	
	
	
	azmuthial = 2* 3.14159265 *ranf(trial);	
	
	
	Ray->fill(x,  y, z, cos(azmuthial)*sin(zenith),sin(azmuthial)*sin(zenith),cos(zenith) ,pow,optics[1]);
	
	
	
	do
	{
		y = Ray->DY() * (domain_ext[5] - Ray->Z())/Ray->DZ()+ Ray->Y();
		x = Ray->DX() * (domain_ext[5] - Ray->Z())/Ray->DZ()+ Ray->X();
		z = domain_ext[5];
		if(y < domain_ext[2] && y > domain_ext[3] && x < domain_ext[0] && x > domain_ext[1])
		{
			dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
			Ray->aT(dis);
			Ray->X(x);
			Ray->Y(y);
			Ray->Z(z);
			//printf("type 1 %f\n",Ray->Power());
			#if LOGRAYS
			{			
				fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
			}			
			#endif
			BoundaryIn(Ray,5);
			
			 break;  
		}
		else
		{
			y = Ray->DY() * (domain_ext[4]- Ray->Z())/Ray->DZ()+ Ray->Y();
			x = Ray->DX() * (domain_ext[4]- Ray->Z())/Ray->DZ()+ Ray->X();
			if(y < domain_ext[2] && y > domain_ext[3] && x < domain_ext[0] && x > domain_ext[1])
			{
				x = Ray->DX() * (domain_ext[2] - Ray->Y())/Ray->DY() + Ray->X();
				z = Ray->DZ() * (domain_ext[2]- Ray->Y())/Ray->DY() + Ray->Z();
				y = domain_ext[2];
				
				if(x < domain_ext[0] && x > domain_ext[1] && z < domain_ext[4] && z > domain_ext[5])
				{
					
					dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
					Ray->aT(dis);
					Ray->X(x);
					Ray->Y(y);
					Ray->Z(z);
					//printf("type 2 %f\n",Ray->Power());
					#if LOGRAYS
					{			
						fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
					}			
					#endif
					BoundaryIn(Ray,2);
					
					 break; 
				}
				
				
				x = Ray->DX() * (domain_ext[3] - Ray->Y())/Ray->DY() + Ray->X();
				z = Ray->DZ() * (domain_ext[3] - Ray->Y())/Ray->DY() + Ray->Z();
				y = domain_ext[3];
				
				if(x < domain_ext[0] && x > domain_ext[1] && z < domain_ext[4] && z > domain_ext[5])
				{
					dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
					Ray->aT(dis);
					Ray->X(x);
					Ray->Y(y);
					Ray->Z(z);
					Ray->set_elem(x/Xelem_size, 0, z/Zelem_size);
					//printf("type 3 %f\n",Ray->Power());
					#if LOGRAYS
					{			
						fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
					}			
					#endif
					
					BoundaryIn(Ray,3);
					 break;  
				}
				
				x = domain_ext[0];
				z = Ray->DZ() * (domain_ext[0]- Ray->X())/Ray->DX() + Ray->Z();
				y = Ray->DY() * (domain_ext[0] - Ray->X())/Ray->DX() + Ray->Y();
				
				if(z < domain_ext[4] && z > domain_ext[5] && y < domain_ext[2] && y > domain_ext[3])
				{
					dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
					Ray->aT(dis);
					Ray->X(x);
					Ray->Y(y);
					Ray->Z(z);
					Ray->set_elem(domain_elemX -1, y/Yelem_size, z/Zelem_size );
					//printf("type 4 %f\n",Ray->Power());
					#if LOGRAYS
					{			
						fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
					}			
					#endif
					
					BoundaryIn(Ray,0);
					 break;  
				}
				
				x =domain_ext[1];
				z = Ray->DZ() * (domain_ext[1] - Ray->X())/Ray->DX() + Ray->Z();
				y = Ray->DY() * (domain_ext[1] - Ray->X())/Ray->DX() + Ray->Y();
				
				if(z < domain_ext[4] && z > domain_ext[5] && y < domain_ext[2] && y > domain_ext[3])
				{
					dis = DISTANCE(Ray->Z(),Ray->Y(),Ray->X(),z,y,x);
					Ray->aT(dis);
					Ray->X(x);
					Ray->Y(y);
					Ray->Z(z);
					//printf("type 5 %f\n",Ray->Power());
					#if LOGRAYS
					{			
						fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
					}			
					#endif
					
					BoundaryIn(Ray,1);
					
					break;
					 
				}
				
				printf("I shouldn't be here, should I?\n");
				Ray->obliterate();
				 return; 
				
				
			}
			else
			{
				Ray->obliterate();
				//printf("%d\n",i);
				Lost_at_emission += 1;
				return;
			}
		}
	}while(false);
	
	return;
	
	/*
	
	y = Ray->DY() * glass_thickness/Ray->DZ()+ Ray->Y();
	x = Ray->DX() * glass_thickness/Ray->DZ()+ Ray->X();
	z = 0;
	if(y < domainY && y > 0 && x < domainX && x > 0)
	{
		dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
		Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
		Ray->aT(dis);
		Ray->X(x);
		Ray->Y(y);
		Ray->Z(z);
		Ray->set_elem(x/Xelem_size, y/Yelem_size, 0);
		//printf("type 1 %f\n",Ray->Power());
		#if LOGRAYS
		{			
			fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
		}			
		#endif
		 return; 
	}
	else
	{
		y = Ray->DY() * (glass_thickness+domainZ)/Ray->DZ()+ Ray->Y();
		x = Ray->DX() * (glass_thickness+domainZ)/Ray->DZ()+ Ray->X();
		if(y < domainY && y > 0 && x < domainX && x > 0)
		{
			x = Ray->DX() * (domainY - Ray->Y())/Ray->DY() + Ray->X();
			z = Ray->DZ() * (domainY - Ray->Y())/Ray->DY() + Ray->Z();
			y = domainY;
			
			if(x < domainX && x > 0 && z < domainZ && z > 0)
			{
				
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(x/Xelem_size, domain_elemY -1, z/Zelem_size);
				//printf("type 2 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			
			x = Ray->DX() * (0 - Ray->Y())/Ray->DY() + Ray->X();
			z = Ray->DZ() * (0 - Ray->Y())/Ray->DY() + Ray->Z();
			y = 0.0;
			
			if(x < domainX && x > 0 && z < domainZ && z > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(x/Xelem_size, 0, z/Zelem_size);
				//printf("type 3 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			x = domainX;
			z = Ray->DZ() * (domainX - Ray->X())/Ray->DX() + Ray->Z();
			y = Ray->DY() * (domainX - Ray->X())/Ray->DX() + Ray->Y();
			
			if(z < domainZ && z > 0 && y < domainY && y > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(domain_elemX -1, y/Yelem_size, z/Zelem_size );
				//printf("type 4 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			x = 0.0;
			z = Ray->DZ() * (0 - Ray->X())/Ray->DX() + Ray->Z();
			y = Ray->DY() * (0 - Ray->X())/Ray->DX() + Ray->Y();
			
			if(z < domainZ && z > 0 && y < domainY && y > 0)
			{
				dis = DISTANCE(glass_thickness,Ray->Y(),Ray->X(),z,y,x);
				Ray->attenuate(Borosilicate_Absorbtion(Ray->Lambda()),dis);
				Ray->aT(dis);
				Ray->X(x);
				Ray->Y(y);
				Ray->Z(z);
				Ray->set_elem(0, y/Yelem_size, z/Zelem_size );
				//printf("type 5 %f\n",Ray->Power());
				#if LOGRAYS
				{			
					fprintf(RAYLOG,"%f,%f,%f,-1,",Ray->X(),Ray->Y(),Ray->Z());	
				}			
				#endif
				 return; 
			}
			
			printf("I shouldn't be here, should I?\n");
			Ray->obliterate();
			
			
			
		}
		else
		{
			Ray->obliterate();
			//printf("%d\n",i);
			Lost_at_emission += 1;
		}
	}
	
	#if LOGRAYS	
	if(Ray->dead())
	{
		fprintf(RAYLOG,"0.0,0.0,0.0,0,");
	}
	#endif
	
	return; */
}
